﻿CREATE PROCEDURE [dbo].[spEmpreendimentoRelacionado_Read]
    @IdEmpreendimento UNIQUEIDENTIFIER = NULL
AS
BEGIN
    SELECT [IdEmpreendimentoRelacionado]
        FROM EmpreendimentoRelacionado
        WHERE (IdEmpreendimento = @IdEmpreendimento);
END