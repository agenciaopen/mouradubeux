﻿CREATE PROCEDURE [dbo].[spEmpreendimentoRelacionado_Delete]
@IdEmpreendimento uniqueidentifier = null,
@IdEmpreendimentoRelacionado uniqueidentifier = null

AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.EmpreendimentoRelacionado WHERE IdEmpreendimento = @IdEmpreendimento and IdEmpreendimentoRelacionado = @IdEmpreendimentoRelacionado;
	COMMIT TRANSACTION
END