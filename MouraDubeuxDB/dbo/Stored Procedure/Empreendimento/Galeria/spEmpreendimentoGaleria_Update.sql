﻿CREATE PROCEDURE [dbo].[spEmpreendimentoGaleria_Update]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@UrlImagem nvarchar(MAX) = null,
	@UrlVideo nvarchar(MAX) = null,
	@Descricao nvarchar(MAX) = null,
	@TipoGaleria int = null,
	@Titulo nvarchar(MAX) = null,
	@EhVideo bit= null,
	@Ordem int = null
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.EmpreendimentoGaleria set UrlImagem = @UrlImagem, UrlVideo = @UrlVideo, Ordem = @Ordem, TipoGaleria = @TipoGaleria, EhVideo = @EhVideo
		where Id = @Id;

		UPDATE dbo.EmpreendimentoGaleria set Descricao = @Descricao, Titulo = @Titulo
		where Id = @Id and IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END