﻿CREATE PROCEDURE [dbo].[spEmpreendimentoGaleria_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.EmpreendimentoGaleria WHERE Id = @Id;
	COMMIT TRANSACTION
END