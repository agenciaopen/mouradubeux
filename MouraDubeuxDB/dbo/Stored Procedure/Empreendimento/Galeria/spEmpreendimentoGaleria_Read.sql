﻿CREATE PROCEDURE [dbo].[spEmpreendimentoGaleria_Read]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@TipoGaleria int = null
AS
BEGIN
	SELECT [Id], [IdiomaId], [UrlImagem], [UrlVideo], [Descricao], [TipoGaleria], [Titulo], [EhVideo], [IdEmpreendimento], [Ordem] FROM dbo.EmpreendimentoGaleria
     WHERE (Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00)) AND (TipoGaleria = @TipoGaleria
	 AND IdEmpreendimento = @IdEmpreendimento)
	 ORDER BY [Ordem];
END
