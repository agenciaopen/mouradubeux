﻿CREATE PROCEDURE [dbo].[spEmpreendimentoValorDescricao_Update]
	@Id uniqueidentifier = NULL,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@Descricao nvarchar(MAX) = NULL,
	@Valor nvarchar(50) = NULL,
	@Titulo nvarchar(200) = NULL,
	@Slug nvarchar(200) = NULL,
	@planta bit = null,
	@ValorPrincipal bit = null
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.EmpreendimentoValorDescricao set Descricao = @Descricao, Valor = @Valor, Titulo = @Titulo, Slug = @Slug, Planta = @planta, ValorPrincipal = @ValorPrincipal
		where Id = @Id and IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END