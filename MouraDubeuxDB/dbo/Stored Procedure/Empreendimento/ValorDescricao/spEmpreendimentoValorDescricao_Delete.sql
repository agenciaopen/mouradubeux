﻿CREATE PROCEDURE [dbo].[spEmpreendimentoValorDescricao_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.EmpreendimentoValorDescricao WHERE Id = @Id;
	COMMIT TRANSACTION
END