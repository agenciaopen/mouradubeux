﻿CREATE PROCEDURE [dbo].[spEmpreendimentoValorDescricao_Read]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null

AS
BEGIN
	SELECT [Id], [IdEmpreendimento], [IdiomaId], [Descricao], [Valor], [Titulo], [Slug], [Planta], [ValorPrincipal]
	  FROM EmpreendimentoValorDescricao
     WHERE (Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
	 AND IdEmpreendimento = @IdEmpreendimento;
END