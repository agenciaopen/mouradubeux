﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.Empreendimento WHERE Id = @Id;
	COMMIT TRANSACTION
END