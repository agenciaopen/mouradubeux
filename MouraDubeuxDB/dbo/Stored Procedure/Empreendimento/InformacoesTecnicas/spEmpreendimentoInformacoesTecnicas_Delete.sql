﻿CREATE PROCEDURE [dbo].[spEmpreendimentoInformacoesTecnicas_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.EmpreendimentoInformacoesTecnicas WHERE Id = @Id;
	COMMIT TRANSACTION
END