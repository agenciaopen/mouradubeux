﻿CREATE PROCEDURE [dbo].[spEmpreendimentoInformacoesTecnicas_Read]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null

AS
BEGIN
	SELECT [Id], [IdEmpreendimento], [IdiomaId], [Nome], [Funcao] FROM dbo.EmpreendimentoInformacoesTecnicas
     WHERE (Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
	 AND IdEmpreendimento = @IdEmpreendimento;
END