﻿CREATE PROCEDURE [dbo].[spEmpreendimentoInformacoesTecnicas_Create]
	@Id uniqueidentifier = null,
	@IdEmpreendimento uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@Nome nvarchar(MAX) = null,
	@Funcao nvarchar(MAX) = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.EmpreendimentoInformacoesTecnicas(Id, IdEmpreendimento, IdiomaId, Nome, Funcao) VALUES(@Id, @IdEmpreendimento, @oneid, @Nome, @Funcao);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END