﻿CREATE PROCEDURE [dbo].[spEmpreendimentoFichaTecnica_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.EmpreendimentoFichaTecnica WHERE Id = @Id;
	COMMIT TRANSACTION
END