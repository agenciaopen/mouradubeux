﻿CREATE PROCEDURE [dbo].[spEmpreendimentoFichaTecnica_Read]
	@Id uniqueidentifier = null,
    @IdEmpreendimento uniqueidentifier = null,
	@TipoFichaTecnica int = null
AS
BEGIN
	SELECT [Id], [IdEmpreendimento], [IdiomaId], [UrlImagem], [TipoFichaTecnica], [Peso], [IdValorDescricao] FROM EmpreendimentoFichaTecnica
	WHERE (Id = @Id or (@Id = CONVERT(UNIQUEIDENTIFIER, 0x00) AND (IdEmpreendimento = @IdEmpreendimento AND TipoFichaTecnica = @TipoFichaTecnica)))
	order by Peso desc;
END
