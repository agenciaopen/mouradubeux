﻿CREATE PROCEDURE [dbo].[spEmpreendimentoFichaTecnica_Update]
	@Id uniqueidentifier = null,
    @IdEmpreendimento uniqueidentifier = null,
    @IdValorDescricao uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@UrlImagem nvarchar(50) = null,
	@TipoFichaTecnica int = null,
	@Peso int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		UPDATE dbo.EmpreendimentoFichaTecnica set Peso = @Peso
		where Id = @Id AND IdEmpreendimento = @IdEmpreendimento;
	COMMIT TRANSACTION
END