﻿CREATE PROCEDURE [dbo].[spEmpreendimentoFichaTecnica_Create]
	@Id uniqueidentifier = null,
    @IdEmpreendimento uniqueidentifier = null,
    @IdValorDescricao uniqueidentifier = null,
	@IdiomaId nvarchar(10) = null,
	@UrlImagem nvarchar(50) = null,
	@TipoFichaTecnica int = null,
	@Peso int = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
		INSERT INTO dbo.EmpreendimentoFichaTecnica(Id, IdiomaId, IdEmpreendimento, UrlImagem, TipoFichaTecnica, Peso, IdValorDescricao) VALUES(@Id, @oneid, @IdEmpreendimento, @UrlImagem, @TipoFichaTecnica, @Peso, @IdValorDescricao);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
	BEGIN TRANSACTION
	COMMIT TRANSACTION
END