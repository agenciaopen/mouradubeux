﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Read]
    @Id uniqueidentifier = null
AS
BEGIN
    SELECT DISTINCT e.[Id], e.[IdiomaId], e.[Nome], e.[TipoEmpreendimento], 
            e.[StatusEmpreendimento], e.[Metragem], e.[VagasGaragem], 
            e.[Quartos], e.[UnidadeSujeitaDisponibilidade], e.[Endereco], 
            e.[UrlVideo], e.[UrlBanner], e.[UrlImagemCard], e.[UrlLogo], e.[Ativo], e.[Lote], 
            e.[DataCriacao], e.[DataAlteracao], e.[UserCriacao], e.[UserAlteracao], 
            e.[StatusFundacao], e.[StatusEstrutura], e.[StatusAlvenaria], 
            e.[StatusFachada], e.[StatusAcabamento], e.[StatusPintura], 
            e.[StatusEntrega], e.[StatusServicosPreliminares], e.[Slug], e.[AnoEntrega],
            e.[DestacarHome], e.[Latitude], e.[Longitude], e.[TourVirtualAtivo], e.[TourVirtual],
            e.[Bairro], e.[DestacarStatus], e.[DestacarVitrine], e.[Sazonal], e.[ArquivoPlanta], e.[ModeloNegocio], 
            e.[Estado], e.[Cidade], e.[CEP], c.Nome as 'CidadeNome', uf.UF as 'EstadoNome', e.[UrlChat], e.[MataTittle], e.[MetaDescription], e.[MetaKeyword],
            e.[IdRdstation], e.[UrlImagemCampanha], e.[OrdemBanner], 
            (
            select top 1 w.[Valor]
            from EmpreendimentoValorDescricao w 
            where w.IdEmpreendimento = e.Id and w.IdiomaId = e.IdiomaId
            order by w.ValorPrincipal desc
            ) as Valor
            FROM Empreendimento e
      LEFT JOIN Estados uf on uf.Slug = e.Estado
      LEFT JOIN Cidades c on c.Slug = e.Cidade and c.UF = uf.UF
     WHERE (e.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
     ORDER BY Valor DESC;
END