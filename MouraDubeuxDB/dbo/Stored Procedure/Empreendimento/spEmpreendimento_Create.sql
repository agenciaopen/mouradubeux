﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Create]
	@Id uniqueidentifier = null,
	@Nome nvarchar(50) = null,
	@IdiomaId nvarchar(2) = null,
	@TipoEmpreendimento int = null, 
	@StatusEmpreendimento int = null, 
	@Metragem nvarchar(50) = null, 
	@VagasGaragem nvarchar(50) = null, 
    @Quartos nvarchar(50) = null,
	@UnidadeSujeitaDisponibilidade bit = null, 
	@Endereco nvarchar(MAX) = null, 
	@UrlVideo nvarchar(MAX) = null, 
	@UrlBanner nvarchar(MAX) = null, 
	@UrlImagemCard nvarchar(MAX) = null, 
	@UrlLogo nvarchar(MAX) = null, 
	@Ativo bit = null, 
	@Lote BIT = NULL, 
	@DataCriacao datetime2(7) = null, 
	@DataAlteracao datetime2(7) = null, 
	@UserCriacao nvarchar(50) = null, 
	@UserAlteracao nvarchar(50) = null,
	@StatusFundacao int = null, 
	@StatusEstrutura int = null, 
	@StatusAlvenaria int = null, 
	@StatusFachada int = null, 
	@StatusAcabamento int = null, 
	@StatusPintura int = null, 
	@StatusEntrega int = null,
	@StatusServicosPreliminares INT = NULL, 
	@Slug nvarchar(MAX) = null,
	@Estado nvarchar(200) = null,
	@Cidade nvarchar(200) = null,
	@CEP nvarchar(200) = null,
	@DestacarHome bit = null,
	@Latitude nvarchar(200) = null,
	@Longitude nvarchar(200) = null,
	@TourVirtualAtivo bit = null,
	@TourVirtual nvarchar(MAX) = null,
	@Bairro nvarchar(200) = null,
	@DestacarStatus bit = null,
	@DestacarVitrine bit = null,
	@Sazonal bit = null,
	@ArquivoPlanta varchar(MAX) = null,
	@ModeloNegocio NVARCHAR(2) = null,
	@AnoEntrega varchar(200) = null,
	@UrlChat NVARCHAR(MAX) = NULL, 
    @MataTittle NVARCHAR(200) = NULL, 
    @MetaDescription NVARCHAR(MAX) = NULL, 
    @MetaKeyword NVARCHAR(200) = NULL,
	@IdRdstation NVARCHAR(MAX) = NULL,
	@UrlImagemCampanha NVARCHAR(MAX) = NULL,
	@Valor NVARCHAR(50) = NULL,
	@OrdemBanner INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.Empreendimento(Id, IdiomaId, Nome, TipoEmpreendimento, StatusEmpreendimento, Metragem, VagasGaragem, Quartos, UnidadeSujeitaDisponibilidade, 
					Endereco, UrlVideo, UrlBanner, UrlLogo, Ativo, DataCriacao, DataAlteracao, UserCriacao, UserAlteracao, Slug, Estado, Cidade, CEP, Lote, StatusServicosPreliminares, 
					DestacarHome, Latitude, Longitude, TourVirtualAtivo, TourVirtual, Bairro, DestacarStatus, DestacarVitrine, Sazonal, ArquivoPlanta, ModeloNegocio, AnoEntrega,
					UrlChat, MataTittle, MetaDescription, MetaKeyword, IdRdstation, UrlImagemCampanha, UrlImagemCard, OrdemBanner) 
				VALUES(@Id, @oneId, @Nome, @TipoEmpreendimento, @StatusEmpreendimento, @Metragem, @VagasGaragem, @Quartos, @UnidadeSujeitaDisponibilidade, 
					@Endereco, @UrlVideo, @UrlBanner, @UrlLogo, @Ativo, @DataCriacao, @DataAlteracao, @UserCriacao, @UserAlteracao, @Slug, @Estado, @Cidade, @CEP, @Lote, @StatusServicosPreliminares,
					@DestacarHome, @Latitude, @Longitude, @TourVirtualAtivo, @TourVirtual, @Bairro, @DestacarStatus, @DestacarVitrine, @Sazonal, @ArquivoPlanta, @ModeloNegocio, @AnoEntrega,
					@UrlChat, @MataTittle, @MetaDescription, @MetaKeyword, @IdRdstation, @UrlImagemCampanha, @UrlImagemCard, @OrdemBanner);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END