﻿CREATE PROCEDURE [dbo].[spEmpreendimento_Read_Distinct_Quartos]
    @Id uniqueidentifier = null,
    @Cidade NVARCHAR(200) NULL,
    @Bairro NVARCHAR(200) NULL
AS
BEGIN
    SELECT DISTINCT e.[Quartos]
      FROM Empreendimento e
      where trim(e.[Quartos]) not like '' and trim(e.[Quartos]) not like '-'
      and (upper(trim(e.Cidade)) like upper(trim(@Cidade)) or trim(@Cidade) like '')
      and (upper(trim(e.Bairro)) like upper(trim(@Bairro)) or trim(@Bairro) like '')
END