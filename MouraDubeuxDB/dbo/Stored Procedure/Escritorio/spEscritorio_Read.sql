﻿CREATE PROCEDURE [dbo].[spEscritorio_Read]
	@Id uniqueidentifier = null
AS
BEGIN
    SELECT e.[Id], e.[IdiomaId], e.[Nome], e.[Telefone], 
            e.[Endereco], e.[Estado], e.[Cidade], e.[CEP], e.[HorarioFuncionamento], e.[UrlBanner],
            c.Nome as 'CidadeNome', uf.UF as 'EstadoNome'
        FROM Escritorio e
        LEFT JOIN Estados uf on uf.Slug = e.Estado
        LEFT JOIN Cidades c on c.Slug = e.Cidade
        WHERE (e.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
        ORDER BY e.[Nome];
END