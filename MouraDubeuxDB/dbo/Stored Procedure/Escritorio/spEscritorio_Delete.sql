﻿CREATE PROCEDURE [dbo].[spEscritorio_Delete]
	@Id uniqueidentifier = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DELETE FROM dbo.Escritorio WHERE Id = @Id;
	COMMIT TRANSACTION
END