﻿CREATE PROCEDURE [dbo].[spEscritorio_Update]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Nome nvarchar(50),
	@Telefone nvarchar(50),
	@Endereco nvarchar(max),
	@Estado nvarchar(200),
	@Cidade nvarchar(200),
	@CEP nvarchar(200),
	@HorarioFuncionamento nvarchar(200),
	@UrlBanner nvarchar(200),
	@EstadoNome nvarchar(200),
	@CidadeNome nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.Escritorio SET IdiomaId = @IdiomaId, Nome = @Nome, Telefone = @Telefone, 
				Endereco = @Endereco, Estado = @Estado, Cidade = @Cidade, CEP = @CEP, HorarioFuncionamento = @HorarioFuncionamento, UrlBanner = @UrlBanner
			WHERE Id = @Id AND IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END