﻿CREATE PROCEDURE [dbo].[spEscritorio_Create]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Nome nvarchar(50),
	@Telefone nvarchar(50),
	@Endereco nvarchar(max),
	@Estado nvarchar(200),
	@Cidade nvarchar(200),
	@CEP nvarchar(200),
	@HorarioFuncionamento nvarchar(200),
	@UrlBanner nvarchar(200),
	@EstadoNome nvarchar(200),
	@CidadeNome nvarchar(200)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.Escritorio(Id, IdiomaId, Nome, Telefone, Endereco, Estado, Cidade, CEP, HorarioFuncionamento, UrlBanner) 
			                       VALUES(@Id, @oneId, @Nome, @Telefone, @Endereco, @Estado, @Cidade, @CEP, @HorarioFuncionamento, @UrlBanner);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END