﻿CREATE PROCEDURE [dbo].[spPagina_Update]
	@Slug NVARCHAR(200) = null,
    @IdiomaId NVARCHAR(10) = null,
    @Titulo NVARCHAR(200)  = null,
    @SubTitulo NVARCHAR(200)  = null,
	@Banner NVARCHAR(MAX) = null,
	@Link NVARCHAR(MAX) = null,
    @Imagem NVARCHAR(MAX) = null,
    @Descricao NVARCHAR(MAX) = null,
    @MataTittle NVARCHAR(200) = null,
    @MetaDescription NVARCHAR(MAX)  = null,
    @MetaKeyword NVARCHAR(200) = null
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
		UPDATE dbo.Pagina SET Titulo = @Titulo, Descricao = @Descricao, MataTittle = @MataTittle, MetaDescription = @MetaDescription, MetaKeyword = @MetaKeyword, Banner = @Banner, Link = @Link, Imagem = @Imagem
		 WHERE upper(Slug) like upper(@Slug) AND IdiomaId = @IdiomaId;
	COMMIT TRANSACTION
END