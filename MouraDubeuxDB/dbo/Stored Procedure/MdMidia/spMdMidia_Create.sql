﻿CREATE PROCEDURE [dbo].[spMdMidia_Create]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Titulo nvarchar(max),
	@Descricao nvarchar(max),
	@Conteudo nvarchar(max),
	@DataPublicacao datetime2(7) = null, 
	@UrlImagem nvarchar(max) = null,
	@Link nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.MdMidia(Id, IdiomaId, Titulo, Descricao, Conteudo, DataPublicacao, UrlImagem, Link) 
			                       VALUES(@Id, @oneId, @Titulo, @Descricao, @Conteudo, @DataPublicacao, @UrlImagem, @Link);
			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION
END