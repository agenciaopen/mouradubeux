﻿CREATE PROCEDURE [dbo].[spMdMidia_Update]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Titulo nvarchar(max),
	@Descricao nvarchar(max),
	@Conteudo nvarchar(max),
	@DataPublicacao datetime2(7) = null, 
	@UrlImagem nvarchar(max) = null,
	@Link nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

		UPDATE dbo.MdMidia SET IdiomaId = @IdiomaId, Titulo = @Titulo, Descricao = @Descricao, Conteudo = @Conteudo, DataPublicacao = @DataPublicacao, UrlImagem = @UrlImagem, Link = @Link
			WHERE Id = @Id AND IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END