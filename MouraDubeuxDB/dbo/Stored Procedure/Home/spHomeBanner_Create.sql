﻿CREATE PROCEDURE [dbo].[spHomeBanner_Create]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Descricao nvarchar(50),
	@Link nvarchar(50),
	@LinkImagem nvarchar(max),
	@Titulo nvarchar(50)
AS
BEGIN
		SET NOCOUNT ON;
		DECLARE @DATAATUAL DATETIME;

		SET @DATAATUAL = GETDATE();


	BEGIN TRANSACTION

		INSERT INTO dbo.HomeBanner(Id, DataCriacao, LinkImagem) VALUES(@Id, @DATAATUAL, @LinkImagem);

		DECLARE @oneId nvarchar(2)
		DECLARE the_cursor CURSOR FAST_FORWARD
		FOR SELECT Id  
			FROM dbo.Idioma
		OPEN the_cursor
		FETCH NEXT FROM the_cursor INTO @oneId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			--EXEC UpdateComputedFullText @oneid
			INSERT INTO dbo.HomeBanner_Detalhes(Id, IdiomaId, Descricao, Link, Titulo) 
			 VALUES (@Id, @oneId, @Descricao, @Link, @Titulo);

			FETCH NEXT FROM the_cursor INTO @oneid
		END

		CLOSE the_cursor
		DEALLOCATE the_cursor
	COMMIT TRANSACTION

END