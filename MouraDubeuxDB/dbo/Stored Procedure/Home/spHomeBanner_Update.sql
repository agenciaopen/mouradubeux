﻿CREATE PROCEDURE [dbo].[spHomeBanner_Update]
	@Id uniqueidentifier = null,
	@IdiomaId nvarchar(2) = null,
	@Descricao nvarchar(50),
	@Link nvarchar(50),
	@LinkImagem nvarchar(max),
	@Titulo nvarchar(50)
AS
BEGIN
		SET NOCOUNT ON;
		DECLARE @DATAATUAL DATETIME;
		SET @DATAATUAL = GETDATE();

	BEGIN TRANSACTION

		UPDATE dbo.HomeBanner set LinkImagem = @LinkImagem
		where Id = @Id;

		UPDATE dbo.HomeBanner_Detalhes set Descricao = @Descricao, Link = @Link, Titulo = @Titulo
		where Id = @Id and IdiomaId = @IdiomaId;

	COMMIT TRANSACTION
END