﻿CREATE PROCEDURE [dbo].[spHomeBanner_Read]
	@Id uniqueidentifier = null
AS
BEGIN
SELECT h.Id, h.LinkImagem, hd.IdiomaId, hd.Titulo, hd.Descricao, hd.Link
  FROM dbo.HomeBanner h
  inner join dbo.HomeBanner_Detalhes hd on hd.Id = h.Id
  where (h.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00));
END