﻿CREATE PROCEDURE [dbo].[spFaq_Read]
    @Id UNIQUEIDENTIFIER = NULL

AS
BEGIN
    SELECT [Id], [IdiomaId], [TipoFaq], [TipoFaqArea], [Secao], [Titulo], [Texto], [UrlImagem]
        FROM Faq
        WHERE (Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
        ORDER BY Secao;
END