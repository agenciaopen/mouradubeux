﻿CREATE PROCEDURE [dbo].[spEstados_Read]
	@Slug varchar(300) = ''
AS

BEGIN
	SELECT * from Estados
	where (Slug = @Slug or @Slug = '')
	ORDER BY Slug ASC
END