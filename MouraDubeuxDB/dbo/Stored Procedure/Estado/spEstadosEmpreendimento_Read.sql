﻿CREATE PROCEDURE [dbo].[spEstadosEmpreendimento_Read]
AS

BEGIN
	SELECT distinct e.* from Estados e
	inner join Empreendimento es on es.Estado = e.Slug
	ORDER BY e.Slug ASC
END