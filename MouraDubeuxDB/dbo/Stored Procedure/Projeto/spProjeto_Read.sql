﻿CREATE PROCEDURE [dbo].[spProjeto_Read]
	@Id uniqueidentifier = null
AS
BEGIN
    SELECT p.[Id], p.[IdiomaId], p.[Nome], p.[Descricao], 
            p.[Endereco], p.[Estado], p.[Cidade], p.[UrlImagem], p.[ProjetoSocial],
            c.Nome as 'CidadeNome', uf.UF as 'EstadoNome'
        FROM Projeto p
        LEFT JOIN Estados uf on uf.Slug = p.Estado
        LEFT JOIN Cidades c on c.Slug = p.Cidade
        WHERE (p.Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00))
        ORDER BY p.[Nome];
END