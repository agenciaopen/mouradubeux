﻿CREATE PROCEDURE [dbo].[spProjetoGaleria_Read]
	@Id uniqueidentifier = null,
	@IdProjeto uniqueidentifier = null
AS
BEGIN
	SELECT [Id], [IdiomaId], [UrlImagem], [UrlVideo], [Titulo], [EhVideo], [IdProjeto], [Ordem] FROM dbo.ProjetoGaleria
     WHERE (Id = @Id or @Id = CONVERT(UNIQUEIDENTIFIER, 0x00)) AND (IdProjeto = @IdProjeto)
	 ORDER BY [Ordem];
END
