﻿CREATE TABLE [dbo].[HomeBanner_Detalhes]
(
    [Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
    [Titulo] NVARCHAR(50) NULL, 
    [Descricao] NVARCHAR(MAX) NULL, 
    [Link] NVARCHAR(200) NULL, 
    [Ativo] BIT NULL, 
    CONSTRAINT [FK_HomeBanner_Detalhes_To_HomeBanner] FOREIGN KEY ([Id]) REFERENCES [HomeBanner]([Id]), 
    CONSTRAINT [FK_HomeBanner_Detalhes_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT HomeBannerPK PRIMARY KEY ([Id], [IdiomaId])
)

GO

CREATE INDEX [IX_HomeBanner_Detalhes_IdiomaId] ON [dbo].[HomeBanner_Detalhes] ([IdiomaId])
