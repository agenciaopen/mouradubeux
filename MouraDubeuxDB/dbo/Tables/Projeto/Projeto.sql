﻿CREATE TABLE [dbo].[Projeto]
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [IdiomaId] NVARCHAR(10) NOT NULL,
    [Nome] NVARCHAR(50) NULL,
    [Descricao] NVARCHAR(MAX) NULL,
    [Endereco] NVARCHAR(MAX) NULL, 
    [Estado] NVARCHAR(200) NULL, 
    [Cidade] NVARCHAR(200) NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL, 
    [ProjetoSocial] BIT NULL, 
    CONSTRAINT [PK_Projeto] PRIMARY KEY ([Id], [IdiomaId]),
    CONSTRAINT [FK_Projeto_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id])
)
