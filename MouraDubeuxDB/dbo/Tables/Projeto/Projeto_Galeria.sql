﻿CREATE TABLE [dbo].[ProjetoGaleria]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL, 
    [UrlVideo] NVARCHAR(MAX) NULL, 
    [Titulo] NVARCHAR(MAX) NULL, 
    [EhVideo] BIT NULL, 
    [IdProjeto] UNIQUEIDENTIFIER NOT NULL, 
    [Ordem] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_ProjetoGaleria] PRIMARY KEY ([Id], [IdiomaId], [IdProjeto]),
    CONSTRAINT [FK_ProjetoGaleria_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT [FK_ProjetoGaleria_To_Empreendimento] FOREIGN KEY ([IdProjeto], [IdiomaId]) REFERENCES [Projeto]([Id], [IdiomaId])
)
