﻿CREATE TABLE [dbo].[Pagina]
(
    [Slug] NVARCHAR(200) NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL,
    [Titulo] NVARCHAR(200) NOT NULL, 
    [SubTitulo] NVARCHAR(200) NULL, 
    [Banner] NVARCHAR(MAX) NULL, 
    [Link] NVARCHAR(MAX) NULL, 
    [Imagem] NVARCHAR(MAX) NULL, 
    [Descricao] NVARCHAR(MAX) NULL, 
    [MataTittle] NVARCHAR(200) NULL, 
    [MetaDescription] NVARCHAR(MAX) NULL, 
    [MetaKeyword] NVARCHAR(200) NULL,
    CONSTRAINT [PK_Pagina] PRIMARY KEY ([Slug], [IdiomaId]),
    CONSTRAINT [FK_Pagina_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id])


)
