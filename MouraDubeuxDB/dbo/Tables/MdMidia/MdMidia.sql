﻿CREATE TABLE [dbo].[MdMidia]
(
    [Id] UNIQUEIDENTIFIER NOT NULL,
    [IdiomaId] NVARCHAR(10) NOT NULL,
    [Titulo] NVARCHAR(MAX) NULL,
    [Descricao] NVARCHAR(MAX) NULL,
    [Conteudo] NVARCHAR(MAX) NULL, 
    [DataPublicacao] DATETIME2 NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL, 
    [Link] NVARCHAR(MAX) NULL,
    CONSTRAINT [PK_MdMidia] PRIMARY KEY ([Id], [IdiomaId]),
    CONSTRAINT [FK_MdMidia_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id])
)
