﻿CREATE TABLE [dbo].[EmpreendimentoValorDescricao]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
    [Descricao] NVARCHAR(MAX) NULL,
    [Valor] NVARCHAR(50) NULL, 
    [IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL, 
    [Titulo] NVARCHAR(200) NULL, 
    [Slug] NVARCHAR(200) NULL, 
    [Planta] BIT NULL,
    [ValorPrincipal] BIT NULL, 
    CONSTRAINT [PK_EmpreendimentoValorDescricao] PRIMARY KEY ([Id], [IdiomaId]),
    CONSTRAINT [FK_EmpreendimentoValorDescricao_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT [FK_EmpreendimentoValorDescricao_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId])
)
