﻿CREATE TABLE [dbo].[EmpreendimentoGaleria]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL, 
    [UrlVideo] NVARCHAR(MAX) NULL, 
    [Descricao] NVARCHAR(MAX) NULL,
    [TipoGaleria] INT NULL, 
    [Titulo] NVARCHAR(MAX) NULL, 
    [EhVideo] BIT NULL, 
    [IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL, 
    [Ordem] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_EmpreendimentoGaleria] PRIMARY KEY ([Id], [IdiomaId], [IdEmpreendimento]),
    CONSTRAINT [FK_EmpreendimentoGaleria_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT [FK_EmpreendimentoGaleria_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId])
)
