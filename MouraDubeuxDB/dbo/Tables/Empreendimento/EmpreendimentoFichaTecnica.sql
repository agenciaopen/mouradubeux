﻿CREATE TABLE [dbo].[EmpreendimentoFichaTecnica]
(
	[Id] UNIQUEIDENTIFIER NOT NULL,
    [IdEmpreendimento] UNIQUEIDENTIFIER NOT NULL, 
    [IdValorDescricao] UNIQUEIDENTIFIER NULL, 
    [IdiomaId] NVARCHAR(10) NOT NULL, 
    [UrlImagem] NVARCHAR(MAX) NULL, 
    [TipoFichaTecnica] INT NULL, 
    [Peso] INT NULL, 
    CONSTRAINT [FK_EmpreendimentoFichaTecnica_To_Empreendimento] FOREIGN KEY ([IdEmpreendimento], [IdiomaId]) REFERENCES [Empreendimento]([Id], [IdiomaId]), 
    CONSTRAINT [FK_EmpreendimentoFichaTecnica_To_Idioma] FOREIGN KEY ([IdiomaId]) REFERENCES [Idioma]([Id]),
    CONSTRAINT [PK_EmpreendimentoFichaTecnica] PRIMARY KEY ([IdiomaId], [Id]) 
)
