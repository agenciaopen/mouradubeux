window.initAbout = function () {
  $('.menu__open ul li a').click(function (e) {
    $('.trigger').click();
  });
  $(".trigger").on('click', function () {
    if ($(".trigger-top").hasClass("no-animation")) {
      $(".menu").toggleClass("menu__open");
      $(".trigger-top").removeClass("no-animation");
      $(".trigger-middle").toggleClass("d-none");
      $(".trigger-bottom").removeClass("no-animation");
      $(this).toggleClass('is-active');
    } else {
      $(this).toggleClass('is-active');
      setTimeout(function () {
        $(".menu").toggleClass("menu__open");
        $(".trigger-middle").toggleClass("d-none");
      }, 100);
    }
  });
  $(function () {
    $('.timeline-nav').slick({
      slidesToShow: 12,
      slidesToScroll: 1,
      asNavFor: '.timeline-slider',
      centerMode: false,
      mobileFirst: true,
      arrows: false,
      infinite: false,
      autoplay: true,
      pauseOnHover: true,
      focusOnSelect: true,

      autoplaySpeed: 5000,
      responsive: [{
          breakpoint: 767,
          settings: {
            slidesToShow: 8
          }
        },


        {
          breakpoint: 0,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 2
          }
        }
      ]
    });





    $('.timeline-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      asNavFor: '.timeline-nav',
      centerMode: true,
      cssEase: 'ease',
      edgeFriction: 0.5,
      centerMode: true,
      centerPadding: '50px',
      mobileFirst: true,
      pauseOnHover: true,
      autoplay: true,
      pauseOnHover: true,
      speed: 500,
      focusOnSelect: true,
      //draggable: false,

      responsive: [{
          breakpoint: 0,
          settings: {
            centerMode: false
          }
        },


        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,

            centerMode: true
          }
        }
      ]
    });

  });

  //# sourceURL=pen.js
}