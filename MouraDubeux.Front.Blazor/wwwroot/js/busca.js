window.initBusca = function () {

    let queryString = window.location.search;

    jQuery(document).ready(function ($) {
        if (queryString != null) {
            $('html, body').animate({
                scrollTop: $('#buscaResultado').offset().top
            }, 'slow');
        } else {
            console.log('empty string');
        }
    });
    if (screen.width > 992) {
        let menu = $(".filter");
        let offsetTopMenu = menu.offset().top;
        console.log(offsetTopMenu);
        $(window).scroll(function(){
        //more then or equals to
        if($(window).scrollTop() >= offsetTopMenu ){
            // $(".filter").addClass('fixed-top');
            $(".filter").addClass("filter__scrolled");

        //less then 100px from top
        } else {
            $(".filter").removeClass('filter__scrolled');

        }
        });
    }
}