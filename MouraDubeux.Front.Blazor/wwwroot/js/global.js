window.initGlobal = function () {
    let navHeight = $('.bread_crumb').height();
    $(document).ready(function(){
    // $(".bread_crumb").css({/* position: 'fixed', 'z-index': '999', */  'width' : '100%'});

    });

    if (screen.width <= 960) {
        // download complicated script
        // swap in full-source images for low-source ones
        // $('body').css({ paddingTop : navHeight + 10 + 'px' });
        $(function(){
            $('.carousel-item img[src$=".jpeg"], .carousel-item img[src*=".jpeg"], .figure img[src*=".jpeg"], .card-header img[src$=".jpeg"], .hero img[src$=".jpeg"]').each(function(index,element) {
                element.src = element.src.replace('.jpeg','_thumb.jpeg');
            });
            $('.card-header, .hero').each(function(index,element) {
                var bg = $(this).css('background-image');
                bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
                console.log(bg);
                element.src = bg.replace('.jpeg','_thumb.jpeg');
                console.log(element.src);
                //element.src = element.src.replace('.jpeg','_thumb.jpeg');
                $(this).css('background-image', 'url("'+ element.src +'")');
            });
            
            // $('[style*=".jpeg"]').each(function(index,element) {
            //     element.src = element.src.replace('.jpeg','_thumb.jpeg');
            // });
        });  
    }
    if (screen.width <= 991) {
    
        let bannerHeight = $('#banner__carousel').height();
        $('#banner__carousel').css({ height : bannerHeight + 'px' });
        $('#banner__carousel').attr('style', 'height:'+bannerHeight+'px !important');
        
    
    }
    $(window).on('resize scroll', function() {
        let cta = $('.cta');
        let ctaTop = 250;
        if ($(window).scrollTop() >= ctaTop) {
            // topBar.removeClass('fixed-top');
            cta.addClass('cta__scrolled');

        } else {
            cta.removeClass('cta__scrolled');
        }
    });
    // $('section h3, section h2, section h1 + p, section h2 + p, section h6, section h1, section div:not(.timeline-slide)h4, section h5, section:not(.busca) .form-group, section:not(#banner__carousel) .btn').attr('data-aos', 'fade-in');
    // $('section:not(#banner__carousel)div:not(.timeline-slide) img, section:not(#banner__carousel) svg, .card').attr('data-aos', 'fade-in');
    // setTimeout(function(){ 
    //     AOS.init({
    //         offset: 200,
    //         duration: 500,
    //         mirror: true,
    //         once: true,
    //         delay: 50,
    //         disable: 'mobile',
    //     });
    // }, 80); 
}

// >>>>>>>>>>> START JS FORNECEDOR <<<<<<<<<<<<

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Finalizar cadastro";
    } else {
        document.getElementById("nextBtn").innerHTML = "Proximo";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
        // ... the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function validateForm() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
        // If a field is empty...
        if (y[i].value == "") {
            // add an "invalid" class to the field:
            y[i].className += " invalid";
            // and set the current valid status to false
            valid = false;
        }
    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
}