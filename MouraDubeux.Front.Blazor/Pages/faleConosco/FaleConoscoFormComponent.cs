﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Pages.faleConosco
{
    public partial class FaleConoscoFormComponent : ComponentBase
    {
        public ContatoFormModel model = new ContatoFormModel();

        [Inject]
        IMailService mailService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        private async Task HandleValidSubmit()
        {
            string htmlString = @$"<html>
                      <body>
                      <h3>Formulário Fala Conosco</h3>
                      <p><strong>Nome</strong></p>
                      <p>{model.Nome}</p>
                      <p><strong>Email</strong></p>
                      <p>{model.Email}</p>
                      <p><strong>Telefone</strong></p>
                      <p>{model.Telefone}</p>
                      <p><strong>Mensagem</strong></p>
                      <p>{model.Mensagem}</p>
                      </body>
                      </html>
                     ";

            try
            {
                await mailService.SendEmailAsync("formularios@mdb.mouradubeux.com.br", "Formulário Fala Conosco", htmlString, null);
            }
            finally
            {
                await JSRuntime.InvokeAsync<object>("showModal", "modal-success");
            }

            model = new ContatoFormModel();
        }
    }
}