﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;

namespace MouraDubeux.Front.Blazor.Pages.portfolio
{
    public partial class FiltroBuscaImovelPortfolio : ComponentBase
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }
        
        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Parameter]
        public string EstadoSlug { get; set; }

        List<CidadesModel> cidades { get; set; }

        List<EmpreendimentoModel> empreendimentoBairros { get; set; }

        BuscaImovelFormComponent model = new BuscaImovelFormComponent();

        List<EmpreendimentoModel> empreendimentosResidencial { get; set; }
        List<EmpreendimentoModel> empreendimentosEmpresarial { get; set; }
        List<EmpreendimentoModel> empreendimentosBeachclass { get; set; }

        string termo { get; set; } = "";
        string cidade { get; set; } = "";
        string bairro { get; set; } = "";
        string ano { get; set; } = "";


        protected override void OnParametersSet()
        {
            GetCidadesbyUf(EstadoSlug);
            GetBairros();
            GetEmpreendimentos();
            base.OnParametersSet();
        }

        protected void GetEmpreendimentos()
        {
            StringValues stringValues;
            var uri = navigationManager.ToAbsoluteUri(navigationManager.Uri);

            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("termo", out stringValues))
            {
                termo = stringValues.ToString();
            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("cidade", out stringValues))
            {
                cidade = stringValues.ToString();
            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("bairro", out stringValues))
            {
                bairro = stringValues.ToString();
            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("ano", out stringValues))
            {
                ano = stringValues.ToString();
            }

            List<EmpreendimentoModel> empreendimentos = new List<EmpreendimentoModel>();

            try
            {
                empreendimentos =
                frontDataService.GetEmpreendimento().Where(x =>
                    (x.IdiomaId == "pt") &&
                    /*(x.Ativo == true) &&*/
                    (x.Estado == EstadoSlug) &&
                    ((int)EnumStatusEmpreendimento.Portfolio == x.StatusEmpreendimento) &&
                    ((!string.IsNullOrEmpty(x.Nome) && !string.IsNullOrEmpty(termo) && x.Nome.IndexOf(termo, StringComparison.OrdinalIgnoreCase) >= 0) || string.IsNullOrEmpty(termo)) &&
                    ((!string.IsNullOrEmpty(x.Cidade) && !string.IsNullOrEmpty(cidade) && x.Cidade == cidade) || string.IsNullOrEmpty(cidade)) &&
                    ((!string.IsNullOrEmpty(x.Bairro) && !string.IsNullOrEmpty(bairro) && extensionMethods.CreateSlug(x.Bairro) == bairro) || string.IsNullOrEmpty(bairro)) &&
                    ((!string.IsNullOrEmpty(x.AnoEntrega) && !string.IsNullOrEmpty(ano) && x.AnoEntrega == ano) || string.IsNullOrEmpty(ano))
                ).ToList();

                empreendimentosResidencial = empreendimentos.Where(x => x.TipoEmpreendimento == (int)EnumTipoEmpreendimento.ResidencialCondominio || x.TipoEmpreendimento == (int)EnumTipoEmpreendimento.ResidencialIncorporacao).Take(6).ToList();
                empreendimentosEmpresarial = empreendimentos.Where(x => x.TipoEmpreendimento == (int)EnumTipoEmpreendimento.Empresarial).Take(6).ToList();
                empreendimentosBeachclass = empreendimentos.Where(x => x.TipoEmpreendimento == (int)EnumTipoEmpreendimento.BeachClass).Take(6).ToList();
            }
            catch (Exception)
            {
            }
        }

        protected void GetCidadesbyUf(string uf)
        {
            cidades = frontDataService.GetCidadesByEstado(uf);
        }

        protected void GetBairros()
        {
            try
            {
                empreendimentoBairros = frontDataService.GetBairros().Where(x => x.Estado == EstadoSlug).ToList();
            }
            catch (Exception)
            {
            }
        }

        protected void HandleValidSubmit()
        {
            var url = "/portfolio/" + EstadoSlug + "/busca?";
            var query = new Dictionary<string, string> {
                { "termo", model.Termo },
                { "cidade", model.Cidade },
                { "bairro", model.Bairro },
                { "ano", model.Ano}
            };
            navigationManager.NavigateTo(QueryHelpers.AddQueryString(url, query), true);
        }

        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            model.Bairro = "";
            try
            {
                empreendimentoBairros = frontDataService.GetBairros().Where(x => x.Cidade == e.Value.ToString()).ToList();

            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }

    }
}
