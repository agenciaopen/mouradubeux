﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.MdMidia;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.salaImprensa
{
    public partial class MouraDubeuxNaMidia
    {

        public List<MdMidiaModel> modelList { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetMdMidia();
            base.OnParametersSet();
        }

        private void GetMdMidia()
        {
            try
            {
                modelList = FrontDataService.GetMdMidia().Where(x => x.IdiomaId == "pt").ToList();

            }
            catch (Exception)
            {
            }
        }
    }
}
