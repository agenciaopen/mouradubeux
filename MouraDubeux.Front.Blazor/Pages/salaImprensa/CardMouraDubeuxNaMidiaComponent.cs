﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.MdMidia;
using Microsoft.JSInterop;
namespace MouraDubeux.Front.Blazor.Pages.salaImprensa
{
    public partial class CardMouraDubeuxNaMidiaComponent
    {
        [Parameter]
        public MdMidiaModel MdMidiaModel { get; set; }

        [Inject]
        IJSRuntime jsRuntime { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
