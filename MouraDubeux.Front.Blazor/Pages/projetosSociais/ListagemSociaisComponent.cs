﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Projeto;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.projetosSociais
{
    public partial class ListagemSociaisComponent
    {

        public List<ProjetoModel> modelList { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetProjetosSociais();
            base.OnParametersSet();
        }

        private void GetProjetosSociais()
        {
            try
            {
                modelList = FrontDataService.GetProjeto().Where(x => x.IdiomaId == "pt" && x.ProjetoSocial).ToList();
            }
            catch (Exception)
            {
            }
        }
    }
}
