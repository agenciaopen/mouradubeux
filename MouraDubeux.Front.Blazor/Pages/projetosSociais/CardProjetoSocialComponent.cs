﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Projeto;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.projetosSociais
{
    public partial class CardProjetoSocialComponent
    {
        [Parameter]
        public ProjetoModel ProjetoModel { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        [Inject]
        IJSRuntime jsRuntime { get; set; }
        public List<ProjetoGaleriaModel> ProjetoGaleriaList { get; set; }

        public string id = Guid.NewGuid().ToString();
        protected override void OnParametersSet()
        {
            GetProjetoGaleria(ProjetoModel.Id);

            base.OnParametersSet();
        }
        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender){
                jsRuntime.InvokeVoidAsync("initSociais", id);
                base.OnParametersSet();
            }        
        }

        private void initSociaisGaleria(){
            jsRuntime.InvokeVoidAsync("initSociaisGaleria", id);
        }
        protected void GetProjetoGaleria(Guid IdProjeto)
        {
            try
            {
                ProjetoGaleriaList = FrontDataService.GetProjetoGaleria(IdProjeto).Where(x => x.IdiomaId == "pt").ToList();
            }
            catch (Exception)
            {
            }
        }
    }
}
