﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Front.Blazor.Models;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class Empreendimento : ComponentBase
    {
        public FormEmpreendimentoModel model = new FormEmpreendimentoModel();

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        [Parameter]
        public string estado { get; set; }

        [Parameter]
        public string cidade { get; set; }

        [Parameter]
        public string bairro { get; set; }

        [Parameter]
        public string tipoEmpreendimento { get; set; }

        [Parameter]
        public string quartos { get; set; }

        [Parameter]
        public string slug { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        public EmpreendimentoModel empreendimentoModel { get; set; }

        public EmpreendimentoModel empreendimentoBannerModel { get; set; }


        [Inject]
        ILocalStorageService localStorageService { get; set; }

        string estadoNome { get; set; }

        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Inject]
        IEmpreendimentoDataService EmpreendimentoDataService { get; set; }

        [Inject]
        IMailService mailService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        public List<EmpreendimentoModel> EmpreendimentosRelacionados { get; set; }

        private void GetEstadoNome()
        {
            try
            {
                estadoNome = frontDataService.GetEstados(estado).FirstOrDefault().Nome;
            }
            catch (Exception)
            {
            }
        }


        protected override void OnParametersSet()
        {
            try
            {
                GetEmpreendimento(slug);

                if (!string.IsNullOrEmpty(estado))
                {
                    localStorageService.SetAsync("estado", estado);
                }
                GetEstadoNome();
                if (empreendimentoModel != null)
                {
                    List<Guid> idsEmpreendimentosRelacionados = EmpreendimentoDataService.GetEmpreendimentoRelacionado(empreendimentoModel.Id).ToList();
                    EmpreendimentosRelacionados = EmpreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt"
                    && idsEmpreendimentosRelacionados.Contains(x.Id) && x.Ativo).OrderByDescending(x => x.DataAlteracao).Take(3).ToList();
                }
                else
                    NavigationManager.NavigateTo("/conteudo-nao-encontrado", true);

            }
            catch (Exception e)
            {
                NavigationManager.NavigateTo("/conteudo-nao-encontrado", true);
            }

            base.OnParametersSet();
        }
        protected void GetEmpreendimento(string slug)
        {
            

                if (bairro != null)
                {
                    empreendimentoModel = FrontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && (x.Ativo == true)
                        && (x.Estado == estado || string.IsNullOrEmpty(estado)) && (x.Cidade == cidade || string.IsNullOrEmpty(cidade))
                        && ((!string.IsNullOrEmpty(bairro) || extensionMethods.CreateSlug(x.Bairro) == bairro)
                        && (x.Slug == slug || string.IsNullOrEmpty(slug)))).First();
                }
                empreendimentoBannerModel = null;

        }

        private async Task HandleValidSubmit()
        {
            string htmlString = @$"<html>
                      <body>
                      <h3>Ligamos para você</h3>
                      <h5>Nome</h5>
                      <p>{model.Nome}</p>
                      <h5>Telefone</h5>
                      <p>{model.Telefone}</p>
                      <h5>Email</h5>
                      <p>{model.TipoContato}</p>
                      </body>
                      </html>
                     ";
            try
            {
                await mailService.SendEmailAsync("formularios@mdb.mouradubeux.com.br", "Formulário Trabalhe Conosco", htmlString, null);
            }
            finally
            {
                await JSRuntime.InvokeAsync<object>("showModal", "modal-success");
            }
            model = new FormEmpreendimentoModel();
        }


    }
}