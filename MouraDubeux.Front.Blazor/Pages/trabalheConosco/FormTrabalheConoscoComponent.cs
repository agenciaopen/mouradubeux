﻿using BlazorInputFile;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Pages.trabalheConosco
{
    public partial class FormTrabalheConoscoComponent : ComponentBase
    {
        public FormTrabalheConoscoModel model = new FormTrabalheConoscoModel();
        public List<EstadosModel> estados { get; set; }
        public List<CidadesModel> cidades { get; set; } = new List<CidadesModel>();

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        ICidadesDataService cidadesDataService { get; set; }
        [Inject]
        IMailService mailService { get; set; }


        string status = "";
        const int MaxFileSize = 5 * 1024 * 1024;

        string fileName;
        string fileContent;
        string imageDataURL;

        Attachment att { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        private async Task HandleValidSubmit()
        {
            string htmlString = @$"<html>
                      <body>
                      <h3>Formulário Trabalhe Conosco</h3>
                      <p><strong>Nome</strong></p>
                      <p>{model.Nome}</p>
                      <p><strong>Email</strong></p>
                      <p>{model.Email}</p>
                      <p><strong>Telefone</strong></p>
                      <p>{model.Telefone}</p>
                      <p><strong>Estado</strong></p>
                      <p>{model.Estado}</p>
                      <p><strong>Cidade</strong></p>
                      <p>{model.Cidade}</p>
                      <p><strong>Endereco</strong></p>
                      <p>{model.Endereco}</p>
                      <p><strong>Mensagem</strong></p>
                      <p>{model.Mensagem}</p>
                      </body>
                      </html>
                     ";
            try
            {
                await mailService.SendEmailAsync("carlos.magalhaes@agenciaopen.com.br", "Formulário Trabalhe Conosco", htmlString, att);
                //await mailService.SendEmailAsync("formularios@mdb.mouradubeux.com.br", "Formulário Trabalhe Conosco", htmlString, att);
            }
            finally
            {
                await JSRuntime.InvokeAsync<object>("showModal", "modal-success");
            }
            model = new FormTrabalheConoscoModel();
        }

        async Task ViewFile(IFileListEntry[] files)
        {
            var file = files.FirstOrDefault();
            if (file == null)
            {
                //return;
            }
            else if (file.Size > MaxFileSize)
            {
                status = $"Favor escolher um arquivo menor do que { MaxFileSize } bytes.";
            }
            else
            {
                bool isOK = (file.Name.EndsWith(".pdf") || file.Name.EndsWith(".doc") || file.Name.EndsWith(".docx") || file.Name.EndsWith(".png") || file.Name.EndsWith(".jpeg") || file.Name.EndsWith(".jpg") || file.Name.EndsWith(".txt"));

                if (isOK)
                {
                    model.Curriculo = "anexo";
                    var ms = new MemoryStream();
                    await file.Data.CopyToAsync(ms);
                    fileName = file.Name;
                    ms.Seek(0, SeekOrigin.Begin);
                    att = new Attachment(ms, name: file.Name);
                    model.CurriculoOk = true;
                    await JSRuntime.InvokeAsync<object>("showModal", "modal-success-anexo-curriculo");
                }
                else
                {
                    model.Curriculo = "";
                    model.CurriculoOk = false;
                }
                StateHasChanged();
            }
        }

        protected override void OnInitialized()
        {

        }


        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                var aff = att;
            }
        }

        protected override void OnParametersSet()
        {
            try
            {
                estados = estadosDataService.GetEstados();
            }
            catch (Exception)
            {
            }
            base.OnParametersSet();
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            try
            {
                cidades = cidadesDataService.GetCidades("", e.Value.ToString());
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }
    }
}