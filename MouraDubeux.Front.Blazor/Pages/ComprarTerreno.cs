﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class ComprarTerreno
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }

        PaginaModel Pagina { get; set; }

        protected override void OnParametersSet()
        {
            GetPagina("venda-seu-terreno");
            base.OnParametersSet();
        }

        protected void GetPagina(string slugPagina)
        {
            try
            {
                Pagina = frontDataService.GetPagina(slugPagina).Where(x => x.IdiomaId == "pt").FirstOrDefault();
            }
            catch (Exception)
            {
            }
        }

    }
}
