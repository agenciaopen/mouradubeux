﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Enum;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Http.Extensions;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class Index : ComponentBase
    {
        [Parameter]
        public string estadoNome { get; set; }

        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Parameter]
        public string estado { get; set; }

        List<EmpreendimentoModel> empreendimentosVitrine { get; set; }
        public int[] OrdemVitrine { get; set; } = new int[6] { 4, 5, 1, 2, 3 ,6 };

        PaginaModel Pagina { get; set; }

        [Inject]
        IJSRuntime jSRuntime { get; set; }

        private bool firstRender { get; set; }

        protected override void OnParametersSet()
        {
            
            var showPopup = "";
            if (!string.IsNullOrEmpty(estado))
            {
                var uri = navigationManager.ToAbsoluteUri(navigationManager.Uri);
                localStorageService.SetAsync("estado", estado);
                if (estado == "pernambuco"){
                    jSRuntime.InvokeAsync<object>("getLocation", "");
                }
                /*StringValues stringValues;
                if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("geo", out stringValues))
                {
                    showPopup = stringValues.ToString();
                    if (showPopup == "true")
                    {
                        jSRuntime.InvokeAsync<object>("showModalGeolocalizacao", "");
                    }
                }*/
            }
            else
            {
                estado = "pernambuco";
                localStorageService.SetAsync("estado", estado);
                //navigationManager.NavigateTo("/pernambuco");
                /*busca ? geo = true*/
            }
                
            //GetPagina("home");
                
            
            base.OnParametersSet();
        }

        protected void GetBanners()
        {
            try
            {
                empreendimentosVitrine = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt"
                                                                             && (x.Ativo.Equals(true))
                                                                             && (x.StatusEmpreendimento != (int)EnumStatusEmpreendimento.Portfolio)
                                                                             && x.Estado == estado
                                                                             ).OrderByDescending(x => (x.DataCriacao, x.DestacarVitrine)).ToList();
            }
            catch (Exception)
            {
            }
         }
        protected async void GetEstados()
        {
            try
            {

                estado = await localStorageService.GetAsync("estado");
                estado = estado.ToString().Replace("\"", "");
                var listaEstados = frontDataService.GetEstados().Where(x => x.Slug.Contains(estado is not null ? estado : string.Empty));
                if (listaEstados.Any())
                {
                    estadoNome = frontDataService.GetEstados().Where(X => X.Slug.Contains(estado is not null ? estado : "")).FirstOrDefault().Nome;
                }
                StateHasChanged();
            }
            catch (Exception e)
            {
                var error = e.Message;
                navigationManager.NavigateTo("/conteudo-nao-encontrado");
            }

        }

        protected void GetPagina(string slugPagina)
        {
            Pagina = frontDataService.GetPagina(slugPagina).Where(x => x.IdiomaId == "pt").FirstOrDefault();
        }
        
    }
}
