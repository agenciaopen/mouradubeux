﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.listagemEmpreendimento
{
    public partial class CardComponent : ComponentBase
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }


        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
