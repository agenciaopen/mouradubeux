﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Front.Blazor.Data;

namespace MouraDubeux.Front.Blazor.Pages.listagemEmpreendimento
{
    public partial class FiltroComponent: ComponentBase
    {
        [Parameter]
        public string estadoNome { get; set; }

        public string estado = "";

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        protected override void OnParametersSet()
        {
            GetEstados();
            base.OnParametersSet();
        }

        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            navigationManager.NavigateTo("/" + e.Value.ToString(), true);
            StateHasChanged();
        }

        protected async void GetEstados()
        {
            try
            {
                estado = await localStorageService.GetAsync("estado");
                if (!string.IsNullOrEmpty(estado))
                {
                    estado = estado.ToString().Replace("\"", "");
                    estadoNome = frontDataService.GetEstados().Where(X => X.Slug.Contains(estado)).FirstOrDefault().Nome;
                }
                //estado = localStorageService.GetAsync("estado") is not null ? await localStorageService.GetAsync("estado") : "Pernambuco";
                //var listaEstados = frontDataService.GetEstados().Where(x => x.Slug.Contains(estado is not null ? estado : string.Empty));
                //if (listaEstados.Any())
                //{
                // if (!string.IsNullOrEmpty(estado))
                // {
                // estado = estado.ToString().Replace("\"", "");
                // estadoNome = frontDataService.GetEstados().Where(X => X.Slug.Contains(estado)).FirstOrDefault().Nome;
                // }
                //}
                // else{
                // estado = "pernambuco";
                // }
                StateHasChanged();
            }
            catch (Exception)
            {
                estado = "pernambuco";
            }
            //finally
            //{
            // if (!string.IsNullOrEmpty(estado))
            // {
            // estado = estado.ToString().Replace("\"", "");
            // estadoNome = frontDataService.GetEstados().Where(X => X.Slug.Contains(estado)).FirstOrDefault().Nome;
            // }
            // StateHasChanged();
            //}
        }
    }
}
