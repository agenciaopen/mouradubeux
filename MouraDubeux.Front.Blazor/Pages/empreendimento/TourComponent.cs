﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class TourComponent : ComponentBase
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
