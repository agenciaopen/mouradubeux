﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class InfoTecnicaComponent
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        public List<EmpreendimentoInformacoesTecnicasModel> EmpreendimentoInfoTecnicaList;

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetEmpreendimentoInfoTecnica(EmpreendimentoModel.Id);
            base.OnParametersSet();
        }

        protected void GetEmpreendimentoInfoTecnica(Guid IdEmpreendimento)
        {
            try
            {
                EmpreendimentoInfoTecnicaList = FrontDataService.GetEmpreendimentoInformacoesTecnicas(IdEmpreendimento).Where(x => x.IdiomaId == "pt").ToList();

            }
            catch (Exception)
            {
            }
        }
    }
}
