﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Front.Blazor.Data;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class BannerComponent
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        public EmpreendimentoModel EmpreendimentoBannerModel { get; set; }


        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        protected override void OnParametersSet()
        {

            base.OnParametersSet();
        }

    }
}
