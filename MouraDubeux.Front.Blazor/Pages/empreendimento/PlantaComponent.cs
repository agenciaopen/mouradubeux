﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class PlantaComponent
    {
        [Inject]
        IFrontDataService FrontDataService { get; set; }

        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        public List<EmpreendimentoGaleriaModel> EmpreendimentoGaleriaList { get; set; }

        protected override void OnParametersSet()
        {
            GetEmpreendimento(EmpreendimentoModel.Id);
            base.OnParametersSet();
        }

        protected void GetEmpreendimento(Guid IdEmpreendimento)
        {
            try
            {
                EmpreendimentoGaleriaList = FrontDataService.GetEmpreendimentoGaleria(IdEmpreendimento, (int)EnumTipoGaleria.Planta).Where(x => x.IdiomaId == "pt").ToList();
            }
            catch (Exception)
            {
            }
        }
    }
}
