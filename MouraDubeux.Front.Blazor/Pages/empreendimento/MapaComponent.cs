﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Front.Blazor.Data;
using System.Collections;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class MapaComponent : ComponentBase
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        public List<EmpreendimentoFichaTecnicaModel> EmpreendimentoFichaTecnicaLazerList { get; set; }
        public List<EmpreendimentoFichaTecnicaModel> EmpreendimentoFichaTecnicaInfraList { get; set; }
        public List<EmpreendimentoFichaTecnicaModel> EmpreendimentoFichaTecnicaServicosList { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        protected override void OnParametersSet()
        {
            GetEmpreendimentoFichaTecnica(EmpreendimentoModel.Id);
            base.OnParametersSet();
        }

        protected void GetEmpreendimentoFichaTecnica(Guid IdEmpreendimento)
        {
            try
            {
                EmpreendimentoFichaTecnicaLazerList = FrontDataService.GetEmpreendimentoFichaTecnica(IdEmpreendimento, (int)EnumTipoFichaTecnica.Lazer).Where(x => x.IdiomaId == "pt").ToList();
                EmpreendimentoFichaTecnicaInfraList = FrontDataService.GetEmpreendimentoFichaTecnica(IdEmpreendimento, (int)EnumTipoFichaTecnica.Infraestrutura).Where(x => x.IdiomaId == "pt").ToList();
                EmpreendimentoFichaTecnicaServicosList = FrontDataService.GetEmpreendimentoFichaTecnica(IdEmpreendimento, (int)EnumTipoFichaTecnica.ServicosProximos).Where(x => x.IdiomaId == "pt").ToList();
            }
            catch (Exception)
            {
            }
        }
    }
}
