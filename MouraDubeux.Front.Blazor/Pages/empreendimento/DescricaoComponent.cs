﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.empreendimento
{
    public partial class DescricaoComponent : ComponentBase
    {
        [Parameter]
        public EmpreendimentoModel EmpreendimentoModel { get; set; }

        public List<EmpreendimentoValorDescricaoModel> EmpreendimentoValorDescricaoList { get; set; }

        public EmpreendimentoValorDescricaoModel EmpreendimentoValorDescricaoModel { get; set; }

        String SelectIdValorDescricao { get; set; } = "";

        [Inject]
        IFrontDataService FrontDataService { get; set; }


        protected override void OnParametersSet()
        {
            GetEmpreendimentoValorDescricao(EmpreendimentoModel.Id);
            InitSelect();
            base.OnParametersSet();
        }


        protected void GetEmpreendimentoValorDescricao(Guid IdEmpreendimento)
        {
            try
            {
                EmpreendimentoValorDescricaoList = FrontDataService.GetEmpreendimentoValorDescricao(IdEmpreendimento).Where(x => x.IdiomaId == "pt").ToList();
            }
            catch (Exception)
            {
            }
        }
        protected void ValorDescricaoChange(ChangeEventArgs e)
        {
            SelectIdValorDescricao = e.Value.ToString();
            EmpreendimentoValorDescricaoModel = EmpreendimentoValorDescricaoList.Where(x => x.Id.Equals(new Guid(SelectIdValorDescricao))).First();
        }

        protected void InitSelect()
        {
            EmpreendimentoValorDescricaoModel = EmpreendimentoValorDescricaoList.First();
            SelectIdValorDescricao = EmpreendimentoValorDescricaoModel.Id.ToString();
        }
    }
}
