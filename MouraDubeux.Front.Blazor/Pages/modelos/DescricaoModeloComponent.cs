﻿using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Pages.modelos
{
    public partial class DescricaoModeloComponent : ComponentBase
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }

        List<EmpreendimentoModel> empreendimentosIncorporacao { get; set; }
        List<EmpreendimentoModel> empreendimentosAdministracao { get; set; }

        protected override void OnParametersSet()
        {
            GetDestaques();
            base.OnParametersSet();
        }
        protected async void GetDestaques()
        {
            try
            {
                empreendimentosIncorporacao = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && (x.Ativo == true) && x.ModeloNegocio == ((int)EnumModeloNegocio.Incorporacao).ToString()).ToList();
                empreendimentosAdministracao = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && (x.Ativo == true) && x.ModeloNegocio == ((int)EnumModeloNegocio.Administracao).ToString()).ToList();
            }
            catch (Exception)
            {
            }
              }
    }
}
