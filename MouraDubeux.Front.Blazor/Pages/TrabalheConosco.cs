﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class TrabalheConosco
    {

        [Inject]
        IFrontDataService frontDataService { get; set; }

        PaginaModel Pagina { get; set; }

       
        protected override void OnParametersSet()
        {
            GetPagina("trabalhe-conosco");
            base.OnParametersSet();
        }

        protected void GetPagina(string slugPagina)
        {
            try
            {
                Pagina = frontDataService.GetPagina(slugPagina).Where(x => x.IdiomaId == "pt").FirstOrDefault();

            }
            catch (Exception)
            {
            }
        }

    }
}
