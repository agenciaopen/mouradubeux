﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Front.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class ListaEmpreendimento : ComponentBase
    {
       
        public List<EmpreendimentoModel> modelList { get; set; }

        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Parameter]
        public string estado { get; set; }

        [Parameter]
        public string statusEmpreendimentoParam { get; set; }

        public string cidade { get; set; } = "";

        public string bairro { get; set; } = "";
        public string quartos { get; set; } = "";

        public string statusEmpreendimento { get; set; } = "";

        public string tipoEmpreendimento { get; set; } = "";
        public string modeloNegocio { get; set; } = "";

        public bool sazonal { get; set; } = false;



        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IFiltrosService filtrosService { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        string estadoNome { get; set; }

        protected override void OnParametersSet()
        {
            if (estado is not null && estado is "cliente-md") navigationManager.NavigateTo("/conteudo-nao-encontrado", forceLoad: true);            

            if (!string.IsNullOrEmpty(estado))
            {
                localStorageService.SetAsync("estado", estado);
            }
            GetEmpreendimentos();
            GetEstadoNome();
            //base.OnParametersSet();
        }

        protected bool BuscaEnumStatusEmpreendimento(string valorParametro)
        {
            return Enum.GetValues(typeof(EnumStatusEmpreendimento)).Cast<EnumStatusEmpreendimento>().ToList().Where(x => x.ToString() == valorParametro.Replace("-", "")).Count() > 0;
        }
        private void GetEmpreendimentos()
        {
            StringValues stringValues;
            var uri = navigationManager.ToAbsoluteUri(navigationManager.Uri);
            int TamanhoLista = uri.AbsolutePath.ToString().Split("/").Length;

            if (TamanhoLista is 4)
            {
                var cidadesModel = frontDataService.GetCidadesByEstado(estado).Where(x => x.Slug.Contains(statusEmpreendimentoParam)).FirstOrDefault();
                
                if (cidadesModel is not null)
                {
                    navigationManager.NavigateTo($"/{estado}/busca?&cidade={cidadesModel.Slug}&bairro=",true);
                }
            }

            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("cidade", out stringValues))
            {
                cidade = stringValues.ToString();
                localStorageService.SetAsync("cidade", cidade);
            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("bairro", out stringValues))
            {

                bairro = stringValues.ToString();
                localStorageService.SetAsync("bairro", bairro);

            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("quartos", out stringValues))
            {
                quartos = stringValues.ToString();
                localStorageService.SetAsync("quartos", quartos);
            }
            if (!string.IsNullOrEmpty(statusEmpreendimentoParam))
            {
                statusEmpreendimento = statusEmpreendimentoParam.ToString();
                localStorageService.SetAsync("statusEmpreendimento", statusEmpreendimentoParam);
            }
            else if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("statusEmpreendimento", out stringValues))
            {
                statusEmpreendimento = stringValues.ToString();
                localStorageService.SetAsync("statusEmpreendimento", statusEmpreendimento);

            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("tipoEmpreendimento", out stringValues))
            {
                tipoEmpreendimento = stringValues.ToString();
                localStorageService.SetAsync("tipoEmpreendimento", tipoEmpreendimento);

            }
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("modeloNegocio", out stringValues))
            {
                modeloNegocio = stringValues.ToString();
                localStorageService.SetAsync("tipoEmpreendimento", tipoEmpreendimento);

            }
            //verificação para empreendimento sazonal
            if (statusEmpreendimento == "sazonal")
            {
                statusEmpreendimento = "";
                sazonal = true;
            }

          

            filtrosService.SetFiltros(estado, cidade, quartos, bairro, statusEmpreendimento, tipoEmpreendimento, modeloNegocio);
            try
            {
                modelList = frontDataService.GetEmpreendimento().Where(x => (x.IdiomaId == "pt") &&
                                                    (x.Ativo == true) &&
                                                    (x.StatusEmpreendimento != (int)EnumStatusEmpreendimento.Portfolio) &&
                                                    ((!string.IsNullOrEmpty(x.ModeloNegocio) && !string.IsNullOrEmpty(modeloNegocio) && extensionMethods.CreateSlug(extensionMethods.Description((EnumModeloNegocio)Int32.Parse(x.ModeloNegocio))) == modeloNegocio) || string.IsNullOrEmpty(modeloNegocio)) &&
                                                    ((!string.IsNullOrEmpty(tipoEmpreendimento) && extensionMethods.CreateSlug(extensionMethods.Description((EnumTipoEmpreendimento)x.TipoEmpreendimento)) == tipoEmpreendimento) || string.IsNullOrEmpty(tipoEmpreendimento)) &&
                                                    ((!string.IsNullOrEmpty(x.Quartos) && !string.IsNullOrEmpty(quartos) && extensionMethods.CreateSlug(x.Quartos.ToLower()).Contains(quartos.ToLower())) || string.IsNullOrEmpty(quartos)) &&
                                                    (x.Estado == estado || string.IsNullOrEmpty(estado)) &&
                                                    (x.Cidade == cidade || string.IsNullOrEmpty(cidade)) &&
                                                    ((!string.IsNullOrEmpty(bairro) && extensionMethods.CreateSlug(x.Bairro) == bairro) || string.IsNullOrEmpty(bairro))
                                                    ).ToList();


                if (statusEmpreendimento == extensionMethods.CreateSlug(extensionMethods.Description(EnumStatusEmpreendimento.Lancamento)) ||
                    statusEmpreendimento == extensionMethods.CreateSlug(extensionMethods.Description(EnumStatusEmpreendimento.NaPlanta)))
                {

                    if (statusEmpreendimento == extensionMethods.CreateSlug(extensionMethods.Description(EnumStatusEmpreendimento.Lancamento)))
                    {
                        modelList = modelList.Where(x => x.StatusEmpreendimento == (int)EnumStatusEmpreendimento.Lancamento).ToList();
                    }

                    if (statusEmpreendimento == extensionMethods.CreateSlug(extensionMethods.Description(EnumStatusEmpreendimento.NaPlanta)))
                    {
                        modelList = modelList.Where(x => x.StatusEmpreendimento == (int)EnumStatusEmpreendimento.NaPlanta).ToList();
                    }
                }
                else
                {
                    modelList = modelList.Where(x =>
                    (!string.IsNullOrEmpty(statusEmpreendimento) &&
                    extensionMethods.CreateSlug(extensionMethods.Description((EnumStatusEmpreendimento)x.StatusEmpreendimento)) == statusEmpreendimento) ||
                    string.IsNullOrEmpty(statusEmpreendimento)).ToList();
                }

            }
            catch (Exception)
            {

            }

            if (sazonal)
            {
                modelList = modelList.Where(x => x.Sazonal).ToList();
            }

            var estadoTeste = filtrosService.GetEstado();

            StateHasChanged();
        }
        private void GetEstadoNome()
        {
            if (estadoNome != null)
            {
                try
                {
                    estadoNome = frontDataService.GetEstados(estado).FirstOrDefault().Nome;
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
