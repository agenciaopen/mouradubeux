﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Data.Escritorio;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.nossosEscritorios
{
    public partial class CardEscritorioComponent: ComponentBase
    {
        [Parameter]
        public EscritorioModel EscritorioModel { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
