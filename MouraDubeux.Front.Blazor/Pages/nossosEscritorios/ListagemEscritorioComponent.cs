﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Pages.nossosEscritorios
{
    public partial class ListagemEscritorioComponent
    {

        public List<EscritorioModel> modelList { get; set; }

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetEscritorios();
            base.OnParametersSet();
        }

        private void GetEscritorios()
        {
            try
            {
                modelList = FrontDataService.GetEscritorio().Where(x => x.IdiomaId == "pt").ToList();

            }
            catch (Exception)
            {
            }
        }
    }
}
