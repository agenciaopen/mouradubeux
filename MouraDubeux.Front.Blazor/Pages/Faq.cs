﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Faq;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;

namespace MouraDubeux.Front.Blazor.Pages
{
    public partial class Faq : ComponentBase
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }

        List<FaqModel> faqList { get; set; }
        List<FaqModel> imovelSobrePagamentosFinanciamentos { get; set; }
        List<FaqModel> imovelSobreNossoModeloDeNegocio { get; set; }
        List<FaqModel> imovelSobreImpostosTaxas { get; set; }
        List<FaqModel> clienteAcompanhamentoDaObra { get; set; }
        List<FaqModel> clientePersonalizacao { get; set; }
        List<FaqModel> clienteTributosImpostosHabiteSe { get; set; }

        protected override void OnParametersSet()
        {
            GetFaq();
            base.OnParametersSet();
        }

        protected void GetFaq()
        {
            try
            {
                faqList = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt").ToList();
                imovelSobrePagamentosFinanciamentos = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ImovelSobrePagamentosFinanciamentos).OrderBy(y => y.Secao).ToList();
                imovelSobreNossoModeloDeNegocio = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ImovelSobreNossoModeloDeNegocio).OrderBy(y => y.Secao).ToList();
                imovelSobreImpostosTaxas = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ImovelSobreImpostosTaxas).OrderBy(y => y.Secao).ToList();
                clienteAcompanhamentoDaObra = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ClienteAcompanhamentoDaObra).OrderBy(y => y.Secao).ToList();
                clientePersonalizacao = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ClientePersonalização).OrderBy(y => y.Secao).ToList();
                clienteTributosImpostosHabiteSe = frontDataService.GetFaq().Where(x => x.IdiomaId == "pt" && x.TipoFaq == (int)EnumFaq.ClienteTributosImpostosHabiteSe).OrderBy(y => y.Secao).ToList();
            }
            catch (Exception)
            {
            }
        }
    }
}