﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Pages.comprarTerreno
{
    public partial class FormCompraTerrenoComponent : ComponentBase
    {
        public FormCompraTerrenoModel model = new FormCompraTerrenoModel();
        public List<EstadosModel> estados { get; set; }
        public List<CidadesModel> cidades { get; set; } = new List<CidadesModel>();

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        ICidadesDataService cidadesDataService { get; set; }

        [Inject]
        IMailService mailService { get; set; }


        [Inject]
        IJSRuntime JSRuntime { get; set; }


        protected override void OnParametersSet()
        {
            try
            {
                estados = estadosDataService.GetEstados();
            }
            catch (Exception)
            {
            }
            base.OnParametersSet();
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            try
            {
                cidades = cidadesDataService.GetCidades("", e.Value.ToString());
            }
            catch (Exception)
            {
            }
            StateHasChanged();
        }

        private async Task HandleValidSubmit()
        {
            string htmlString = @$"<html>
                        <body>
                        <h3>Formulário Fala Conosco</h3>
                        <p><strong>Nome</strong></p>
                        <p>{model.Nome}</p>
                        <p><strong>Email</strong></p>
                        <p>{model.Email}</p>
                        <p><strong>Telefone</strong></p>
                        <p>{model.Telefone}</p>
                        <p><strong>Mensagem</strong></p>
                        <p>{model.Mensagem}</p>
                        <p><strong>Estado</strong></p>
                        <p>{model.Estado}</p>
                        <p><strong>Cidade</strong></p>
                        <p>{model.Cidade}</p>
                        <p><strong>Bairro</strong></p>
                        <p>{model.Bairro}</p>
                        <p><strong>Metragem</strong></p>
                        <p>{model.Metragem}</p>
                        <p><strong>Valor</strong></p>
                        <p>{model.Valor}</p>
                        </body>
                        </html>
                     ";

            try
            {
                await mailService.SendEmailAsync("formularios@mdb.mouradubeux.com.br", "Formulário CompraTerreno", htmlString, null);
            }
            finally
            {
                await JSRuntime.InvokeAsync<object>("showModal", "modal-success");
            }

            model = new FormCompraTerrenoModel();
        }
    }
}