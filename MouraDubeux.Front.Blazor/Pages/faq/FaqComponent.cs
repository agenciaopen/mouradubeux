﻿using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Pages.faq
{
    public partial class FaqComponent : ComponentBase
    {
        [Parameter]
        public List<FaqModel> faqList { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
