﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class ContatoFormModel
    {
        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Email é obrigatório.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Telefone é obrigatório.")]
        public string Telefone { get; set; }
        [Required(ErrorMessage = "Mensagem é obrigatório.")]
        public string Mensagem { get; set; }
    }
}
