﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class BuscaImovelFormComponent
    {
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Quartos { get; set; }
        public string Bairro { get; set; }
        public string StatusEmpreendimento { get; set; }
        public string TipoEmpreendimento { get; set; }
        public string ModeloNegocio { get; set; }
        public string Ano { get; set; }
        public string Termo { get; set; }
    }
}
