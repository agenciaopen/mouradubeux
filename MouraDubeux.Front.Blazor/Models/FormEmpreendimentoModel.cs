﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class FormEmpreendimentoModel
    {


        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Telefone é obrigatório.")]
        public string Telefone { get; set; }

        [Required(ErrorMessage = "Email é obrigatório.")]
        public string Email { get; set; }
        
        public string TipoContato { get; set; }

        [Required(ErrorMessage = "Email é obrigatório.")]
        public bool Aceito { get; set; }

        public string NomeImovel { get; set; }

    }
}
