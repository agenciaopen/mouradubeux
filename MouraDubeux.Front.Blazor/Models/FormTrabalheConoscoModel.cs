﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class FormTrabalheConoscoModel
    {
        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Email é obrigatório.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefone é obrigatório.")]
        public string Telefone { get; set; }

        [Required(ErrorMessage = "Estado é obrigatório.")]
        public string Estado { get; set; }

        [Required(ErrorMessage = "Cidade é obrigatório.")]
        public string Cidade { get; set; }

        [Required(ErrorMessage = "Endereco é obrigatório.")]
        public string Endereco { get; set; }

        [Required(ErrorMessage = "Mensagem é obrigatório.")]
        public string Mensagem { get; set; }

        [Required(ErrorMessage = "Curriculo é obrigatório.")]
        public string Curriculo { get; set; }

        public bool CurriculoOk { get; set; }

    }
}
