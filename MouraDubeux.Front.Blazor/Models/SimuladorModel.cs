﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class SimuladorModel
    {
        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Empresa é obrigatório.")]
        public string Empresa { get; set; }
        [Required(ErrorMessage = "Email é obrigatório.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Telefone é obrigatório.")]
        public string Telefone { get; set; }
        [Required(ErrorMessage = "Valor é obrigatório.")]
        public string Valor { get; set; }
        [Required(ErrorMessage = "Renda é obrigatório.")]
        public string Renda { get; set; }
        [Required(ErrorMessage = "Parcelas é obrigatório.")]
        public string Parcelas { get; set; }
    }
}
