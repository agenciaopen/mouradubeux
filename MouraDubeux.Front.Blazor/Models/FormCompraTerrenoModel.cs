﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Models
{
    public class FormCompraTerrenoModel
    {
        [Required(ErrorMessage = "Nome é obrigatório.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Email é obrigatório.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefone é obrigatório.")]
        public string Telefone { get; set; }

        [Required(ErrorMessage = "Estado é obrigatório.")]
        public string Estado { get; set; }

        [Required(ErrorMessage = "Cidade é obrigatório.")]
        public string Cidade { get; set; }

        [Required(ErrorMessage = "Bairro é obrigatório.")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Metragem é obrigatório.")]
        public string Metragem { get; set; }

        [Required(ErrorMessage = "Valor é obrigatório.")]
        public string Valor { get; set; }

        [Required(ErrorMessage = "Mensagem é obrigatório.")]
        public string Mensagem { get; set; }
    }
}
