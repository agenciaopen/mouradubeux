﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public interface IFiltrosService
    {
        void SetFiltros(string estado,
                        string cidade,
                        string quartos,
                        string bairro,
                        string statusEmpreendimento,
                        string tipoEmpreendimento,
                        string modeloNegocio);
        string GetEstado();
        string GetCidade();
        string GetQuartos();
        string GetBairro();
        string GetStatusEmpreendimento();
        string GetTipoEmpreendimento();
        string GetModeloNegocio();
    }
}
