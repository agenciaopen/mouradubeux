﻿using Microsoft.Extensions.Configuration;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.DataAccess;
using System;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using MouraDubeux.Front.Blazor.Data;

namespace MouraDubeux.Front.Blazor.Helpers
{
    public class UrlHelper
    {
        public static IConfiguration Configuration;

        /// <summary>
        /// Regular expression to get all paths from short URL path without "/". Moreover, the first and last "/" may or may not be present. 
        /// Example: from the string "path1/path2/path3" - path1, path2 and path3 will be selected
        /// </summary>
        private static Regex ShortUrlPathRegex = new(@"^((?:/?)([\w\s\.-]+)*)*/*", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);

        /// <summary>
        /// Retrieving the last path from short URL path based on a regular expression.
        /// </summary>
        /// <param name="path">Short URL path</param>
        /// <returns>Last path from short URL path</returns>
        public static string GetLastPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;
            try
            {
                var match = ShortUrlPathRegex.Match(path);
                if (!match.Success)
                    return string.Empty;
                return match.Groups[2].Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get data for SEO (title, keywords, description, canonical) depending on the path URL
        /// </summary>
        /// <param name="path">URL path</param>
        /// <returns>
        /// Tuple: 
        /// item1 - title 
        /// item2 - keywords 
        /// item3 - description 
        /// item4 - canonical
        /// </returns>
        public static (string, string, string, string) GetSeoData(string path)
        {
            string title = "Seja bem-vindo a Moura Dubeux";
            string keywords = "Apartamentos, Flats e Salas Comerciais nos estados de Alagoas, Bahia, Ceará, Pernambuco e Rio Grande do Norte. Conheça nossos empreendimentos.";
            string description = "Apartamentos, Flats e Salas Comerciais nos estados de Alagoas, Bahia, Ceará, Pernambuco e Rio Grande do Norte. Conheça nossos empreendimentos.";
            string canonical = "https://www.mouradubeux.com.br";
            string nomeEmpreendimento = "";
            string cidade = "";
            string EstadoPagina = "";

            if (string.IsNullOrWhiteSpace(path))
                return (title, keywords, description, canonical);


            if (!string.IsNullOrEmpty(path))
            {
                if (path.Equals("/") || path.Equals("/home"))
                {
                    if (path == "/home")
                    {
                        path = "/";
                    }
                    canonical = canonical + path;

                    return (title, keywords, description, canonical);
                }
            }


            var frontDataService = new FrontDataService(new SqlDataAccess(Configuration));

            // faz a verificação se é pagina no formato /home/sala-de-emprensa
            var capturaSlug = path.Split("/");
            if (capturaSlug.Length is 3 && capturaSlug[1] is "home")
            {
                path = $"{capturaSlug[0]}/{capturaSlug[2]}/";
            }
            else
            {
                var listaEstados = frontDataService.GetEstados().Where(x => x.Slug.Contains(capturaSlug[1] is not null ? capturaSlug[1] : string.Empty));
                if (listaEstados.Any() && capturaSlug.Length is 3)
                {
                    EstadoPagina = path;
                    path = $"{capturaSlug[0]}/{capturaSlug[2]}/";
                }
            }

            

            // -----------------
            var pagina = frontDataService.GetPagina(UrlHelper.GetLastPath(path.Split('/')[1])).Where(x => x.IdiomaId == "pt");

            var empreendimento = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && x.Slug == UrlHelper.GetLastPath(path));

            var estado = frontDataService.GetEstados().Where(x => x.Slug == path.Split('/')[1]);

            // Parte a url e coloca em uma lista
            var urlCompletaParticionada = path.Split('/');

            // Verifica se tem 8 posições

            if (urlCompletaParticionada.Length > 5)
            {
                int indice = urlCompletaParticionada.Length == 8 ? 6 : 4;
                // nome do empreendimento
                nomeEmpreendimento = urlCompletaParticionada[indice].Replace("-", " ");
                // cidade
                cidade = urlCompletaParticionada[2].Replace("-", " ");

            }



            if (path == "/home")
            {
                path = "/";
            }
            canonical += path;

            // Verifica se retornou algum empreendimento
            if (empreendimento.Any())
            
            {
                if (urlCompletaParticionada.Length > 5)
                {
                    title = CultureInfo.CurrentCulture.TextInfo.ToTitleCase("Conheça " + nomeEmpreendimento + " - " + cidade);
                }
                else
                    title = empreendimento.FirstOrDefault().MataTittle;
                keywords = empreendimento.FirstOrDefault().MetaKeyword;
                description = empreendimento.FirstOrDefault().MetaDescription;
                if (empreendimento.FirstOrDefault().MetaKeyword == null && empreendimento.FirstOrDefault().MetaDescription == null)
                {
                    keywords = "Apartamentos, Flats e Salas Comerciais nos estados de Alagoas, Bahia, Ceará, Pernambuco e Rio Grande do Norte. Conheça nossos empreendimentos.";
                    description = $"{title.Replace("-","em")} e saiba mais deste maravilhoso empreendimento.";
                }
                return (title, keywords, description, canonical);
            }
            else
            {
                RetornaSeoPendente retornaTitle = new();
                if (pagina.Any())
                {
                    var achouTitle = (pagina.Select(x => x.MataTittle is not null ? x.MataTittle : string.Empty).ToList()[0].ToString());
                    if (achouTitle.Length == 0)
                    {
                        var tags = retornaTitle.GetTags(path, urlCompletaParticionada[1]);
                        if (tags != null)
                        {
                            title = tags.Title;
                            keywords = tags.Keywords;
                            description = tags.Description;
                        }
                        else
                        {
                            title = pagina.FirstOrDefault().MataTittle;
                            keywords = pagina.FirstOrDefault().MetaKeyword;
                            description = pagina.FirstOrDefault().MetaDescription;
                        }
                    }
                    else
                    {
                        title = pagina.FirstOrDefault().MataTittle;
                        keywords = pagina.FirstOrDefault().MetaKeyword;
                        description = pagina.FirstOrDefault().MetaDescription;

                    }
                }
                else
                    {
                    var tags = retornaTitle.GetTags(EstadoPagina, capturaSlug[1]);
                    if (tags != null)
                    {
                        title = tags.Title;
                        keywords = tags.Keywords;
                        description = tags.Description;
                    }
                    else
                    {
                        title = pagina.FirstOrDefault().MataTittle;
                        keywords = pagina.FirstOrDefault().MetaKeyword;
                        description = pagina.FirstOrDefault().MetaDescription;
                    }
                }
                return (title, keywords, description, canonical);
            }
        }

        private static string RetornaTitle(IFrontDataService frontDataService, string title, string slug, string idiomaId)
        {
            return frontDataService.UpdateMetaTags(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title), slug, idiomaId);
        }

    }

}