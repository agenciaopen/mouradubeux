﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public interface ILocalStorageService
    {
        Task SetAsync<T>(string key, T value);

        Task<string> GetAsync(string key);


        Task RemoveAsync(string key);

        Task ClearAsync();
       
    }
}
