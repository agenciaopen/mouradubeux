﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public class ExtensionMethods : IExtensionMethods
    {
        public string Description(Enum enumValue)
        {
            var descriptionAttribute = enumValue.GetType()
                .GetField(enumValue.ToString())
                .GetCustomAttributes(false)
                .SingleOrDefault(attr => attr.GetType() == typeof(DescriptionAttribute)) as DescriptionAttribute;

            // return description
            return descriptionAttribute?.Description ?? "";
        }
        public string RemoverAcentos(string texto)
        {
            if (!string.IsNullOrEmpty(texto))
            {
                string s = texto.Normalize(NormalizationForm.FormD);

                StringBuilder sb = new StringBuilder();

                for (int k = 0; k < s.Length; k++)
                {
                    UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                    if (uc != UnicodeCategory.NonSpacingMark)
                    {
                        sb.Append(s[k]);
                    }
                }
                return sb.ToString();
            }
            return "";
        }
        public string RemoveCaracteresEspeciais(string str)
        {
            if (str == null || str.Trim() == String.Empty)
                return String.Empty;

            // Remove espaços duplos da string
            while (str.IndexOf("  ") > 0)
                str = str.Replace("  ", " ");

            str = str.Replace(" - ", " ");
            str = str.Replace("à", "a").Replace("á", "a").Replace("â", "a").Replace("ã", "a");
            str = str.Replace(" + ", "+");
            str = str.Replace("é", "e").Replace("ê", "e").Replace("í", "i").Replace("î", "i");
            str = str.Replace("ó", "o").Replace("ô", "o").Replace("õ", "o").Replace("ú", "u").Replace("û", "u");
            str = str.Replace("À", "A").Replace("Á", "A").Replace("Â", "A").Replace("Ã", "A");
            str = str.Replace("É", "E").Replace("Ê", "E").Replace("Í", "I").Replace("Î", "I");
            str = str.Replace("Ó", "O").Replace("Ô", "O").Replace("Õ", "O").Replace("Ú", "U").Replace("Û", "U");
            str = str.Replace("ç", "c").Replace("Ç", "C");
            str = str.Replace("%", "").Replace("´", "").Replace("`", "").Replace("~", "").Replace("^", "").Replace("&", "");
            str = str.Replace("\"", "").Replace("'", "").Replace("@", "").Replace("#", "").Replace("$", "").Replace("!", "").Replace("*", "").Replace("(", "").Replace(")", "");
            str = str.Replace("\t", "").Replace("\r", "").Replace("\n", "").Replace("¹", "1").Replace("²", "2").Replace("³", "3");
            str = str.Replace("\\", "").Replace("º", "o").Replace("ª", "a").Replace("/", "").Replace("//", "").Replace(".", "-");

            return str.Trim();
        }
        public string CreateSlug(string str)
        {
            var slug = RemoverAcentos(str);

            slug = RemoveCaracteresEspeciais(slug);

            slug = slug.Replace(" ", "-").ToLower();
            return slug;
        }
    }
}
