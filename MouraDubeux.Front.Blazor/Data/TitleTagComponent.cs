﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System.Web;

namespace MouraDubeux.Front.Blazor.Components
{
    public class TitleTagComponent : ComponentBase
    {
        [Parameter]
        public string Content { get; set; }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            builder.OpenElement(0, "title");
            builder.AddContent(1, HttpUtility.HtmlDecode(Content) ?? string.Empty);
            builder.CloseElement();
        }

    }
}