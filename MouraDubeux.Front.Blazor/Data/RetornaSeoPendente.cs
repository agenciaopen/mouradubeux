﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public class RetornaSeoPendente
    {
        static readonly List<string> listaEstados = new() { "pernambuco", "ceara", "alagoas", "banhia", "rio-grande-do-norte" };
        private readonly List<string> guardaTermos;
        public RetornaSeoPendente()
        {
            guardaTermos = ListaSlug.Retorna();
            
        }
        
        public class Tags
        {
            
            public string Title { get; set; } = "Conteúdo não encontrado – Moura Dubeux";
            public string Keywords { get; set; } = "Conteúdo não encontrado, erro 404, página não encontrada";
            public string Description { get; set; } = "Conteúdo não encontrado, pesquise novamente";
            public string Canonical { get; set; }= "https://www.mouradubeux.com.br";
        }

        public Tags GetTags(string path, string estado)
        {
            string verificaContemEstado1posicao = "";
            //[0]->dominio/[1]->estado/[2]-> pagina
            var pathLista = path.Split("/");
            if (pathLista.Count() > 2)
            {
                verificaContemEstado1posicao = estado;
                path = pathLista[2];
            }
            else
            {
                if (!VerificaEstadoUrl(estado))
                {
                    path = pathLista.Length==2? pathLista[1] : estado;
                }
                else
                    return null;

            }


            //verificaContemEstado1posicao = path.Split("/")[1];

            //if (!VerificaEstadoUrl(estado))
            //{
            //    verificaContemEstado1posicao = path.Split("/")[1];
            //}
            //else


            
            //if (string.IsNullOrEmpty(estado))
            //    verificaContemEstado1posicao = path.Split("/")[1];
            //else
            //    verificaContemEstado1posicao = path.Split("/")[2];
            //if (listaEstados.Contains(verificaContemEstado1posicao))
            //    path = "_estado";
            //else
            //    path = verificaContemEstado1posicao;
            Tags tags = new();
            switch (GetTipo(path))
            {
                //somente estado
                case "area-do-cliente":
                    tags.Title = "Área do cliente – Moura Dubeux";
                    tags.Keywords = "area do cliente moura dubeux, login dubeux";
                    tags.Description = "Tenha informações importantes como financeiro, dúvidas frequentes e saiba como personalizar em nossa página de atendimento ao cliente.";
                    return tags;
                //somente estado
                case "faq":
                    tags.Title = "faq - Dúvidas frequentes de nossos clientes – Moura Dubeux";
                    tags.Keywords = "faq moura dubeux,Dúvidas frequentes moura";
                    tags.Description = "Acesse nosso faq e tire suas dúvidas mais frequentes sobre nossos empreendimentos.";
                    return tags;
                //em-construcaoarea-do-cliente
                case "como-investir":
                    tags.Title= "Saiba como investir – Moura Dubeux";
                    tags.Keywords = "Como Investir, Investir, Apartamentos, Flats e Salas Comerciais nos estados de Alagoas, Bahia, Ceará, Pernambuco e Rio Grande do Norte. Conheça nossos empreendimentos.";
                    tags.Description = "Para quem está começando a investir, esse universo pode parecer intimidador. Nesta página, vamos explicar alguns dos principais conceitos que você precisa conhecer.";
                    return tags;
                    //em-construcao
                case "em-construcao":
                    tags.Title= $"Empreendimento em construção em  {estado} – Moura Dubeux";
                    tags.Keywords = $"Empreendimento em construção em  {estado},  Apartamentos, Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description =$"Veja nossos Empreendimento em construção em  {estado}, Apartamentos, Flats e Salas Comerciais. Conheça nossos empreendimentos.";
                    return tags;
                //na planta
                case "na-planta":
                    tags.Title = $"Empreendimento na planta em  {estado} – Moura Dubeux";
                    tags.Keywords = $"Empreendimento na planta em  {estado},  Apartamentos, Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description = $"Veja nossos Empreendimento na planta em {estado}, Apartamentos, Flats e Salas Comerciais. Conheça nossos empreendimentos.";
                    return tags;
                //busca
                case "busca":                   
                    tags.Title = $"Resultado da Busca de empreendimento em  {estado} – Moura Dubeux";
                    tags.Keywords = $"Empreendimentos encontrado em {estado},  Apartamentos, Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description = $"Veja Empreendimentos encontrados em {estado}, Apartamentos, Flats e Salas Comerciais. Conheça nossos empreendimentos.";
                    return tags;
                //pronto
                case "pronto":                    
                    tags.Title = $"Empreendimento prontos em  {estado} – Moura Dubeux";
                    tags.Keywords = $"Empreendimentos prontos em {estado},  Apartamentos, Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description = $"Veja Empreendimentos que estão prontos em {estado}, Apartamentos, Flats e Salas Comerciais. Conheça nossos empreendimentos.";
                    return tags;
                //Revenda
                case "revenda":
                    tags.Title = $"Encontre um imóvel para revenda em {estado} - Moura Dubeux";
                    tags.Keywords = $"Revenda de imóveis em {estado},  Apartamentos, Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description = $"Encontre revendas de imóveis em {estado}, temos escritórios em 5 estados brasileiros, com consultores prontos para te ajudar a decidir qual dos nossos imóveis atende às suas necessidades.";
                    return tags;
                //lançamento
                case "lancamento":                  
                    tags.Title = $"Confira os lançamentos de empreendimentos em {estado} - Moura Dubeux";
                    tags.Keywords = $"lançamentos em {estado}, lançamentos de Apartamentos em {estado}, lançamentos de Flats e Salas Comerciais no estado de {estado}, Conheça nossos empreendimentos.";
                    tags.Description = $"Veja nossos lanaçamentos em {estado}, Apartamentos, Flats e Salas Comerciais. Conheça nossos empreendimentos.";
                    return tags;
                //cliente-md/atualizacao-de-cadastro
                case "atualizacao-de-cadastro":
                    tags.Title = "Atualização de cadastros – Moura Dubeux";
                    tags.Keywords = "Atualização de cadastros, Atualizar cadastros, Atualização de cadastros Moura, Atualização de cadastros Moura Dubeux";
                    tags.Description = "Faça a atualização de cadastros em nosso sistema e fique atualizado de nossos empreendimentos";
                    return tags;

                //empreendimentos
                case "empreendimentos":
                    tags.Title = "Faça uma busca por empreendimentos em nosso site – Moura Dubeux";
                    tags.Keywords = "Apartamentos, Flats e Salas Comerciais";
                    tags.Description = "Faça uma busca em nosso site por Apartamentos, Flats e Salas Comerciais, por Estado, Cidade, Bairro, por Tipo e encpntr";
                    return tags;
                //cliente-md/canais-de-atendimento
                case "canais-de-atendimento":
                    tags.Title = "Canais de atendimento – Moura Dubeux";
                    tags.Keywords = "Canais de atendimento Moura, Canais de atendimento Moura Dubeux, Fale conosco";
                    tags.Description = "Use nossos canais de atendimento para tirar suas dúvidas, pedir mais informações, fazer sugestões e muito mais." +
                        " Acesse e escolha o canal de atendimento mais adequado";
                    return tags;
                //conteudo-nao-encontrado
                case "conteudo-nao-encontrado":                   
                    tags.Title = "Conteúdo não encontrado – Moura Dubeux";
                    tags.Keywords = "Conteúdo não encontrado, erro 404, página não encontrada";
                    tags.Description = "Conteúdo não encontrado, pesquise novamente";
                    return tags;
                //personaliza
                case "personalize":
                    tags.Title = "Deixe o seu apartamento com a sua cara. – Moura Dubeux";
                    tags.Keywords = "personalizar apartamento, personalizar flat, personalizar";
                    tags.Description = "Deixe o apartamento com a sua cara com nossa ferramenta de personalização online.";
                    return tags;
                ////portfolio
                //case "portfolio":
                //    tags.Title = "Conheça o portfólio da maior incorporadora do Nordeste – Moura Dubeux";
                //    tags.Keywords = "portfólio, nosso portfólio";
                //    tags.Description = "Com mais de 35 anos de história, temos uma trajetória que atravessa os sonhos de milhares de pessoas. Construímos uma presença sólida em cinco estados e suas principais cidades: Alagoas, Bahia, Ceará, Pernambuco e Rio Grande do Norte.";
                //    return tags;
                //portfolio
                case "mapa-do-site":
                    tags.Title = "Mapa do site – Moura Dubeux";
                    tags.Keywords = "Mapa do site moura, Mapa do site moura dubeux";
                    tags.Description = "Confira nosso mapa do site e encontre com facilidade o conteúdo que deseja ";
                    return tags;
            }
            return tags;
        }


        public static bool VerificaEstadoUrl(string path)
        {
            string verificaContemEstdao = path;
            return listaEstados.Contains(verificaContemEstdao);
        }


        public string GetTipo(string path)
        {
            foreach (var item in guardaTermos)
            {
                if (path.Contains(item))
                    return item;
            }
            return string.Empty;
        }
    }
}
