﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public class FiltrosService : IFiltrosService
    {
        public FiltrosService()
        {
        }

        private string Estado { get; set; }
        private string Cidade { get; set; }
        private string Quartos { get; set; }
        private string Bairro { get; set; }
        private string StatusEmpreendimento { get; set; }
        private string TipoEmpreendimento { get; set; }
        private string ModeloNegocio { get; set; }
        public void SetFiltros(string estado, 
                               string cidade,
                               string quartos, 
                               string bairro, 
                               string statusEmpreendimento, 
                               string tipoEmpreendimento, 
                               string modeloNegocio)
        {
            Estado = estado;
            Cidade = cidade;
            Quartos = quartos;
            Bairro = bairro;
            StatusEmpreendimento = statusEmpreendimento;
            TipoEmpreendimento = tipoEmpreendimento;
            ModeloNegocio = modeloNegocio;
        }

        public string GetEstado()
        {
            return Estado;
        }

        public string GetCidade()
        {
            return Cidade;
        }
        public string GetQuartos()
        {
            return Quartos;
        }
        public string GetBairro()
        {
            return Bairro;
        }
        public string GetStatusEmpreendimento()
        {
            return StatusEmpreendimento;
        }
        public string GetTipoEmpreendimento()
        {
            return TipoEmpreendimento;
        }
        public string GetModeloNegocio()
        {
            return ModeloNegocio;
        }
    }
}
