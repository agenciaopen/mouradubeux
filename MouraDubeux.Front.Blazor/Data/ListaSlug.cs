﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Data
{
    public class ListaSlug
    {
        public static List<string> Retorna() =>  new()
        {
            "empreendimentos",
            "em-construcao",
            "na-planta",
            "busca",
            "pronto",
            "revenda",
            "lancamento",
            "atualizacao-de-cadastro",
            "canais-de-atendimento",
            "conteudo-nao-encontrado",
            "personalize",
            "area-do-cliente",
            //"portfolio",
            "como-investir",
            "faq",
            "mapa-do-site"
        };
        
    }
}
