﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System.Web;

namespace MouraDubeux.Front.Blazor.Components
{
    public class DescriptionMetaTagComponent : ComponentBase
    {
        [Parameter]
        public string Content { get; set; }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            builder.OpenElement(0, "meta");
            builder.AddAttribute(1, "name", "description");
            builder.AddAttribute(2, "content", HttpUtility.HtmlDecode(Content) ?? string.Empty);
            builder.CloseElement();
        }   

    }
}