﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Front.Blazor.Data;



namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class NotFound404 : ComponentBase
    {

        [Inject]
        IFiltrosService filtrosService { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        string SelectedEstado { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }

        protected void ClickButton()
        {
            filtrosService.SetFiltros(SelectedEstado, "", "", "", "", "", "");
            localStorageService.SetAsync("estado", SelectedEstado);
            /*var val = await _localStorage.GetAsync<int>("Test");
            await _localStorage.RemoveAsync("Test");
            await _localStorage.ClearAsync();*/
            navigationManager.NavigateTo("/" + SelectedEstado, true);
        }

    }
}
