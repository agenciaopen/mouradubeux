﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class MetadadosComponent : ComponentBase
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Parameter]
        public PaginaModel Pagina { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
