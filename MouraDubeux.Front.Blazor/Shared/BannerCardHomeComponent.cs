﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Admin.Data.Enum;

namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class BannerCardHomeComponent : ComponentBase
    {
        [Inject]
        IFrontDataService frontDataService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Parameter]
        public string estado { get; set; }

        [Parameter]
        public string statusEmpreendimento { get; set; }

        List<EmpreendimentoModel> empreendimentosHome { get; set; }

        protected override void OnParametersSet()
        {
            GetBanners();
            base.OnParametersSet();
        }

        protected void GetBanners()
        {
            try
            {
                if (!string.IsNullOrEmpty(statusEmpreendimento))
                {
                    empreendimentosHome = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && (x.StatusEmpreendimento != (int)EnumStatusEmpreendimento.Portfolio) && (x.Ativo == true) && x.DestacarStatus).ToList();

                }
                else
                {
                    empreendimentosHome = frontDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt" && (x.StatusEmpreendimento != (int)EnumStatusEmpreendimento.Portfolio) && (x.Ativo == true) && x.DestacarHome).OrderBy(x => x.OrdemBanner).ToList();
                }
                if (!string.IsNullOrEmpty(estado))
                {
                    empreendimentosHome = empreendimentosHome.Where(x => x.IdiomaId == "pt" && (x.Ativo == true) && (x.StatusEmpreendimento != (int)EnumStatusEmpreendimento.Portfolio) && x.Estado == estado).ToList();
                }
            }
            catch (Exception)
            {
            }
            
        }

    }
}
