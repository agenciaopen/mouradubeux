﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Front.Blazor.Data;
using System.Collections;
using MouraDubeux.Front.Blazor.Models;
using MouraDubeux.Admin.Data.Models.Cidades;
using Microsoft.AspNetCore.WebUtilities;

namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class BuscaImovelComponent : ComponentBase
    {

        [Inject]
        IFrontDataService FrontDataService { get; set; }

        BuscaImovelFormComponent model = new BuscaImovelFormComponent();

        [Parameter]
        public EventCallback FiltroCallBack { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IFiltrosService filtrosService { get; set; }

        [Parameter]
        public string estado { get; set; }

        [Parameter]
        public string cidade { get; set; }

        [Parameter]
        public string bairro { get; set; }

        [Parameter]

        public string quartos { get; set; }

        [Parameter]

        public string statusEmpreendimento { get; set; } 

        [Parameter]

        public string tipoEmpreendimento { get; set; }

        [Parameter]
        public string modeloNegocio { get; set; } 


        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        List<EmpreendimentoModel> empreendimentoBairros { get; set; }
        List<EmpreendimentoModel> empreendimentoQuartos { get; set; }
        List<CidadesModel> cidades { get; set; }


        protected override void OnParametersSet()
        {
            GetQuartos(cidade, bairro);
            GetCidadesbyUf(estado);
            GetBairros();
            model.Estado = estado;
            model.Cidade = cidade;
            model.Bairro = bairro;
            model.Quartos = quartos;
            model.StatusEmpreendimento = statusEmpreendimento;
            model.TipoEmpreendimento = tipoEmpreendimento;
            model.ModeloNegocio = modeloNegocio;


            base.OnParametersSet();
        }
        protected void GetCidadesbyUf(string uf)
        {
            cidades = FrontDataService.GetCidadesByEstado(uf);
        }
        protected void GetBairros()
        {
            try
            {
                if (string.IsNullOrEmpty(cidade))
                {
                    empreendimentoBairros = new List<EmpreendimentoModel>();
                }
                else if (!string.IsNullOrEmpty(cidade))
                {
                    empreendimentoBairros = FrontDataService.GetBairros().Where(x => x.Cidade == cidade).ToList();
                }
                else if (string.IsNullOrEmpty(estado))
                {
                    empreendimentoBairros = FrontDataService.GetBairros().Where(x => x.Estado == estado).ToList();
                }
            }
            catch (Exception)
            {
            }
        }
        protected void GetQuartos(string cidade, string bairro)
        {
            try
            {
                if (!string.IsNullOrEmpty(cidade) || !string.IsNullOrEmpty(bairro))
                {
                    empreendimentoQuartos = FrontDataService.GetQuartosByCidadeBairro(cidade, bairro);
                }
                else 
                {
                    empreendimentoQuartos = FrontDataService.GetQuartos();
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        protected void HandleFirstDropDownCidadeChange(ChangeEventArgs e)
        {
            model.Bairro = "";
            localStorageService.SetAsync("cidade", e.Value.ToString());
            empreendimentoBairros = FrontDataService.GetBairros().Where(x => x.Cidade == e.Value.ToString()).ToList();
            GetQuartos(e.Value.ToString(), "");
            StateHasChanged();
        }

        protected void HandleFirstDropDownBairroChange(ChangeEventArgs e)
        {
            var bairro = "";
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                bairro = empreendimentoBairros.Where(x => extensionMethods.CreateSlug(x.Bairro) == e.Value.ToString()).FirstOrDefault().Bairro;
            }

            GetQuartos(model.Cidade, bairro);
            StateHasChanged();
        }
        protected void HandleValidSubmit()
        {
            filtrosService.SetFiltros(estado, model.Cidade, model.Quartos, model.Bairro, model.StatusEmpreendimento, model.TipoEmpreendimento, "");
            var url = "/" + estado + "/busca?";

            localStorageService.SetAsync("estado", estado);
            localStorageService.SetAsync("cidade", model.Cidade);
            localStorageService.SetAsync("bairro", model.Bairro);
            localStorageService.SetAsync("quartos", model.Quartos);
            localStorageService.SetAsync("statusEmpreendimento", model.StatusEmpreendimento);
            localStorageService.SetAsync("tipoEmpreendimento", model.TipoEmpreendimento);
            localStorageService.SetAsync("modeloNegocio", model.ModeloNegocio);

            var query = new Dictionary<string, string> {
                { "cidade", model.Cidade },
                { "bairro", model.Bairro },
                { "quartos", model.Quartos },
                { "statusEmpreendimento", model.StatusEmpreendimento },
                { "tipoEmpreendimento", model.TipoEmpreendimento },
                { "modeloNegocio", model.ModeloNegocio }

            };
            navigationManager.NavigateTo(QueryHelpers.AddQueryString(url, query), true);
        }
    }
}
