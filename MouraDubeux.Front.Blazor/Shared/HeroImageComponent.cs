﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Front.Blazor.Data;
using System.Collections;
using MouraDubeux.Front.Blazor.Models;

namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class HeroImageComponent : ComponentBase
    {

        [Parameter]
        public string bannerUrl { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();
        }
    }
}
