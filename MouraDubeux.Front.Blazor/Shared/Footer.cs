﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Front.Blazor.Data;
using MouraDubeux.Front.Blazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Front.Blazor.Shared
{
    public partial class Footer : ComponentBase
    {
        public string estado { get; set; }

         private Newsletter model = new Newsletter();

        private string modelGeolocation = "";

        public string estadoInput = "";

        [Inject]
        IMailService mailService { get; set; }

        [Inject]
        ILocalStorageService localStorageService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IFrontDataService frontDataService { get; set; }

        private async Task HandleValidSubmit()
        {
            model.Estado = estado;
            string htmlString = @$"<html>
                    <body>
                    <h3>Formulário Newsletter</h3>
                    <p><strong>Nome</strong></p>
                    <p>{model.Nome}</p>
                    <p><strong>Email</strong></p>
                    <p>{model.Email}</p>
                    <p><strong>Telefone</strong></p>
                    <p>{model.Telefone}</p>
                    <p><strong>Estado</strong></p>
                    <p>{model.Estado}</p>
                    </body>
                    </html>
                    ";

            try
            {
                await mailService.SendEmailAsync("formularios@mdb.mouradubeux.com.br", "Formulário Newsletter", htmlString, null);
            }
            finally
            {
                await JSRuntime.InvokeAsync<object>("hideModal", "modalNewsletterRD");
                await JSRuntime.InvokeAsync<object>("hideModal", "modalNewsletter");
                await JSRuntime.InvokeAsync<object>("showModal", "modal-success");
            }

            model = new Newsletter();
        }
        private async Task HandleAceiteGeolocation()
        {
       /*     try
            {
                EstadosModel estadoModel = frontDataService.GetEstados().Where(x => x.UF == estadoInput).FirstOrDefault();
                if(estadoModel != null)
                {
                    List<string> estadosArray = new List<string>{ "alagoas", "bahia", "ceara", "pernambuco", "rio-grande-do-norte" };
                    if (estadosArray.Contains(estadoModel.Slug))
                    {
                        await localStorageService.SetAsync("estado", estadoModel.Slug);
                        navigationManager.NavigateTo("/" + estadoModel.Slug);
                    }
                    else
                    {
                        await localStorageService.SetAsync("estado", "pernambuco");
                        navigationManager.NavigateTo("/pernambuco");
                        //navigationManager.NavigateTo("/alagoas");
                    }
                }
            }
            finally
            {
               await JSRuntime.InvokeAsync<object>("hideModal", "modalGeolocation");
            }*/
        }
        protected async void GetEstados()
        {
            try
            {
                estado = await localStorageService.GetAsync("estado");
                estado = estado.ToString().Replace("\"", "");
                StateHasChanged();
            }
            catch (Exception)
            {

            }
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {

            // await JSRuntime.InvokeVoidAsync("loadScript", "https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js");
            // await JSRuntime.InvokeVoidAsync("loadScript", "https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js");
            // await JSRuntime.InvokeVoidAsync("loadScript", "https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/js/lightgallery-all.min.js");
            await JSRuntime.InvokeVoidAsync("loadScript", "https://d335luupugsy2.cloudfront.net/js/loader-scripts/f55e50e4-ab3e-4425-a568-444c1de36a96-loader.js");
            if (firstRender)
            {
                
                GetEstados();
                if (string.IsNullOrEmpty(estado) || estado == "pernambuco")
                {
                    // await JSRuntime.InvokeAsync<object>("showModalGeolocalizacao", new object[] { });
                }
            }
            await base.OnAfterRenderAsync(firstRender);
        }

    }
}
