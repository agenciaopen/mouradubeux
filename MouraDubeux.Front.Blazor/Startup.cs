using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Data.Estados;
using MouraDubeux.Admin.Data.Data.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Front.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Front.Blazor.Models;
using MouraDubeux.Front.Blazor.Helpers;

namespace MouraDubeux.Front.Blazor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            UrlHelper.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
            services.AddSingleton<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IFrontDataService, FrontDataService>();
            services.AddSingleton<IIdiomaDataService, IdiomaDataService>();
            services.AddSingleton<IExtensionMethods, ExtensionMethods>();
           
            services.AddScoped<IEstadosDataService, EstadosDataService>();
            services.AddScoped<ICidadesDataService, CidadesDataService>();
            services.AddScoped<IFiltrosService, FiltrosService>();
            services.AddScoped<ILocalStorageService, LocalStorageService>();
            services.AddSingleton(Configuration.GetSection("MailSettings").Get<MailSettings>());
            services.AddScoped<IMailService, MailService>();
            services.AddScoped<IEmpreendimentoDataService, EmpreendimentoDataService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {

            }
            else
            {
                //app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
