﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.DataAccess
{
    public interface ISqlDataAccess
    {
        Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters, string connectionStringName);
        List<T> LoadDataSync<T, U>(string storedProcedure, U parameters, string connectionStringName);
        Task SaveData<T>(string storedProcedure, T parameters, string connectionStringName);
    }
}