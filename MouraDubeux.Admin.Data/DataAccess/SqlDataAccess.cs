﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.DataAccess
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private readonly IConfiguration _config;

        public SqlDataAccess(IConfiguration config)
        {
            _config = config;
        }
        public async Task<List<T>> LoadData<T, U>(string storedProcedure, U parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);

           
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var rows = await connection.QueryAsync<T>(storedProcedure, parameters,
                    commandType: CommandType.StoredProcedure);

                return rows.ToList();
            }
            
        }

        public List<T> LoadDataSync<T, U>(string storedProcedure, U parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);


            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    var rows = connection.Query<T>(storedProcedure, parameters,
                        commandType: CommandType.StoredProcedure);

                    return rows.ToList();
                }
                catch(Exception er)
                {
                    var erro = er.StackTrace;
                }
                return null;
            }

        }

        public async Task SaveData<T>(string storedProcedure, T parameters, string connectionStringName)
        {
            string connectionString = _config.GetConnectionString(connectionStringName);

            using (IDbConnection connection = new SqlConnection(connectionString))
            {

                try
                {
                    var rows = await connection.QueryAsync(storedProcedure, parameters,
                    commandType: CommandType.StoredProcedure);
                }
                catch(Exception e)
                {
                    var err = e.StackTrace; 
                }
               
            }
        }
    }
}
