﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Empreendimento
{
    public class EmpreendimentoFichaTecnicaDataService : IEmpreendimentoFichaTecnicaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EmpreendimentoFichaTecnicaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public int EmpreendimentoFichaTecnicaCreate(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnica)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spEmpreendimentoFichaTecnica_Create", empreendimentoFichaTecnica, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica()
        {
            try
            {
                var empreendimentoFichaTecnicas = _dataAccess.LoadDataSync<EmpreendimentoFichaTecnicaModel, dynamic>("dbo.spEmpreendimentoFichaTecnica_Read", new { id = Guid.Empty }, "SQLDB");
                return empreendimentoFichaTecnicas.ToList<EmpreendimentoFichaTecnicaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica(Guid id, Guid idEmpreendimento, int tipoFichaTecnica)
        {
            try
            {
                var empreendimentoFichaTecnicas = _dataAccess.LoadDataSync<EmpreendimentoFichaTecnicaModel, dynamic>("dbo.spEmpreendimentoFichaTecnica_Read", new { Id = id, IdEmpreendimento = idEmpreendimento, TipoFichaTecnica  = tipoFichaTecnica }, "SQLDB");
                return empreendimentoFichaTecnicas.ToList<EmpreendimentoFichaTecnicaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoFichaTecnicaUpdate(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnica)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoFichaTecnica_Update", empreendimentoFichaTecnica, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EmpreendimentoFichaTecnicaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEmpreendimentoFichaTecnica_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
