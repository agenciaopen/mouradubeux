﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Escritorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Escritorio
{
    public class EscritorioDataService : IEscritorioDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public EscritorioDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public string EscritorioCreate(IEscritorioModel escritorio)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spEscritorio_Create", escritorio, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }
        public List<EscritorioModel> GetEscritorio()
        {
            try
            {
                var escritorios = _dataAccess.LoadDataSync<EscritorioModel, dynamic>("dbo.spEscritorio_Read", new { id = Guid.Empty }, "SQLDB");
                return escritorios.ToList<EscritorioModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EscritorioModel> GetEscritorio(Guid id)
        {
            try
            {
                var escritorios = _dataAccess.LoadDataSync<EscritorioModel, dynamic>("dbo.spEscritorio_Read", new { Id = id }, "SQLDB");
                return escritorios.ToList<EscritorioModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EscritorioUpdate(IEscritorioModel escritorio)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEscritorio_Update", escritorio, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void EscritorioDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spEscritorio_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
