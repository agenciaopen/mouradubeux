﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Admin.Data.Models.MdMidia;
using MouraDubeux.Admin.Data.Models.Projeto;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Cidades;

namespace MouraDubeux.Admin.Data.Data
{
    public class FrontDataService : IFrontDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public List<EmpreendimentoModel> getEmpreendimento { get; set; }
        public List<EstadosModel> getEstados { get; set; }
        public List<EscritorioModel> getEscritorio { get; set; }
        public List<ProjetoModel> getProjeto { get; set; }
        public List<EmpreendimentoModel> getBairros { get; set; }
        public List<EmpreendimentoModel> getQuartos { get; set; }
        public List<FaqModel> getFaq { get; set; }

        public List<MdMidiaModel> getMdMidia { get; set; }

        public FrontDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<EmpreendimentoModel> ResetCacheEmpreendimento()
        {
            try
            {
                getEmpreendimento = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { id = Guid.Empty }, "SQLDB");
                return getEmpreendimento.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetEmpreendimento()
        {
            try
            {
                if (getEmpreendimento == null)
                {
                    getEmpreendimento = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { id = Guid.Empty }, "SQLDB");
                }

                return getEmpreendimento.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoModel> GetEmpreendimento(Guid id)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read", new { Id = id }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid idEmpreendimento, int tipoGaleria)
        {
            try
            {
                var empreendimentoGalerias = _dataAccess.LoadDataSync<EmpreendimentoGaleriaModel,
                    dynamic>("dbo.spEmpreendimentoGaleria_Read",
                    new { Id = Guid.Empty, IdEmpreendimento = idEmpreendimento, TipoGaleria = tipoGaleria }, "SQLDB");
                return empreendimentoGalerias.ToList<EmpreendimentoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid idEmpreendimento)
        {
            try
            {
                var empreendimentoValorDescricaos = _dataAccess.LoadDataSync<EmpreendimentoValorDescricaoModel, dynamic>("dbo.spEmpreendimentoValorDescricao_Read", new { Id = Guid.Empty, IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentoValorDescricaos.ToList<EmpreendimentoValorDescricaoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EstadosModel> GetEstados()
        {
            try
            {
                if (getEstados == null)
                {
                    getEstados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = "" }, "SQLDB");
                }
                return getEstados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid idEmpreendimento)
        {
            try
            {
                var empreendimentoInformacoesTecnicas = _dataAccess.LoadDataSync<EmpreendimentoInformacoesTecnicasModel, dynamic>("dbo.spEmpreendimentoInformacoesTecnicas_Read", new { id = Guid.Empty, IdEmpreendimento = idEmpreendimento }, "SQLDB");
                return empreendimentoInformacoesTecnicas.ToList<EmpreendimentoInformacoesTecnicasModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica(Guid idEmpreendimento, int tipoFichaTecnica)
        {
            try
            {
                var empreendimentoFichaTecnicas = _dataAccess.LoadDataSync<EmpreendimentoFichaTecnicaModel, dynamic>("dbo.spEmpreendimentoFichaTecnica_Read", new { Id = Guid.Empty, IdEmpreendimento = idEmpreendimento, TipoFichaTecnica = tipoFichaTecnica }, "SQLDB");
                return empreendimentoFichaTecnicas.ToList<EmpreendimentoFichaTecnicaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EscritorioModel> GetEscritorio()
        {
            try
            {
                if (getEscritorio == null)
                {
                    getEscritorio = _dataAccess.LoadDataSync<EscritorioModel, dynamic>("dbo.spEscritorio_Read", new { id = Guid.Empty }, "SQLDB");
                }
                return getEscritorio.ToList<EscritorioModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<MdMidiaModel> GetMdMidia()
        {
            try
            {
                if (getMdMidia == null)
                {
                    getMdMidia = _dataAccess.LoadDataSync<MdMidiaModel, dynamic>("dbo.spMdMidia_Read", new { id = Guid.Empty }, "SQLDB");
                }
                return getMdMidia.ToList<MdMidiaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<ProjetoModel> GetProjeto()
        {
            try
            {
                if (getProjeto == null)
                {
                    getProjeto = _dataAccess.LoadDataSync<ProjetoModel, dynamic>("dbo.spProjeto_Read", new { id = Guid.Empty }, "SQLDB");
                }
                return getProjeto.ToList<ProjetoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<ProjetoGaleriaModel> GetProjetoGaleria(Guid idProjeto)
        {
            try
            {
                var projetoGalerias = _dataAccess.LoadDataSync<ProjetoGaleriaModel, dynamic>("dbo.spProjetoGaleria_Read", new { Id = Guid.Empty, IdProjeto = idProjeto }, "SQLDB");
                return projetoGalerias.ToList<ProjetoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<EmpreendimentoModel> GetBairros()
        {
            try
            {
                if (getBairros == null)
                {
                    getBairros = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_Distinct_Bairro", new { id = Guid.Empty }, "SQLDB");
                }
                return getBairros.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<CidadesModel> GetCidadesByEstado(string estadoSlug)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<CidadesModel, dynamic>("dbo.spCidadesEmpreendimentos_Read", new { SlugEstado = estadoSlug }, "SQLDB");
                return empreendimentos.ToList<CidadesModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }
        public List<EmpreendimentoModel> GetQuartos()
        {
            try
            {
                if (getQuartos == null)
                {
                    getQuartos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_Distinct_Quartos", new { id = Guid.Empty, Cidade = "", Bairro = "" }, "SQLDB");
                }
                return getQuartos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EmpreendimentoModel> GetQuartosByCidadeBairro(string cidade, string bairro)
        {
            try
            {
                var empreendimentos = _dataAccess.LoadDataSync<EmpreendimentoModel, dynamic>("dbo.spEmpreendimento_Read_Distinct_Quartos", new { id = Guid.Empty, Cidade = cidade, Bairro = bairro }, "SQLDB");
                return empreendimentos.ToList<EmpreendimentoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public List<EstadosModel> GetEstados(string slug)
        {
            try
            {
                var estados = _dataAccess.LoadDataSync<EstadosModel, dynamic>("dbo.spEstados_Read", new { Slug = slug }, "SQLDB");
                return estados.ToList<EstadosModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<PaginaModel> GetPagina(string slug)
        {
            try
            {
                var pagina = _dataAccess.LoadDataSync<PaginaModel, dynamic>("dbo.spPagina_Read", new { Slug = slug }, "SQLDB");
                return pagina.ToList<PaginaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public string UpdateMetaTags(string title, string slug, string idioma)
        {
            try
            {
                var pagina = _dataAccess.SaveData("dbo.spPagina_Update_Title",
                    new
                    {
                        Slug = slug,
                        IdiomaId = idioma,
                        MataTittle = title

                    }, "SQLDB");
                return "";

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<FaqModel> GetFaq()
        {
            try
            {
                if (getFaq == null)
                {
                    getFaq = _dataAccess.LoadDataSync<FaqModel, dynamic>("dbo.spFaq_Read", new { id = Guid.Empty }, "SQLDB");
                }
                return getFaq.ToList<FaqModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}