﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.MdMidia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.MdMidia
{
    public class MdMidiaDataService : IMdMidiaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public MdMidiaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public string MdMidiaCreate(IMdMidiaModel mdMidia)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spMdMidia_Create", mdMidia, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<MdMidiaModel> GetMdMidia()
        {
            try
            {
                var mdMidias = _dataAccess.LoadDataSync<MdMidiaModel, dynamic>("dbo.spMdMidia_Read", new { id = Guid.Empty }, "SQLDB");
                return mdMidias.ToList<MdMidiaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<MdMidiaModel> GetMdMidia(Guid id)
        {
            try
            {
                var mdMidias = _dataAccess.LoadDataSync<MdMidiaModel, dynamic>("dbo.spMdMidia_Read", new { Id = id }, "SQLDB");
                return mdMidias.ToList<MdMidiaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void MdMidiaUpdate(IMdMidiaModel mdMidia)
        {
            try
            {
                _dataAccess.SaveData("dbo.spMdMidia_Update", mdMidia, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void MdMidiaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spMdMidia_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
