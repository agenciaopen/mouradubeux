﻿using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Cidades
{
    public class CidadesDataService : ICidadesDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public CidadesDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public List<CidadesModel> GetCidades()
        {
            try
            {
                var cidades = _dataAccess.LoadDataSync<CidadesModel, dynamic>("dbo.spCidades_Read", new { Slug = "" }, "SQLDB");
                return cidades.ToList<CidadesModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<CidadesModel> GetCidades(string slug, string slugEstado)
        {
            try
            {
                var cidades = _dataAccess.LoadDataSync<CidadesModel, dynamic>("dbo.spCidades_Read", new { Slug = slug, SlugEstado = slugEstado }, "SQLDB");
                return cidades.ToList<CidadesModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
