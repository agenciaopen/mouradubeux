﻿using MouraDubeux.Admin.Data.Models.Cidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Data.Interfaces.Cidades
{
    public interface ICidadesDataService
    {
        List<CidadesModel> GetCidades();
        List<CidadesModel> GetCidades(string slug, string slugEstado);
    }
}
