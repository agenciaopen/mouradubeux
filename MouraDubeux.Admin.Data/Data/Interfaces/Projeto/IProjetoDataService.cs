﻿using MouraDubeux.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Projeto
{
    public interface IProjetoDataService
    {
        List<ProjetoModel> GetProjeto();
        List<ProjetoModel> GetProjeto(Guid id);
        string ProjetoCreate(IProjetoModel projeto);
        void ProjetoUpdate(IProjetoModel projeto);
        void ProjetoDelete(Guid id);
    }
}