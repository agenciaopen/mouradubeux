﻿using MouraDubeux.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Data.Interfaces.Faq
{
    public interface IFaqDataService
    {
        List<FaqModel> GetFaq();
        List<FaqModel> GetFaq(Guid id);
        string FaqCreate(IFaqModel Faq);
        void FaqUpdate(IFaqModel Faq);
        void FaqDelete(Guid id);
    }
}
