﻿using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Admin.Data.Models.MdMidia;
using MouraDubeux.Admin.Data.Models.Projeto;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Data.Models.Faq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Models.Cidades;

namespace MouraDubeux.Admin.Data.Data
{
    public interface IFrontDataService
    {
        List<EmpreendimentoModel> ResetCacheEmpreendimento();
        List<EmpreendimentoModel> GetEmpreendimento();
        List<EmpreendimentoModel> GetEmpreendimento(Guid id);
        List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid idEmpreendimento, int tipoGaleria);
        List<EmpreendimentoValorDescricaoModel> GetEmpreendimentoValorDescricao(Guid idEmpreendimento);
        List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid idEmpreendimento);
        List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica(Guid idEmpreendimento, int tipoFichaTecnica);
        List<EscritorioModel> GetEscritorio();
        List<MdMidiaModel> GetMdMidia();
        List<ProjetoModel> GetProjeto();
        List<ProjetoGaleriaModel> GetProjetoGaleria(Guid idProjeto);
        List<EmpreendimentoModel> GetBairros();
        List<EmpreendimentoModel> GetQuartosByCidadeBairro(string cidade, string bairro);
        List<EmpreendimentoModel> GetQuartos();
        List<EstadosModel> GetEstados();
        List<EstadosModel> GetEstados(string slug);
        List<PaginaModel> GetPagina(string slug);
        List<FaqModel> GetFaq();
        List<CidadesModel> GetCidadesByEstado(string estadoSlug);
        string UpdateMetaTags(string title, string slug, string idioma);

    }
}