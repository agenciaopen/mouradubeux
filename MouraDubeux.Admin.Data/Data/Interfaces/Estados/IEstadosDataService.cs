﻿using MouraDubeux.Admin.Data.Models.Estados;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Data.Interfaces.Estados
{
    public interface IEstadosDataService
    {
        List<EstadosModel> GetEstados();
        List<EstadosModel> GetEstados(string slug);
    }
}
