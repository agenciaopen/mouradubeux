﻿using MouraDubeux.Admin.Data.Models.MdMidia;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.MdMidia
{
    public interface IMdMidiaDataService
    {
        List<MdMidiaModel> GetMdMidia();
        List<MdMidiaModel> GetMdMidia(Guid id);
        string MdMidiaCreate(IMdMidiaModel mdMidia);
        void MdMidiaUpdate(IMdMidiaModel mdMidia);
        void MdMidiaDelete(Guid id);
    }
}