﻿using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Empreendimento
{
    public  interface IEmpreendimentoGaleriaDataService
    {
        List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria();
        List<EmpreendimentoGaleriaModel> GetEmpreendimentoGaleria(Guid id, Guid idEmpreendimento, int tipoGaleria);
        int EmpreendimentoGaleriaCreate(IEmpreendimentoGaleriaModel empreendimentoGaleria);
        void EmpreendimentoGaleriaUpdate(IEmpreendimentoGaleriaModel empreendimentoGaleria);
        void EmpreendimentoGaleriaOrderUpdate(IEmpreendimentoGaleriaModel empreendimentoGaleria);
        void EmpreendimentoGaleriaDelete(Guid id);
    }
}