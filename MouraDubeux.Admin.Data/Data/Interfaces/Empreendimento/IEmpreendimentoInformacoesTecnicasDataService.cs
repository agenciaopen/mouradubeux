﻿using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Empreendimento
{
    public interface IEmpreendimentoInformacoesTecnicasDataService
    {
        List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas();
        List<EmpreendimentoInformacoesTecnicasModel> GetEmpreendimentoInformacoesTecnicas(Guid id, Guid idEmpreendimento);
        int EmpreendimentoInformacoesTecnicasCreate(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicas);
        void EmpreendimentoInformacoesTecnicasUpdate(IEmpreendimentoInformacoesTecnicasModel empreendimentoInformacoesTecnicas);
        void EmpreendimentoInformacoesTecnicasDelete(Guid id);
    }
}