﻿using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Empreendimento
{
    public interface IEmpreendimentoFichaTecnicaDataService
    {
        List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica();
        List<EmpreendimentoFichaTecnicaModel> GetEmpreendimentoFichaTecnica(Guid id, Guid idEmpreendimento, int tipoFichaTecnica);
        int EmpreendimentoFichaTecnicaCreate(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnica);
        void EmpreendimentoFichaTecnicaUpdate(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnica);
        void EmpreendimentoFichaTecnicaDelete(Guid id);
    }
}