﻿using MouraDubeux.Admin.Data.Models.Idioma;
using System.Collections.Generic;

namespace MouraDubeux.Admin.Data.Data
{
    public interface IIdiomaDataService
    {
        List<IIdiomaModel> Get();
    }
}