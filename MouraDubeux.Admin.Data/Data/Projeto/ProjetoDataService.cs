﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Projeto
{
    public class ProjetoDataService : IProjetoDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public ProjetoDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public string ProjetoCreate(IProjetoModel projeto)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<string, dynamic>("dbo.spProjeto_Create", projeto, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente:" + e.Message);
            }
        }
        public List<ProjetoModel> GetProjeto()
        {
            try
            {
                var projetos = _dataAccess.LoadDataSync<ProjetoModel, dynamic>("dbo.spProjeto_Read", new { id = Guid.Empty }, "SQLDB");
                return projetos.ToList<ProjetoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<ProjetoModel> GetProjeto(Guid id)
        {
            try
            {
                var projetos = _dataAccess.LoadDataSync<ProjetoModel, dynamic>("dbo.spProjeto_Read", new { Id = id }, "SQLDB");
                return projetos.ToList<ProjetoModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void ProjetoUpdate(IProjetoModel projeto)
        {
            try
            {
                _dataAccess.SaveData("dbo.spProjeto_Update", projeto, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void ProjetoDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spProjeto_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
