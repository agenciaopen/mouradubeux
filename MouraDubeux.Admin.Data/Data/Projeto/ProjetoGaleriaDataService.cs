﻿using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Data.Data.Projeto
{
    public class ProjetoGaleriaDataService : IProjetoGaleriaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public ProjetoGaleriaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public int ProjetoGaleriaCreate(IProjetoGaleriaModel projetoGaleria)
        {
            try
            {
                var ids = _dataAccess.LoadDataSync<int, dynamic>("dbo.spProjetoGaleria_Create", projetoGaleria, "SQLDB");
                return ids.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<ProjetoGaleriaModel> GetProjetoGaleria()
        {
            try
            {
                var projetoGalerias = _dataAccess.LoadDataSync<ProjetoGaleriaModel, dynamic>("dbo.spProjetoGaleria_Read", new { id = Guid.Empty }, "SQLDB");
                return projetoGalerias.ToList<ProjetoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public List<ProjetoGaleriaModel> GetProjetoGaleria(Guid id, Guid idProjeto)
        {
            try
            {
                var projetoGalerias = _dataAccess.LoadDataSync<ProjetoGaleriaModel, dynamic>("dbo.spProjetoGaleria_Read", new { Id = id, IdProjeto = idProjeto }, "SQLDB");
                return projetoGalerias.ToList<ProjetoGaleriaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void ProjetoGaleriaUpdate(IProjetoGaleriaModel projetoGaleria)
        {
            try
            {
                _dataAccess.SaveData("dbo.spProjetoGaleria_Update", projetoGaleria, "SQLDB");

            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }

        public void ProjetoGaleriaDelete(Guid id)
        {
            try
            {
                _dataAccess.SaveData("dbo.spProjetoGaleria_Delete", new { Id = id }, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
