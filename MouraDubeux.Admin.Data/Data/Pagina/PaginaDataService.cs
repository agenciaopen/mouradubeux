﻿using MouraDubeux.Admin.Data.Data.Interfaces.Pagina;
using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Pagina;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MouraDubeux.Admin.Data.Data.Pagina
{
    public class PaginaDataService : IPaginaDataService
    {
        private readonly ISqlDataAccess _dataAccess;

        public PaginaDataService(ISqlDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public List<PaginaModel> GetPagina(string slug)
        {
            try
            {
                var pagina = _dataAccess.LoadDataSync<PaginaModel, dynamic>("dbo.spPagina_Read", new { Slug = slug }, "SQLDB");
                return pagina.ToList<PaginaModel>();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
        public string PaginaCreate(IPaginaModel escritorio)
        {
            try
            {
                var pagina = _dataAccess.LoadDataSync<string, dynamic>("dbo.spPagina_Create", escritorio, "SQLDB");
                return pagina.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente: " + e.Message);
            }
        }

        public void PaginaUpdate(IPaginaModel pagina)
        {
            try
            {
                _dataAccess.SaveData("dbo.spPagina_Update", pagina, "SQLDB");
            }
            catch (Exception e)
            {
                throw new Exception("Ops, ocorreu um erro, tente novamente");
            }
        }
    }
}
