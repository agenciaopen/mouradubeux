﻿using System.ComponentModel;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumCategoriaIcones
    {
        [Description("Lazer")]
        Lazer = 1,

        [Description("Infra Estrutura")]
        InfraEstrutura = 2,

        [Description("Status")]
        Status = 3,

        [Description("Externo")]
        Externo = 4,
    }
}
