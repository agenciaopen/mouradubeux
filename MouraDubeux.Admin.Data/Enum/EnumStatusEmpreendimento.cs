﻿using System.ComponentModel;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumStatusEmpreendimento
    {
        [Description("Em Construção")]
        EmConstrucao = 1,

        [Description("Pronto")]
        Pronto = 2,

        [Description("Revenda")]
        Revenda = 3,

        [Description("Lançamento")]
        Lancamento = 4,

        [Description("Na Planta")]
        NaPlanta = 5,

        [Description("Portfólio")]
        Portfolio = 6
    }
}


