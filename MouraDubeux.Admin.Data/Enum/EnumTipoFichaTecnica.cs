﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumTipoFichaTecnica
    {
        [Description("Infraestrutura")]
        Infraestrutura = 1,

        [Description("Lazer")]
        Lazer = 2,

        [Description("Serviços Próximos")]
        ServicosProximos = 3,

    }
}
