﻿using System.ComponentModel;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumTipoEmpreendimento
    {
        [Description("Empresarial")]
        Empresarial = 1,

        [Description("Residenciais Condomínio")]
        ResidencialCondominio = 2,

        [Description("Residenciais Incorporação")]
        ResidencialIncorporacao = 3,

        [Description("Beach Class")]
        BeachClass = 4
    }
}
