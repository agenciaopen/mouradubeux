﻿using System.ComponentModel;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumTipoGaleria
    {
        [Description("Imóvel")]
        Imovel = 1,

        [Description("Galeria de Plantas")]
        Planta = 2,

        [Description("Galeria de Status da Obra")]
        StatusDaObra = 3,

        [Description("Área Comum")]
        AreaComum = 4,

        [Description("Decorado")]
        Decorado = 5,

    }
}
