﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumFichaTecnicaServicosProximosIcones
    {
        [Description("Contabilidade")]
        accounting = 1,

        [Description("Aeroporto")]
        airport = 2,

        [Description("Parque de diversões")]
        amusement_park = 3,

        [Description("Aquário")]
        aquarium = 4,

        [Description("Galeria de Arte")]
        art_gallery = 5,

        [Description("Atm")]
        atm = 6,

        [Description("Padaria")]
        bakery = 7,

        [Description("Banco")]
        bank = 8,

        [Description("Bar")]
        bar = 9,

        [Description("salão de beleza")]
        beauty_salon = 10,

        [Description("Bicicletaria")]
        bicycle_store = 11,

        [Description("Livraria")]
        book_store = 12,

        [Description("Boliche")]
        bowling_alley = 13,

        [Description("Estação de ônibus")]
        bus_station = 14,

        [Description("Cafeteria")]
        cafe = 15,

        [Description("Área de camping")]
        campground = 16,

        [Description("Venda de carros")]
        car_dealer = 17,

        [Description("Aluguel de carros")]
        car_rental = 18,

        [Description("Reparo de carro")]
        car_repair = 19,

        [Description("Lava-jato")]
        car_wash = 20,

        [Description("Cassino")]
        casino = 21,

        [Description("Cemitério")]
        cemetery = 22,

        [Description("Igreja")]
        church = 23,

        [Description("Câmara Municipal")]
        city_hall = 24,

        [Description("Loja de roupas")]
        clothing_store = 25,

        [Description("Loja de conveniência")]
        convenience_store = 26,

        [Description("Tribunal")]
        courthouse = 27,

        [Description("Dentista")]
        dentist = 28,

        [Description("Loja de departamento")]
        department_store = 29,

        [Description("Doutor")]
        doctor = 30,

        [Description("Eletricista")]
        electrician = 31,

        [Description("Loja de eletrônicos")]
        electronics_store = 32,

        [Description("Embaixada")]
        embassy = 33,

        [Description("Corpo de Bombeiros")]
        fire_station = 34,

        [Description("Florista")]
        florist = 35,

        [Description("Funerária")]
        funeral_home = 36,

        [Description("Loja de móveis")]
        furniture_store = 37,

        [Description("Posto de gasolina")]
        gas_station = 38,

        [Description("Academia")]
        gym = 39,

        [Description("Cuidado capilar")]
        hair_care = 40,

        [Description("Loja de hardware")]
        hardware_store = 41,

        [Description("Templo hindu")]
        hindu_temple = 42,

        [Description("Loja de bens do lar")]
        home_goods_store = 43,

        [Description("Hospital")]
        hospital = 44,

        [Description("Agência de seguros")]
        insurance_agency = 45,

        [Description("Joalheria")]
        jewelry_store = 46,

        [Description("Lavanderia")]
        laundry = 47,

        [Description("Advogado")]
        lawyer = 48,

        [Description("Biblioteca")]
        library = 49,

        [Description("Loja de bebidas")]
        liquor_store = 50,

        [Description("Escritório do governo local")]
        local_government_office = 51,

        [Description("Chaveiro")]
        locksmith = 52,

        [Description("Alojamento")]
        lodging = 53,

        [Description("Entrega de mel")]
        mel_delivery = 54,

        [Description("Refeição para viagem")]
        meal_takeaway = 55,

        [Description("Mesquita")]
        mosque = 56,

        [Description("Aluguel de filmes")]
        movie_rental = 57,

        [Description("Cinema")]
        movie_theater = 58,

        [Description("Empresa de mudanças")]
        moving_company = 59,

        [Description("Museu")]
        museum = 60,

        [Description("Boate")]
        night_club = 61,

        [Description("Pintor")]
        painter = 62,

        [Description("Parque")]
        park = 63,

        [Description("Estacionamento")]
        parking = 64,

        [Description("Loja de animais")]
        pet_store = 65,

        [Description("Farmacia")]
        pharmacy = 66,

        [Description("Fisioterapeuta")]
        physiotherapist = 67,

        [Description("Encanador")]
        plumber = 68,

        [Description("Polícia")]
        police = 69,

        [Description("Correios")]
        post_office = 70,

        [Description("Agência imobiliária")]
        real_estate_agency = 71,

        [Description("Restaurante")]
        restaurant = 72,

        [Description("Empreiteiro de telhados")]
        roofing_contractor = 73,

        [Description("Parque de trailers")]
        rv_park = 74,

        [Description("Escola")]
        school = 75,

        [Description("Loja de sapatos")]
        shoe_store = 76,

        [Description("Shopping")]
        shopping_mall = 77,

        [Description("Spa")]
        spa = 78,

        [Description("Estádio")]
        stadium = 79,

        [Description("Armazém")]
        storage = 80,

        [Description("Loja")]
        store = 81,

        [Description("Estação de metrô")]
        subway_station = 82,

        [Description("Sinagoga")]
        synagogue = 83,

        [Description("Ponto de taxi")]
        taxi_stand = 84,

        [Description("Estação de trem")]
        train_station = 85,

        [Description("Estação de trânsito")]
        transit_station = 86,

        [Description("Agência de viagens")]
        travel_agency = 87,

        [Description("universidade")]
        university = 88,

        [Description("Veterinário")]
        veterinary_care = 89,

        [Description("Zoológico")]
        zoo = 90

    }
}
