﻿using System.ComponentModel;

namespace MouraDubeux.Admin.Data.Enum
{
    public enum EnumModeloNegocio
    {
        [Description("Incorporação")]
        Incorporacao = 1,

        [Description("Administração")]
        Administracao = 2
    }
}
