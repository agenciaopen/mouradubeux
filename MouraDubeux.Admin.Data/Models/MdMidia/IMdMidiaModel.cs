﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace MouraDubeux.Admin.Data.Models.MdMidia
{
    public interface IMdMidiaModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        string Titulo { get; set; }
        string Descricao { get; set; }
        string Conteudo { get; set; }
        DateTime DataPublicacao { get; set; }
        string UrlImagem { get; set; }
        string Link { get; set; }
    }
}