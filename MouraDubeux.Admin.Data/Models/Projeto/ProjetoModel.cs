﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace MouraDubeux.Admin.Data.Models.Projeto
{
    public class ProjetoModel : IProjetoModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Endereco { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string UrlImagem { get; set; }
        public bool ProjetoSocial { get; set; }
        public string EstadoNome { get; set; }
        public string CidadeNome { get; set; }
    }
}
