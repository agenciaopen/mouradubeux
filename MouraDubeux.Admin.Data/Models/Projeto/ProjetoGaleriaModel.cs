﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace MouraDubeux.Admin.Data.Models.Projeto
{
    public class ProjetoGaleriaModel : IProjetoGaleriaModel
    {
        public Guid Id { get; set; }
        public Guid IdProjeto { get; set; }
        public string IdiomaId { get; set; }
        public string UrlImagem { get; set; }
        public string UrlVideo { get; set; }
        public string Titulo { get; set; }
        public bool EhVideo { get; set; }
        public int Ordem { get; set; }

        public string GetIdUrlVideo()
        {
            if (UrlVideo != null)
            {
                var uri = new Uri(UrlVideo);
                var query = HttpUtility.ParseQueryString(uri.Query);
                return query["v"];
            }
            return UrlVideo;
        }

    }
}
