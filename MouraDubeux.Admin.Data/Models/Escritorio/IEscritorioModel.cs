﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace MouraDubeux.Admin.Data.Models.Escritorio
{
    public interface IEscritorioModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        string Nome { get; set; }
        string Telefone { get; set; }
        string Endereco { get; set; }
        string Estado { get; set; }
        string Cidade { get; set; }
        string CEP { get; set; }
        string HorarioFuncionamento { get; set; }
        string UrlBanner { get; set; }
        string EstadoNome { get; set; }
        string CidadeNome { get; set; }
    }
}