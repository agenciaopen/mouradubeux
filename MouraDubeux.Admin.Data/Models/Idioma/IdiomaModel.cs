﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Idioma
{
    public class IdiomaModel : IIdiomaModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
    }
}
