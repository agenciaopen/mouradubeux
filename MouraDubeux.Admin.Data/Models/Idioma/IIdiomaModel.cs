﻿namespace MouraDubeux.Admin.Data.Models.Idioma
{
    public interface IIdiomaModel
    {
        string Id { get; set; }
        string Nome { get; set; }
    }
}