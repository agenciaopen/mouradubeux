﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Estados
{
    public class EstadosModel : IEstadosModel
    {
        public string UF { get; set; }
        public string Nome { get; set; }
        public string Slug { get; set; }
    }
}
