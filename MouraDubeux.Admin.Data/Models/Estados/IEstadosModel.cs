﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Estados
{
    public interface IEstadosModel
    {
        string UF { get; set; }
        string Nome { get; set; }
        string Slug { get; set; }
    }
}
