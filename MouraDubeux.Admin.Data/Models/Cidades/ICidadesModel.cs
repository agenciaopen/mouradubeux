﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Cidades
{
    public interface ICidadesModel
    {
        string UF { get; set; }
        string Nome { get; set; }
        string Slug { get; set; }
    }
}
