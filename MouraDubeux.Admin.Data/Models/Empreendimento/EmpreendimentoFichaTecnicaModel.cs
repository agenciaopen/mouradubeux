﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public class EmpreendimentoFichaTecnicaModel : IEmpreendimentoFichaTecnicaModel
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public Guid IdValorDescricao { get; set; }
        public string IdiomaId { get; set; }
        public string UrlImagem { get; set; }
        public int TipoFichaTecnica { get; set; }
        public int Peso { get; set; }
    }
}
