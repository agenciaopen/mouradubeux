﻿using System;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoInformacoesTecnicasModel
    {
        Guid Id { get; set; }
        Guid IdEmpreendimento { get; set; }
        string IdiomaId { get; set; }
        string Nome { get; set; }
        string Funcao { get; set; }
    }
}