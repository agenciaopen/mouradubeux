﻿using System;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoFichaTecnicaModel
    {
        Guid Id { get; set; }
        Guid IdEmpreendimento { get; set; }
        Guid IdValorDescricao { get; set; }
        string IdiomaId { get; set; }
        string UrlImagem { get; set; }
        int TipoFichaTecnica { get; set; }
        int Peso { get; set; }
    }
}