﻿using System;
using System.Web;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public interface IEmpreendimentoGaleriaModel
    {
        Guid Id { get; set; }
        Guid IdEmpreendimento { get; set; }
        string IdiomaId { get; set; }
        string UrlImagem { get; set; }
        string UrlVideo { get; set; }
        string Descricao { get; set; }
        int TipoGaleria { get; set; }
        string Titulo { get; set; }
        bool EhVideo { get; set; }
        string GetIdUrlVideo();
    }
}