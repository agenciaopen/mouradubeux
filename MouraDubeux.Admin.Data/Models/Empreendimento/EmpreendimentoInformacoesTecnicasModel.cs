﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public class EmpreendimentoInformacoesTecnicasModel : IEmpreendimentoInformacoesTecnicasModel
    {
        public Guid Id { get; set; }

        public Guid IdEmpreendimento { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Funcao { get; set; }
    }
}
