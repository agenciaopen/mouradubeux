﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Empreendimento
{
    public class EmpreendimentoValorDescricaoModel : IEmpreendimentoValorDescricaoModel
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string IdiomaId { get; set; }
        public string Descricao { get; set; }
        public string Valor { get; set; }
        public string Titulo { get; set; }
        public string Slug { get; set; }
        public bool Planta { get; set; }
        public bool ValorPrincipal { get; set; }
        
    }
}
