﻿using System;

namespace MouraDubeux.Admin.Data.Models.Home
{
    public interface IHomeBannerModel
    {

        Guid Id { get; set; }
        string IdiomaId { get; set; }
        string Descricao { get; set; }
        string Link { get; set; }
        string LinkImagem { get; set; }
        string Titulo { get; set; }

    }
}