﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Home
{
    public class HomeBannerModel : IHomeBannerModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string LinkImagem { get; set; }
        public string Link { get; set; }
    }
}
