﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Pagina
{
    public interface IPaginaModel
    {
        string Slug { get; set; }
        string IdiomaId { get; set; }
        string Titulo { get; set; }
        string Banner { get; set; }
        string Link { get; set; }
        string Imagem { get; set; }
        string Descricao { get; set; }
        string MataTittle { get; set; }
        string MetaDescription { get; set; }
        string MetaKeyword { get; set; }

    }
}
