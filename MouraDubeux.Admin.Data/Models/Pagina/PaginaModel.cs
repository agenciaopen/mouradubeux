﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Pagina
{
    public class PaginaModel : IPaginaModel
    {
        public string Slug { get; set; }
        public string IdiomaId { get; set; }
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
        public string Banner { get; set; }
        public string Link { get; set; }
        public string Imagem { get; set; }
        public string Descricao { get; set; }
        public string MataTittle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
    }
}
