﻿using MouraDubeux.Admin.Data.Models.Pagina;
using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Faq
{
    public class FaqModel : IFaqModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public int TipoFaq { get; set; }
        public int TipoFaqArea { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string UrlImagem { get; set; }
        public int Secao { get; set; }
    }
}
