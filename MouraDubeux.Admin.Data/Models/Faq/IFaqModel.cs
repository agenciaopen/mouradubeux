﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MouraDubeux.Admin.Data.Models.Faq
{
    public interface IFaqModel
    {
        Guid Id { get; set; }
        string IdiomaId { get; set; }
        int TipoFaq { get; set; }
        int TipoFaqArea { get; set; }
        string Titulo { get; set; }
        string Texto { get; set; }
        string UrlImagem { get; set; }
        int Secao { get; set; }
    }
}
