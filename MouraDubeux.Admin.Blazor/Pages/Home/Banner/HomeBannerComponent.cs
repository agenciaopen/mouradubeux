﻿using MouraDubeux.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Blazor.Models;
using MouraDubeux.Admin.Blazor.Data;

namespace MouraDubeux.Admin.Blazor.Pages.Home.Banner
{
    public partial class HomeBannerComponent : ComponentBase
    {

        private IHomeBannerModel banner = new HomeBannerModelForm();
        public List<HomeBannerModel> modelDetalhes { get; set; }
        public List<HomeBannerModel> modelList { get; set; }


        private int idToDelete = 0;
        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IHomeDataService homeDataService { get; set; }

        [Inject]
        IIdiomaDataService idiomaDataService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }
        protected override void OnParametersSet()
        {
            GetBanners();
            base.OnParametersSet();
        }

        protected async override Task OnParametersSetAsync()
        {
            //await Task.Delay(3330000);
        }
        private void IncrementCount(string id)
        {
            var teste = id;
        }

        private void GetBanners()
        {
            modelList = homeDataService.GetBanner().Where(x => x.IdiomaId == "pt").ToList();
        }

        private void DeleteBanner(IHomeBannerModel banner)
        {
            homeDataService.HomeBannerDelete(banner.Id);
            blobService.DeleteBlobAsync(banner.LinkImagem);
            GetBanners();
        }
        private void CreateBanner()
        {
            banner = new HomeBannerModelForm();
            modelDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-banner");
        }

        private void EditBanner(IHomeBannerModel banner)
        {
            modelDetalhes = homeDataService.GetBanner(banner.Id);
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-banner");
        }
    }
}

