﻿using Microsoft.AspNetCore.Components;
using MouraDubeux.Admin.Blazor.Models;
using MouraDubeux.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using Microsoft.Extensions.Configuration;
using MouraDubeux.Admin.Data.Data;

namespace MouraDubeux.Admin.Blazor.Pages.Home.Banner
{
    public partial class HomeBannerFormComponent : ComponentBase
    {

        private HomeBannerModelForm model = new HomeBannerModelForm();

        [Parameter]
        public List<HomeBannerModel> modelDatalhes { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IHomeDataService homeDataService { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Parameter]
        public EventCallback OnSave { get; set; }

        private void HandleProcessarImagemCallBack(string imagem)
        {
            model.LinkImagem = imagem;
            fileUploaded = true;
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            if (modelDatalhes == null)
            {
                hideTabsIdioma = true;
                model = new HomeBannerModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = modelDatalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();

                model = new HomeBannerModelForm
                {
                    Id = modelSelected.Id,
                    IdiomaId = modelSelected.IdiomaId,
                    Descricao = modelSelected.Descricao,
                    Link = modelSelected.Link,
                    LinkImagem = fileUploaded ? model.LinkImagem : modelSelected.LinkImagem,
                    Titulo = modelSelected.Titulo
                };
            }
        }

        private async Task HandleValidSubmit()
        {
            var modelSave = new HomeBannerModel
            {
                Id = model.Id,
                IdiomaId = model.IdiomaId,
                Descricao = model.Descricao,
                Link = model.Link,
                LinkImagem = model.LinkImagem,
                Titulo = model.Titulo
            };

            try
            {
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    var path = extensionMethods.CreatePathBlobFile(modelSave.Titulo, navigationManager.Uri, id);
                    
                    blobService.UploadBlobFileAsync(modelSave.LinkImagem, path);

                    modelSave.Id = id;
                    modelSave.LinkImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + path;

                    await homeDataService.HomeBannerCreate(modelSave);
                }
                else 
                {
                    if (!modelSave.LinkImagem.Contains("data:image/jpeg")) 
                    {
                        blobService.DeleteBlobAsync(modelSave.LinkImagem);
                    }
                    var path = extensionMethods.CreatePathBlobFile(modelSave.Titulo, navigationManager.Uri, model.Id);

                    blobService.UploadBlobFileAsync(modelSave.LinkImagem, path);

                    modelSave.LinkImagem = configuration.GetValue<string>("AzureStoredBlobServerUrl") + path;
                    await homeDataService.HomeBannerUpdate(modelSave);
                }

                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Banner Salvo com sucesso.", tipo = "ok" });
                await JSRuntime.InvokeAsync<object>("hideModal", "modal-form-banner");
                await OnSave.InvokeAsync();
            }
            catch (Exception e)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = e.Message, tipo = "error" });
            }
        }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");

            base.OnParametersSet();
        }
    }
}
