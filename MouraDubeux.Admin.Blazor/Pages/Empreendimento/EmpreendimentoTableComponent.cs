﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Models.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoTableComponent : ComponentBase
    {
        public List<EmpreendimentoModel> modelList { get; set; }
        public List<EstadosModel> estados { get; set; }

        private EmpreendimentoModelForm model = new EmpreendimentoModelForm();

        public Guid idToDelete { get; set; }

        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IEmpreendimentoFichaTecnicaDataService empreendimentoFichaTecnicaDataService { get; set; }

        [Inject]
        IEmpreendimentoGaleriaDataService empreendimentoGaleriaDataService { get; set; }

        [Inject]
        IEmpreendimentoInformacoesTecnicasDataService empreendimentoInformacoesTecnicasDataService { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }
        protected override void OnParametersSet()
        {
            GetTableList();
            estados = estadosDataService.GetEstados();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").OrderByDescending(x => x.DataAlteracao).ToList();
        }

        void Create()
        {
            NavigationManager.NavigateTo("Home/Empreendimento/novo");
        }

        private void Edit(IEmpreendimentoModel empreendimento)
        {
            
            NavigationManager.NavigateTo("Home/Empreendimento/editar/" + empreendimento.Id);
            //JSRuntime.InvokeAsync<object>("showModal", "modal-form-banner");
        }

        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Value.ToString()))
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.IdiomaId == "pt").ToList();
            } else
            {
                modelList = empreendimentoDataService.GetEmpreendimento().Where(x => x.Estado == e.Value.ToString() && x.IdiomaId == "pt").ToList();
            }
            StateHasChanged();
        }
        private void DeleteGaleriaConfirm(IEmpreendimentoModel empreendimentoModel)
        {
            idToDelete = empreendimentoModel.Id;
        }
        private void DeleteGaleria(IEmpreendimentoModel empreendimentoModel)
        {
            var modelListFichaTecnicaLazer = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoFichaTecnica.Lazer).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var fichaTecnica in modelListFichaTecnicaLazer)
            {
                empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaDelete(fichaTecnica.Id);
            }

            var modelListFichaTecnicaInfraestrutura = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoFichaTecnica.Infraestrutura).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var tecnicaInfraestrutura in modelListFichaTecnicaInfraestrutura)
            {
                empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaDelete(tecnicaInfraestrutura.Id);
            }

            var modelListFichaTecnicaServicosProximos= empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoFichaTecnica.ServicosProximos).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var servicosProximos in modelListFichaTecnicaServicosProximos)
            {
                empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaDelete(servicosProximos.Id);
            }

            var modelListGaleriaImovel = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.Imovel).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaImovel in modelListGaleriaImovel)
            {
                if (!galeriaImovel.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaImovel.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaImovel.Id);
            }

            var modelListGaleriaPlanta = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.Planta).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaPlanta in modelListGaleriaPlanta)
            {
                if (!galeriaPlanta.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaPlanta.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaPlanta.Id);
            }

            var modelListGaleriaStatusDaObra = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, empreendimentoModel.Id, (int)EnumTipoGaleria.StatusDaObra).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var galeriaStatusDaObra in modelListGaleriaStatusDaObra)
            {
                if (!galeriaStatusDaObra.EhVideo)
                {
                    blobService.DeleteBlobAsync(galeriaStatusDaObra.UrlImagem);
                }
                empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(galeriaStatusDaObra.Id);
            }

            var modelListInformacoesTecnicas = empreendimentoInformacoesTecnicasDataService.GetEmpreendimentoInformacoesTecnicas(Guid.Empty, empreendimentoModel.Id).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var informacoesTecnicas in modelListInformacoesTecnicas)
            {
                empreendimentoInformacoesTecnicasDataService.EmpreendimentoInformacoesTecnicasDelete(informacoesTecnicas.Id);
            }

            var modelListValorDescricao = empreendimentoValorDescricaoDataService.GetEmpreendimentoValorDescricao(Guid.Empty, empreendimentoModel.Id).Where(x => x.IdiomaId == "pt").ToList();
            foreach (var valorDescricao in modelListValorDescricao)
            {
                empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoDelete(valorDescricao.Id);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlBanner))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlBanner);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlImagemCampanha))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlImagemCampanha);
            }
            if (!string.IsNullOrEmpty(empreendimentoModel.UrlLogo))
            {
                blobService.DeleteBlobAsync(empreendimentoModel.UrlLogo);
            }
            List<Guid> modelListEmpreendimentoRelacionado = empreendimentoDataService.GetEmpreendimentoRelacionado(empreendimentoModel.Id).ToList();
            foreach (var idEmreendimentoRelacionado in modelListEmpreendimentoRelacionado)
            {
                empreendimentoDataService.EmpreendimentoRelacionadoDelete(empreendimentoModel.Id, idEmreendimentoRelacionado);
            }
            empreendimentoDataService.EmpreendimentoDelete(idToDelete);
            GetTableList();
            StateHasChanged();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }
    }
}
