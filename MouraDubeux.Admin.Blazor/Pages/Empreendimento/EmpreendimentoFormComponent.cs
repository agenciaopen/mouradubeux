﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Models.Estados;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoFormComponent : ComponentBase
    {
        [Parameter]
        public String id { get; set; }

        public List<EmpreendimentoModel> modelDetalhes { get; set; }
        public string[] bairrosList { get; set; }
        public List<EstadosModel> estados { get; set; }

        public List<CidadesModel> cidades { get; set; } = new List<CidadesModel>();

        public EmpreendimentoModelForm modelToDeleteTour { get; set; } = null;

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoDataService empreendimentoDataService { get; set; }

        [Inject]
        ICidadesDataService cidadesDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        NavigationManager navigationManager { get; set; }

        private EmpreendimentoModelForm model = new EmpreendimentoModelForm();

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public bool novoRegistro = true;

        public bool alreadySave { get; set; } = false;
        public string idiomaModal { get; set; }
        protected override void OnAfterRender(bool firstRender)
        {
            if (firstRender)
            {
                JSRuntime.InvokeVoidAsync("initialize", null);
                StateHasChanged();
            }
            bairrosList = empreendimentoDataService.GetEmpreendimento().Where(x => x.Bairro != null).Select(p => p.Bairro).Distinct().ToArray();
        }
        private void HandleProcessarIdiomaModel(string idioma)
        {
            if (!alreadySave)
            {
                alreadySave = false;

                if (!string.IsNullOrEmpty(idioma))
                {
                    idiomaModal = idioma;
                }
                if (id == null)
                {
                    hideTabsIdioma = true;
                    model = new EmpreendimentoModelForm { };
                    model.IdiomaId = idioma;
                    model.UrlBanner = "";
                    model.UrlImagemCard = "";
                    model.UrlLogo = "";
                    estados = estadosDataService.GetEstados();
                    cidades = new List<CidadesModel>();
                }
                else
                {
                    hideTabsIdioma = false;
                    modelDetalhes = empreendimentoDataService.GetEmpreendimento(Guid.Parse(id));

                    var modelSelected = modelDetalhes.Where(p => p.IdiomaId == idiomaModal).FirstOrDefault();
                    modelSelected.UrlBanner = fileUploaded ? model.UrlBanner : modelSelected.UrlBanner;
                    modelSelected.UrlLogo = fileUploaded ? model.UrlLogo : modelSelected.UrlLogo;
                    modelSelected.UrlImagemCard = fileUploaded ? model.UrlImagemCard : modelSelected.UrlImagemCard;
                    modelSelected.UrlImagemCampanha = fileUploaded ? model.UrlImagemCampanha : modelSelected.UrlImagemCampanha;
                    if (string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                    {
                        cidades = new List<CidadesModel>();
                    } else if (!string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                    {
                        cidades = cidadesDataService.GetCidades("", model.Estado);
                    }
                    else if (!string.IsNullOrEmpty(modelSelected.Estado) && !string.IsNullOrEmpty(modelSelected.Cidade))
                    {
                        cidades = cidadesDataService.GetCidades("", modelSelected.Estado);
                    }
                    else
                    {
                        cidades = new List<CidadesModel>();
                    }
                    estados = estadosDataService.GetEstados();
                    model = _mapper.Map<EmpreendimentoModelForm>(modelSelected);
                    novoRegistro = false;
                }
            }
        }
        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            StateHasChanged();
            base.OnParametersSet();
        }
        private async Task HandleValidSubmit()
        {
            var modelSave = _mapper.Map<EmpreendimentoModelUpdateCreate>(model);

            if (modelSave.Id == Guid.Empty)
            {
                var id = Guid.NewGuid();
                modelSave.Id = id;
                modelSave.Slug = extensionMethods.CreateSlug(modelSave.Nome);
                modelSave.DataAlteracao = DateTime.Now;
                modelSave.DataCriacao = DateTime.Now;

                var empreendimentoValorDescricaoModel = new EmpreendimentoValorDescricaoModel
                {
                    Id = Guid.NewGuid(),
                    IdEmpreendimento = id,
                    Descricao = "",
                    Valor = "0",
                    Titulo = "-"
                };

                empreendimentoDataService.EmpreendimentoCreate(modelSave);
                empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoCreate(empreendimentoValorDescricaoModel);

                NavigationManager.NavigateTo("Home/Empreendimento/editar/" + modelSave.Id);
            }
            else
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");
                var pathBanner = "";
                var pathLogo = "";

                if (modelSave.UrlBanner != null && modelSave.UrlBanner.Contains("data:image/jpeg"))
                {
                    pathBanner = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_banner", "/banner", model.Id);
                    var urlBannerAntiga = modelDetalhes.FirstOrDefault().UrlBanner;
                    if (!string.IsNullOrEmpty(urlBannerAntiga))
                    {
                        blobService.DeleteBlobAsync(urlBannerAntiga);
                    }
                    blobService.UploadBlobFileAsync(modelSave.UrlBanner, pathBanner);
                    modelSave.UrlBanner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
                }

                if(modelSave.UrlImagemCampanha != null && modelSave.UrlImagemCampanha.Contains("data:image/jpeg"))
                {
                    string pathImagemCampanha = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_imagemCampanha", "/imagemCampanha", model.Id);
                    string urlImagemCampanhaAntiga = modelDetalhes.FirstOrDefault().UrlImagemCampanha;
                    if (!string.IsNullOrEmpty(urlImagemCampanhaAntiga))
                        blobService.DeleteBlobAsync(urlImagemCampanhaAntiga);

                    blobService.UploadBlobFileAsync(modelSave.UrlImagemCampanha, pathImagemCampanha);
                    modelSave.UrlImagemCampanha = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathImagemCampanha;
                }

                if (modelSave.UrlImagemCard != null && modelSave.UrlImagemCard.Contains("data:image/jpeg"))
                {
                    string pathImagemCard = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_imagemCard", "/imagemCampanha", model.Id);
                    string urlImagemCardAntiga = modelDetalhes.FirstOrDefault().UrlImagemCard;
                    if (!string.IsNullOrEmpty(urlImagemCardAntiga))
                        blobService.DeleteBlobAsync(urlImagemCardAntiga);

                    blobService.UploadBlobFileAsync(modelSave.UrlImagemCard, pathImagemCard);
                    modelSave.UrlImagemCard = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathImagemCard;
                }

                if (modelSave.UrlLogo != null && modelSave.UrlLogo.Contains("data:image/jpeg"))
                {
                    pathLogo = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_logo", "/logo", model.Id);
                    var urlLogoAntiga = modelDetalhes.FirstOrDefault().UrlLogo;
                    if (!string.IsNullOrEmpty(urlLogoAntiga))
                    {
                        blobService.DeleteBlobAsync(urlLogoAntiga);
                    }
                    blobService.UploadBlobFileAsync(modelSave.UrlLogo, pathLogo);
                    modelSave.UrlLogo = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathLogo;
                }
       
                JSRuntime.InvokeAsync<object>("enableButtons", "");

                modelSave.DataAlteracao = DateTime.Now;
                modelSave.Slug = extensionMethods.CreateSlug(modelSave.Nome);
                empreendimentoDataService.EmpreendimentoUpdate(modelSave);
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                HandleProcessarIdiomaModel("");
                StateHasChanged();
            }
        }

        private void HandleStateChange()
        {
            StateHasChanged();
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            cidades = cidadesDataService.GetCidades("", e.Value.ToString());
            StateHasChanged();
        }

        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlBanner = imagem;
            }
            fileUploaded = true;
        }

        private void HandleProcessarUrlImagemCampanhaCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
                model.UrlImagemCampanha = imagem;
            fileUploaded = true;
        }

        private void HandleProcessarUrlImagemCardCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
                model.UrlImagemCard = imagem;
            fileUploaded = true;
        }

        private void HandleProcessarUrlLogoCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlLogo = imagem;
            }
            fileUploaded = true;
        }

        private void DeleteTour()
        {
            string deletePath = @".\wwwroot\tour\" + modelToDeleteTour.TourVirtual;
            if (Directory.Exists(deletePath))
                Directory.Delete(deletePath, true);

            var modelSave = _mapper.Map<EmpreendimentoModelUpdateCreate>(modelToDeleteTour);
            modelSave.TourVirtual = null;
            empreendimentoDataService.EmpreendimentoUpdate(modelSave);
            modelToDeleteTour = null;
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Tour removido com sucesso.", tipo = "ok" });
            HandleProcessarIdiomaModel("");
            StateHasChanged();
        }
        private void DeleteGaleriaConfirm(EmpreendimentoModelForm empreendimentoModelForm)
        {
            modelToDeleteTour = empreendimentoModelForm;
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
        }
        private async Task <IEnumerable<string>> buscarBairros(string palavraChave)
        {

            var result = await Task.FromResult(bairrosList.Where(x => x.ToLower().Contains(palavraChave.ToLower())).ToList());

            if (result.Count == 0)
            {
                string[] strArrPalavraChave = new string[] { palavraChave };
                result = await Task.FromResult(strArrPalavraChave.ToList());
            }

            return result;
        }
    }
}
