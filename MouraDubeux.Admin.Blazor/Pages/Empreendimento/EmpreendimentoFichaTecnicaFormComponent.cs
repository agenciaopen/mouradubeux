﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoFichaTecnicaFormComponent : ComponentBase
    {
        private EmpreendimentoFichaTecnicaModelForm model = new EmpreendimentoFichaTecnicaModelForm();


        [Parameter]
        public List<EmpreendimentoFichaTecnicaModel> empreendimentoFichaTecnicaModelDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]
        public Guid idEmpreendimento { get; set; }

        [Parameter]
        public int tipoFichaTecnica { get; set; }


        [Parameter]
        public Guid idValorDescricao { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoFichaTecnicaDataService empreendimentoFichaTecnicaDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }
        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (empreendimentoFichaTecnicaModelDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new EmpreendimentoFichaTecnicaModelForm { };
                model.IdiomaId = idioma;
                model.TipoFichaTecnica = tipoFichaTecnica;

            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = empreendimentoFichaTecnicaModelDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<EmpreendimentoFichaTecnicaModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<EmpreendimentoFichaTecnicaModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    modelSave.Id = id;
                    modelSave.IdValorDescricao = idValorDescricao == Guid.Empty ? Guid.Empty : idValorDescricao;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaCreate(modelSave);
                }
                else
                {
                    empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                var modal = (EnumTipoFichaTecnica)tipoFichaTecnica;
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Ficha-Tecnica-" + modal);
                ProcessarEventCallback.InvokeAsync();
            }
            alreadySave = false;
        }
    }
}
