﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoGaleriaTableComponent : ComponentBase
    {
        public List<EmpreendimentoGaleriaModel> modelList { get; set; }

        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaFotosDetalhes { get; set; }
        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaPlantasDetalhes { get; set; }
        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaStatusDaObraDetalhes { get; set; }
        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaAreaComumDetalhes { get; set; }
        public List<EmpreendimentoGaleriaModel> empreendimentoGaleriaDecoradoDetalhes { get; set; }

        [Parameter]
        public int TipoGaleria { get; set; }

        [Parameter]
        public Guid idEmpreendimento { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoGaleriaDataService empreendimentoGaleriaDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(Guid.Empty, idEmpreendimento, TipoGaleria).Where(x => x.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            empreendimentoGaleriaFotosDetalhes = null;
            empreendimentoGaleriaPlantasDetalhes = null;
            empreendimentoGaleriaStatusDaObraDetalhes = null;
            empreendimentoGaleriaAreaComumDetalhes = null;
            empreendimentoGaleriaDecoradoDetalhes = null;

            var modal = (EnumTipoGaleria)TipoGaleria;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-" + modal);
            StateHasChanged();
        }

        void Order()
        {
            var modal = (EnumTipoGaleria)TipoGaleria;
            JSRuntime.InvokeAsync<object>("showModal", "modal-galeria-order-" + modal);
        }
        void Edit(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            empreendimentoGaleriaFotosDetalhes = null;
            empreendimentoGaleriaPlantasDetalhes = null;
            empreendimentoGaleriaStatusDaObraDetalhes = null;
            empreendimentoGaleriaAreaComumDetalhes = null;
            empreendimentoGaleriaDecoradoDetalhes = null;
            switch (empreendimentoGaleria.TipoGaleria)
            {
                case (int)EnumTipoGaleria.Imovel:
                    empreendimentoGaleriaFotosDetalhes = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(empreendimentoGaleria.Id, idEmpreendimento, empreendimentoGaleria.TipoGaleria);
                    break;
                case (int)EnumTipoGaleria.Planta:
                    empreendimentoGaleriaPlantasDetalhes = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(empreendimentoGaleria.Id, idEmpreendimento, empreendimentoGaleria.TipoGaleria);
                    break;
                case (int)EnumTipoGaleria.StatusDaObra:
                    empreendimentoGaleriaStatusDaObraDetalhes = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(empreendimentoGaleria.Id, idEmpreendimento, empreendimentoGaleria.TipoGaleria);
                    break;
                case (int)EnumTipoGaleria.AreaComum:
                    empreendimentoGaleriaAreaComumDetalhes = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(empreendimentoGaleria.Id, idEmpreendimento, empreendimentoGaleria.TipoGaleria);
                    break;
                case (int)EnumTipoGaleria.Decorado:
                    empreendimentoGaleriaDecoradoDetalhes = empreendimentoGaleriaDataService.GetEmpreendimentoGaleria(empreendimentoGaleria.Id, idEmpreendimento, empreendimentoGaleria.TipoGaleria);
                    break;
                default:
                    break;
            }

            var modal = (EnumTipoGaleria)TipoGaleria;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-" + modal);
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteGaleriaConfirm(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            idToDelete = empreendimentoGaleria.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }
        private void DeleteGaleria(IEmpreendimentoGaleriaModel empreendimentoGaleria)
        {
            empreendimentoGaleriaDataService.EmpreendimentoGaleriaDelete(empreendimentoGaleria.Id);
            if (!empreendimentoGaleria.EhVideo)
            {
                blobService.DeleteBlobAsync(empreendimentoGaleria.UrlImagem);
            }
            ProcessarEventCallback.InvokeAsync();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
        }
    }
}
