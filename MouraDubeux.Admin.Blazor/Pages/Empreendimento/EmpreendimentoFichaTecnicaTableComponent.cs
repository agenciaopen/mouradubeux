﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoFichaTecnicaTableComponent : ComponentBase
    {
        private EmpreendimentoFichaTecnicaModelForm model = new EmpreendimentoFichaTecnicaModelForm();

        public List<EmpreendimentoFichaTecnicaModel> modelList { get; set; }
        public List<EmpreendimentoValorDescricaoModel> valorDescricaoList { get; set; }

        public List<EmpreendimentoFichaTecnicaModel> empreendimentoFichaTecnicaModelDetalhes { get; set; }

        [Parameter]
        public Guid idEmpreendimento { get; set; }

        public Guid idValorDescricao { get; set; }

        [Parameter]
        public int tipoFichaTecnica { get; set; }


        [Parameter]
        public int tipoEmpreendimento { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoFichaTecnicaDataService empreendimentoFichaTecnicaDataService { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            GetValorDescricaoTableList();
            base.OnParametersSet();
        }

        private void GetTableList()
        {
            if (model.IdValorDescricao == Guid.Empty)
            {
                modelList = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, idEmpreendimento, tipoFichaTecnica).Where(x => x.IdiomaId == "pt" && x.IdValorDescricao == Guid.Empty).ToList();

            } else
            {
                modelList = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, idEmpreendimento, tipoFichaTecnica).Where(x => x.IdiomaId == "pt" && x.IdValorDescricao == model.IdValorDescricao).ToList();

            }

        }

        void Create()
        {
            empreendimentoFichaTecnicaModelDetalhes = null;
            var modal = (EnumTipoFichaTecnica)tipoFichaTecnica;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Ficha-Tecnica-" + modal);
            JSRuntime.InvokeAsync<object>("selectpicker", null);
            StateHasChanged();
        }
        private void DeleteGaleriaConfirm(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnicaModel)
        {
            idToDelete = empreendimentoFichaTecnicaModel.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }

        private void DeleteGaleria(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnicaModel)
        {
            empreendimentoFichaTecnicaDataService.EmpreendimentoFichaTecnicaDelete(empreendimentoFichaTecnicaModel.Id);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            ProcessarEventCallback.InvokeAsync();
        }
        void Edit(IEmpreendimentoFichaTecnicaModel empreendimentoFichaTecnicaModel)
        {
            empreendimentoFichaTecnicaModelDetalhes = null;
            empreendimentoFichaTecnicaModelDetalhes = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(empreendimentoFichaTecnicaModel.Id, empreendimentoFichaTecnicaModel.IdEmpreendimento, empreendimentoFichaTecnicaModel.TipoFichaTecnica);
            var modal = (EnumTipoFichaTecnica)tipoFichaTecnica;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-Ficha-Tecnica-" + modal);
            JSRuntime.InvokeAsync<object>("selectpicker", null);
        }
        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        protected void HandleClickValorDescricaoSelect(ChangeEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Value.ToString()) || e.Value.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                modelList = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, idEmpreendimento, tipoFichaTecnica).Where(x => x.IdiomaId == "pt" && x.IdValorDescricao == Guid.Empty).ToList();
            }
            else
            {
                modelList = empreendimentoFichaTecnicaDataService.GetEmpreendimentoFichaTecnica(Guid.Empty, idEmpreendimento, tipoFichaTecnica).Where(x => x.IdiomaId == "pt" && x.IdValorDescricao == new Guid(e.Value.ToString())).ToList();
            }
        }
        private void GetValorDescricaoTableList()
        {
            valorDescricaoList = empreendimentoValorDescricaoDataService.GetEmpreendimentoValorDescricao(Guid.Empty, idEmpreendimento).Where(x => x.IdiomaId == "pt").ToList();
        }
    }
}
