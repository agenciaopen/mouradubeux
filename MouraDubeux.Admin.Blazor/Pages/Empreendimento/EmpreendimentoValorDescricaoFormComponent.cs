﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoValorDescricaoFormComponent : ComponentBase
    {

        private EmpreendimentoValorDescricaoModelForm model = new EmpreendimentoValorDescricaoModelForm();


        [Parameter]
        public List<EmpreendimentoValorDescricaoModel> empreendimentoValorDescricaoDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]
        public Guid idEmpreendimento { get; set; }

        [Parameter]
        public int TipoEmpreendimento { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoValorDescricaoDataService empreendimentoValorDescricaoDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }
        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            alreadySave = false;
            idiomaModal = idioma;
            if (empreendimentoValorDescricaoDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new EmpreendimentoValorDescricaoModelForm { };
                model.IdiomaId = idioma;
                model.Titulo = "-";

            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = empreendimentoValorDescricaoDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<EmpreendimentoValorDescricaoModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                alreadySave = true;
                JSRuntime.InvokeAsync<object>("disableButtons", "");
                var modelSave = _mapper.Map<EmpreendimentoValorDescricaoModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    modelSave.Id = id;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    modelSave.Slug = extensionMethods.CreateSlug(modelSave.Descricao);
                    empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoCreate(modelSave);
                    Thread.Sleep(1000);
                    JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                    JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Valor-Descricao");
                    JSRuntime.InvokeAsync<object>("enableButtons", "");
                    ProcessarEventCallback.InvokeAsync();
                }
                else
                {
                    modelSave.Slug = extensionMethods.CreateSlug(modelSave.Descricao);
                    empreendimentoValorDescricaoDataService.EmpreendimentoValorDescricaoUpdate(modelSave);
                    Thread.Sleep(1000);
                    JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                    JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Valor-Descricao");
                    JSRuntime.InvokeAsync<object>("enableButtons", "");
                    ProcessarEventCallback.InvokeAsync();
                }
            }
            alreadySave = false;
        }
    }
}
