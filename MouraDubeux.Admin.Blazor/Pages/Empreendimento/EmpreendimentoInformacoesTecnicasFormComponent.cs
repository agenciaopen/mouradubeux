﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Empreendimento
{
    public partial class EmpreendimentoInformacoesTecnicasFormComponent : ComponentBase
    {
        private EmpreendimentoInformacoesTecnicasModelForm model = new EmpreendimentoInformacoesTecnicasModelForm();


        [Parameter]
        public List<EmpreendimentoInformacoesTecnicasModel> empreendimentoInformacoesTecnicasDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Parameter]
        public Guid idEmpreendimento { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEmpreendimentoInformacoesTecnicasDataService empreendimentoInformacoesTecnicasDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (empreendimentoInformacoesTecnicasDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new EmpreendimentoInformacoesTecnicasModelForm { };
                model.IdiomaId = idioma;
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = empreendimentoInformacoesTecnicasDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                model = _mapper.Map<EmpreendimentoInformacoesTecnicasModelForm>(modelSelected);
            }
        }
        private void HandleValidSubmit()
        {
            if (!alreadySave)
            { 
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<EmpreendimentoInformacoesTecnicasModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();
                    modelSave.Id = id;
                    modelSave.IdEmpreendimento = idEmpreendimento;
                    empreendimentoInformacoesTecnicasDataService.EmpreendimentoInformacoesTecnicasCreate(modelSave);
                }
                else
                {
                    empreendimentoInformacoesTecnicasDataService.EmpreendimentoInformacoesTecnicasUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");

                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Informacoes-Tecnicas");
                ProcessarEventCallback.InvokeAsync();
            }
            alreadySave = false;
        }
    }
}
