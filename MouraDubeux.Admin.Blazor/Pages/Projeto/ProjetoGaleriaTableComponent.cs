﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Data.Data.Projeto;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Projeto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.Projeto
{
    public partial class ProjetoGaleriaTableComponent : ComponentBase
    {
        public List<ProjetoGaleriaModel> modelList { get; set; }

        public List<ProjetoGaleriaModel> projetoGaleriaDetalhes { get; set; }

        [Parameter]
        public Guid idProjeto { get; set; }
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IProjetoGaleriaDataService projetoGaleriaDataService { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        protected override void OnParametersSet()
        {
            GetTableList();
            base.OnParametersSet();
        }
        private void GetTableList()
        {
            modelList = projetoGaleriaDataService.GetProjetoGaleria(Guid.Empty, idProjeto).Where(x => x.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            projetoGaleriaDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-ProjetoGaleria");
            StateHasChanged();
        }

        void Edit(IProjetoGaleriaModel projetoGaleria)
        {
            projetoGaleriaDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-ProjetoGaleria");
        }

        private void HandleProcessarCallBack()
        {
            ProcessarEventCallback.InvokeAsync();
        }

        private void DeleteGaleriaConfirm(IProjetoGaleriaModel projetoGaleria)
        {
            idToDelete = projetoGaleria.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }
        private void DeleteGaleria(IProjetoGaleriaModel projetoGaleria)
        {
            projetoGaleriaDataService.ProjetoGaleriaDelete(projetoGaleria.Id);
            if (!projetoGaleria.EhVideo)
            {
                blobService.DeleteBlobAsync(projetoGaleria.UrlImagem);
            }
            ProcessarEventCallback.InvokeAsync();
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Remoção feita com sucesso.", tipo = "ok" });
        }
    }
}
