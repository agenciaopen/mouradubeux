﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.MdMidia;
using MouraDubeux.Admin.Blazor.Models.Paginas;
using MouraDubeux.Admin.Data.Data.Interfaces.Pagina;
using MouraDubeux.Admin.Data.Data.MdMidia;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.MdMidia;
using MouraDubeux.Admin.Data.Models.Pagina;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Pages.MdMidia
{
    public partial class MdMidiaTableComponent
    {
        public const string pagina = "sala-de-imprensa";
        public List<PaginaModel> modelDetalhes { get; set; }

        private PaginaSalaDeImprensaModelForm model = new PaginaSalaDeImprensaModelForm();

        public List<MdMidiaModel> modelList { get; set; }
        public List<MdMidiaModel> mdMidiaModelDetalhes { get; set; }

        [Parameter]
        public Guid idToDelete { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IPaginaDataService paginaDataService { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        IMdMidiaDataService mdMidiaDataService { get; set; }

        public bool hideTabsIdioma = false;
        public bool fileUploaded = false;
        public bool novoRegistro = true;
        public bool alreadySave { get; set; } = false;
        public string idiomaModal { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");

            GetTableList();
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            if (!alreadySave)
            {
                alreadySave = false;

                if (!string.IsNullOrEmpty(idioma))
                {
                    idiomaModal = idioma;
                }

                hideTabsIdioma = false;
                modelDetalhes = paginaDataService.GetPagina(pagina);
                var modelSelected = modelDetalhes.Where(p => p.IdiomaId == idiomaModal).FirstOrDefault();
                model = _mapper.Map<PaginaSalaDeImprensaModelForm>(modelSelected);
                novoRegistro = false;
            }
        }

        private async Task HandleValidSubmit()
        {
            try
            {
                var modelSave = _mapper.Map<PaginaModel>(model);
                var pathBanner = "";
                var pathLogo = "";

                if (modelSave.Banner != null && modelSave.Banner.Contains("data:image/jpeg"))
                {
                    pathBanner = extensionMethods.CreatePathBlobFilePagina("/banner", model.Slug);
                    var urlBannerAntiga = modelDetalhes.FirstOrDefault().Banner;
                    if (!string.IsNullOrEmpty(urlBannerAntiga))
                    {
                        blobService.DeleteBlobAsync(urlBannerAntiga);
                    }
                    blobService.UploadBlobFileAsync(modelSave.Banner, pathBanner);
                    modelSave.Banner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathBanner;
                }

                paginaDataService.PaginaUpdate(modelSave);
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
            }
            catch (Exception)
            {
                await JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Ops, ocorreu algum erro ao salvar. Tente novamente.", tipo = "error" });
            }
        }
        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.Banner = imagem;
            }
            fileUploaded = true;
        }

        private void GetTableList()
        {
            modelList = mdMidiaDataService.GetMdMidia().Where(x => x.IdiomaId == "pt").ToList();
        }

        void Create()
        {
            mdMidiaModelDetalhes = null;
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-MdMidia");
            JSRuntime.InvokeAsync<object>("selectpicker", null);
            StateHasChanged();
        }
        private void DeleteConfirm(IMdMidiaModel mdMidiaModel)
        {
            idToDelete = mdMidiaModel.Id;
            //JSRuntime.InvokeAsync<object>("showPopover", "popoverDeleteConfirm");
        }

        private void Delete(IMdMidiaModel mdMidiaModel)
        {
            mdMidiaDataService.MdMidiaDelete(mdMidiaModel.Id);
            JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Remoção feita com sucesso.", tipo = "ok" });
            HandleProcessarCallBack();
        }
        void Edit(IMdMidiaModel mdMidiaModel)
        {
            mdMidiaModelDetalhes = null;
            mdMidiaModelDetalhes = mdMidiaDataService.GetMdMidia(mdMidiaModel.Id);
            JSRuntime.InvokeAsync<object>("showModal", "modal-form-MdMidia");
            JSRuntime.InvokeAsync<object>("selectpicker", null);
        }
        private void HandleProcessarCallBack()
        {
            GetTableList();
            ProcessarEventCallback.InvokeAsync();
        }
    }
}
