﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Escritorio;
using MouraDubeux.Admin.Data.Data.Escritorio;
using MouraDubeux.Admin.Data.Enum;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Admin.Data.Models.Cidades;
using MouraDubeux.Admin.Data.Models.Estados;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MouraDubeux.Admin.Blazor.Pages.Escritorio
{
    public partial class EscritorioFormComponent : ComponentBase
    {
        private EscritorioModelForm model = new EscritorioModelForm();

        [Parameter]
        public List<EscritorioModel> escritorioModelDetalhes { get; set; }

        [Parameter]
        public EventCallback ProcessarEventCallback { get; set; }

        public List<EstadosModel> estados { get; set; }

        public List<CidadesModel> cidades { get; set; } = new List<CidadesModel>();

        public bool hideTabsIdioma = false;

        public bool fileUploaded = false;
        public string idiomaModal { get; set; }

        public bool alreadySave { get; set; }

        [Inject]
        IJSRuntime JSRuntime { get; set; }

        [Inject]
        IExtensionMethods extensionMethods { get; set; }

        [Inject]
        IEscritorioDataService escritorioDataService { get; set; }

        [Inject]
        ICidadesDataService cidadesDataService { get; set; }

        [Inject]
        IEstadosDataService estadosDataService { get; set; }

        [Inject]
        IMapper _mapper { get; set; }

        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        IBlobService blobService { get; set; }

        [Inject]
        IConfiguration configuration { get; set; }

        protected override void OnParametersSet()
        {
            HandleProcessarIdiomaModel("pt");
            base.OnParametersSet();
        }

        private void HandleProcessarIdiomaModel(string idioma)
        {
            idiomaModal = idioma;
            alreadySave = false;

            if (escritorioModelDetalhes == null)
            {
                hideTabsIdioma = true;
                model = new EscritorioModelForm { };
                model.IdiomaId = idioma;
                estados = estadosDataService.GetEstados();
                cidades = new List<CidadesModel>();
            }
            else
            {
                hideTabsIdioma = false;
                var modelSelected = escritorioModelDetalhes.Where(p => p.IdiomaId == idioma).FirstOrDefault();
                if (string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    cidades = new List<CidadesModel>();
                }
                else if (!string.IsNullOrEmpty(modelSelected.Estado) && string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    cidades = cidadesDataService.GetCidades("", model.Estado);
                }
                else if (!string.IsNullOrEmpty(modelSelected.Estado) && !string.IsNullOrEmpty(modelSelected.Cidade))
                {
                    cidades = cidadesDataService.GetCidades("", modelSelected.Estado);
                }
                else
                {
                    cidades = new List<CidadesModel>();
                }
                estados = estadosDataService.GetEstados();

                model = _mapper.Map<EscritorioModelForm>(modelSelected);
            }
        }

        private void HandleValidSubmit()
        {
            if (!alreadySave)
            {
                JSRuntime.InvokeAsync<object>("disableButtons", "");

                var modelSave = _mapper.Map<EscritorioModel>(model);
                if (modelSave.Id == Guid.Empty)
                {
                    var id = Guid.NewGuid();

                    var pathUrlBanner = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_escritorio", "/banner/", id);
                    blobService.UploadBlobFileAsync(modelSave.UrlBanner, pathUrlBanner);
                    modelSave.UrlBanner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlBanner;

                    modelSave.Id = id;
                    escritorioDataService.EscritorioCreate(modelSave);
                }
                else
                {
                    var pathUrlBanner = "";

                    if (modelSave.UrlBanner != null && modelSave.UrlBanner.Contains("data:image/jpeg"))
                    {
                        pathUrlBanner = extensionMethods.CreatePathBlobFile(modelSave.Nome + "_escritorio", "/banner", model.Id);
                        var urlBannerAntiga = escritorioModelDetalhes.FirstOrDefault().UrlBanner;
                        if (!string.IsNullOrEmpty(urlBannerAntiga))
                        {
                            blobService.DeleteBlobAsync(urlBannerAntiga);
                        }
                        blobService.UploadBlobFileAsync(modelSave.UrlBanner, pathUrlBanner);
                        modelSave.UrlBanner = configuration.GetValue<string>("AzureStoredBlobServerUrl") + pathUrlBanner;
                    }

                    escritorioDataService.EscritorioUpdate(modelSave);
                }
                JSRuntime.InvokeAsync<object>("enableButtons", "");
                Thread.Sleep(500);
                JSRuntime.InvokeAsync<object>("sendMessage", new { mensagem = "Edição feita com sucesso.", tipo = "ok" });
                JSRuntime.InvokeAsync<object>("hideModal", "modal-form-Escritorio");
                ProcessarEventCallback.InvokeAsync();
            }
            alreadySave = false;
        }
        protected void HandleFirstDropDownChange(ChangeEventArgs e)
        {
            cidades = cidadesDataService.GetCidades("", e.Value.ToString());
            StateHasChanged();
        }

        private void HandleProcessarUrlBannerCallBack(string imagem)
        {
            if (!string.IsNullOrEmpty(imagem))
            {
                model.UrlBanner = imagem;
            }
            fileUploaded = true;
        }
    }
}
