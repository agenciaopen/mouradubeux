﻿using MouraDubeux.Admin.Data.Models.MdMidia;
using System;
using System.ComponentModel.DataAnnotations;

namespace MouraDubeux.Admin.Blazor.Models.MdMidia
{
    public class MdMidiaModelForm : IMdMidiaModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Conteudo { get; set; }
        public DateTime DataPublicacao { get; set; }
        public string UrlImagem { get; set; }
        public string Link { get; set; }
    }
}
