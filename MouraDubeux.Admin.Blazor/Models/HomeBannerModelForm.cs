﻿using MouraDubeux.Admin.Data.Models.Home;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Models
{
    public class HomeBannerModelForm : IHomeBannerModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }

        [Required]
        public string Descricao { get; set; }
        [Required]
        public string Link { get; set; }
        [Required]
        public string LinkImagem { get; set; }

        [Required]
        public string Titulo { get; set; }

    }
}
