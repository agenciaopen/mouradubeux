﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Models.Empreendimento
{
    public class EmpreendimentoFichaTecnicaModelForm
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public Guid IdValorDescricao { get; set; }
        public string IdiomaId { get; set; }
        public string UrlImagem { get; set; }
        public int TipoFichaTecnica { get; set; }
        public int Peso { get; set; }
    }
}
