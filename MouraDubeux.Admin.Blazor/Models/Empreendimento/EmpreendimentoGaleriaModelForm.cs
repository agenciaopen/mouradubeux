﻿using MouraDubeux.Admin.Data.Models.Empreendimento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MouraDubeux.Admin.Blazor.Models.Empreendimento
{
    public class EmpreendimentoGaleriaModelForm : IEmpreendimentoGaleriaModel
    {
        public Guid Id { get; set; }
        public Guid IdEmpreendimento { get; set; }
        public string IdiomaId { get; set; }
        public string UrlImagem { get; set; }
        public string UrlVideo { get; set; }
        public string Descricao { get; set; }
        public int TipoGaleria { get; set; }
        public string Titulo { get; set; }
        public bool EhVideo { get; set; }

        public string GetIdUrlVideo()
        {
            var uri = new Uri(UrlVideo);
            var query = HttpUtility.ParseQueryString(uri.Query);
            return query["v"];
        }
    }
}
