﻿using MouraDubeux.Admin.Blazor.Models.Paginas.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Models.Paginas
{
    public class SEOModelForm : SEOComponent
    {
        public string Slug { get; set; }
        public string IdiomaId { get; set; }
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
        public string Banner { get; set; }
        public string Link { get; set; }
        public string Imagem { get; set; }
        public string Descricao { get; set; }
        public string MataTittle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
    }
}
    