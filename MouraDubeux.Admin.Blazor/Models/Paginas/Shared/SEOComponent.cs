﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Models.Paginas.Shared
{
    public interface SEOComponent
    {
        public string Descricao { get; set; }
        public string MataTittle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
    }
}