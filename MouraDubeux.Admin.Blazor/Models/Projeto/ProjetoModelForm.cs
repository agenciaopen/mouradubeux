﻿using MouraDubeux.Admin.Data.Models.Projeto;
using System;
using System.ComponentModel.DataAnnotations;

namespace MouraDubeux.Admin.Blazor.Models.Projeto
{
    public class ProjetoModelForm : IProjetoModel
    {
        public Guid Id { get; set; }
        public string IdiomaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Endereco { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string UrlImagem { get; set; }
        public bool ProjetoSocial { get; set; }
        public string EstadoNome { get; set; }
        public string CidadeNome { get; set; }
    }
}
