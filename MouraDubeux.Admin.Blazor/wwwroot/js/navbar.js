window.initNavbar = function () {
    $(".trigger").on('click', function(){
        if($(".trigger-top").hasClass("no-animation")){
            $(".menu").toggleClass("menu__open");
            $(".trigger-top").removeClass("no-animation");
            $(".trigger-middle").toggleClass("d-none");
            $(".trigger-bottom").removeClass("no-animation");
            $(this).toggleClass('is-active');
        }
        else{
            $(this).toggleClass('is-active');
            setTimeout(function() { 
                $(".menu").toggleClass("menu__open");
                $(".trigger-middle").toggleClass("d-none");
            }, 100); 
        }
      });
      $(window).scroll(function() {
        let nav = $('.navbar_main');
        let top = 50;
        let topBar = $('.acessibility');

        if ($(window).scrollTop() >= top) {
            // topBar.removeClass('fixed-top');
            nav.addClass('nav_scrolled');
    
        } else {
            topBar.addClass('fixed-top');
            nav.removeClass('nav_scrolled');
        }
    });
    
    var $affectedElements = $("p, h1, h2, h3, h4, h5, h6, span, a, li, label"); // Can be extended, ex. $("div, p, span.someClass")

    // Storing the original size in a data attribute so size can be reset
    $affectedElements.each( function(){
      var $this = $(this);
      $this.data("orig-size", $this.css("font-size") );
    });
    
    $("#btn-increase").click(function(){
      changeFontSize(1);
    })
    
    $("#btn-decrease").click(function(){
      changeFontSize(-1);
    })
    
    $("#btn-orig").click(function(){
      $affectedElements.each( function(){
            var $this = $(this);
            $this.css( "font-size" , $this.data("orig-size") );
       });
    })
    
    function changeFontSize(direction){
        $affectedElements.each( function(){
            var $this = $(this);
            $this.css( "font-size" , parseInt($this.css("font-size"))+direction );
        });
    }
}