﻿using System;
using System.Drawing;
using System.Threading.Tasks;

namespace MouraDubeux.Admin.Blazor.Data
{
    public interface IExtensionMethods
    {
        string GetBase64ImageString(Image image, int width, int height);
        string RemoveCaracteresEspeciais(string str);
        string CreatePathBlobFile(string str, string uri, Guid id);

        string CreatePathBlobFilePagina(string uri, string pagina);
        string RemoverAcentos(string texto);

        string Description(Enum enumValue);
        string CreateSlug(string str);
    }
}