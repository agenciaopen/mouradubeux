using AutoMapper;
using Azure.Storage.Blobs;
using BlazorTable;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MouraDubeux.Admin.Blazor.Areas.Identity;
using MouraDubeux.Admin.Blazor.Data;
using MouraDubeux.Admin.Blazor.Models.Empreendimento;
using MouraDubeux.Admin.Blazor.Models.Escritorio;
using MouraDubeux.Admin.Blazor.Models.Projeto;
using MouraDubeux.Admin.Blazor.Models.MdMidia;
using MouraDubeux.Admin.Data.Data;
using MouraDubeux.Admin.Data.Data.Empreendimento;
using MouraDubeux.Admin.Data.Data.Cidades;
using MouraDubeux.Admin.Data.Data.Estados;
using MouraDubeux.Admin.Data.Data.Escritorio;
using MouraDubeux.Admin.Data.Data.Projeto;
using MouraDubeux.Admin.Data.Data.MdMidia;
using MouraDubeux.Admin.Data.Data.Interfaces.Cidades;
using MouraDubeux.Admin.Data.Data.Interfaces.Estados;
using MouraDubeux.Admin.Data.DataAccess;
using MouraDubeux.Admin.Data.Models.Empreendimento;
using MouraDubeux.Admin.Data.Models.Escritorio;
using MouraDubeux.Admin.Data.Models.Projeto;
using MouraDubeux.Admin.Data.Models.MdMidia;
using MouraDubeux.DragDrop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MouraDubeux.Admin.Data.Data.Interfaces.Pagina;
using MouraDubeux.Admin.Data.Data.Pagina;
using MouraDubeux.Admin.Data.Models.Pagina;
using MouraDubeux.Admin.Blazor.Models.Paginas;
using MouraDubeux.Admin.Data.Data.Interfaces.Faq;
using MouraDubeux.Admin.Data.Data.Faq;
using MouraDubeux.Admin.Data.Models.Faq;
using MouraDubeux.Admin.Blazor.Models.Faq;

namespace MouraDubeux.Admin.Blazor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<IdentityUser>>();
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddSingleton<IExtensionMethods, ExtensionMethods>();
            services.AddSingleton<ISqlDataAccess, SqlDataAccess>();
            services.AddScoped<IHomeDataService, HomeDataService>();
            services.AddScoped<IEmpreendimentoDataService, EmpreendimentoDataService>();
            services.AddScoped<IEmpreendimentoGaleriaDataService, EmpreendimentoGaleriaDataService>();
            services.AddScoped<IEmpreendimentoValorDescricaoDataService, EmpreendimentoValorDescricaoDataService>();
            services.AddScoped<IEmpreendimentoInformacoesTecnicasDataService, EmpreendimentoInformacoesTecnicasDataService>();
            services.AddScoped<IEmpreendimentoFichaTecnicaDataService, EmpreendimentoFichaTecnicaDataService>();
            services.AddScoped<IEstadosDataService, EstadosDataService>();
            services.AddScoped<ICidadesDataService, CidadesDataService>();
            services.AddScoped<IEscritorioDataService, EscritorioDataService>();
            services.AddScoped<IProjetoDataService, ProjetoDataService>();
            services.AddScoped<IProjetoGaleriaDataService, ProjetoGaleriaDataService>();
            services.AddScoped<IMdMidiaDataService, MdMidiaDataService>();
            services.AddScoped<IPaginaDataService, PaginaDataService>();
            services.AddScoped<IFaqDataService, FaqDataService>();
            services.AddSingleton<IIdiomaDataService, IdiomaDataService>();
            
            services.AddBlazorTable();
            services.AddBlazorDragDrop();
            services.AddSingleton(x => new BlobServiceClient(Configuration.GetValue<string>("AzureStorageBlobConnectionStrings")));
            services.AddScoped<IBlobService, BlobService>();
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EmpreendimentoModelForm, EmpreendimentoModelUpdateCreate>();
                cfg.CreateMap<EmpreendimentoModelUpdateCreate, EmpreendimentoModelForm>();
                cfg.CreateMap<EmpreendimentoModel, EmpreendimentoModelForm>();
                cfg.CreateMap<EmpreendimentoModelForm, EmpreendimentoModel>();
                cfg.CreateMap<EmpreendimentoGaleriaModelForm, EmpreendimentoGaleriaModel>();
                cfg.CreateMap<EmpreendimentoGaleriaModel, EmpreendimentoGaleriaModelForm>();
                cfg.CreateMap<EmpreendimentoValorDescricaoModelForm, EmpreendimentoValorDescricaoModel>();
                cfg.CreateMap<EmpreendimentoValorDescricaoModel, EmpreendimentoValorDescricaoModelForm>();
                cfg.CreateMap<EmpreendimentoInformacoesTecnicasModel, EmpreendimentoInformacoesTecnicasModelForm>();
                cfg.CreateMap<EmpreendimentoInformacoesTecnicasModelForm, EmpreendimentoInformacoesTecnicasModel>();
                cfg.CreateMap<EmpreendimentoFichaTecnicaModel, EmpreendimentoFichaTecnicaModelForm>();
                cfg.CreateMap<EmpreendimentoFichaTecnicaModelForm, EmpreendimentoFichaTecnicaModel>();
                cfg.CreateMap<EscritorioModel, EscritorioModelForm>();
                cfg.CreateMap<EscritorioModelForm, EscritorioModel>();
                cfg.CreateMap<ProjetoModel, ProjetoModelForm>();
                cfg.CreateMap<ProjetoModelForm, ProjetoModel>();
                cfg.CreateMap<ProjetoGaleriaModel, ProjetoGaleriaModelForm>();
                cfg.CreateMap<ProjetoGaleriaModelForm, ProjetoGaleriaModel>();
                cfg.CreateMap<MdMidiaModel, MdMidiaModelForm>();
                cfg.CreateMap<MdMidiaModelForm, MdMidiaModel>();
                cfg.CreateMap<PaginaVendaSeuTerrenoModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaVendaSeuTerrenoModelForm>();
                cfg.CreateMap<PaginaSimuladorModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaSimuladorModelForm>();
                cfg.CreateMap<PaginaModelosModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaModelosModelForm>(); 
                cfg.CreateMap<PaginaProjetosSociaisModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaProjetosSociaisModelForm>();
                cfg.CreateMap<PaginaEscritorioModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaEscritorioModelForm>();
                cfg.CreateMap<PaginaHomeModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaHomeModelForm>();
                cfg.CreateMap<PaginaQuemSomosModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaQuemSomosModelForm>();
                cfg.CreateMap<PaginaProgramaDeIntegridadeModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaProgramaDeIntegridadeModelForm>();
                cfg.CreateMap<PaginaSalaDeImprensaModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaSalaDeImprensaModelForm>();
                cfg.CreateMap<PaginaTrabalheConoscoModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaTrabalheConoscoModelForm>();
                cfg.CreateMap<PaginaComoInvestirComponentModelForm, PaginaModel>();
                cfg.CreateMap<PaginaModel, PaginaComoInvestirComponentModelForm>();
                cfg.CreateMap<FaqModel, FaqModelForm>();
                cfg.CreateMap<FaqModelForm, FaqModel>();
                cfg.CreateMap<PaginaModel, SEOModelForm>();
                cfg.CreateMap<SEOModelForm, PaginaModel>();

            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseMigrationsEndPoint();

            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }*/

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
